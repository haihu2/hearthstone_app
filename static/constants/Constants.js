var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({

	ADD_CARD: null,
	REMOVE_CARD: null,
	SET_SEARCH_RESULT: null,
	RESET_VIEW: null,
	SET_DECK_LIST: null,
	USERNAME_CHANGE: null,
	DECK_SAVED: null,
	SET_INVITATION_LIST: null,
	SET_PENDING_LIST: null,
});
