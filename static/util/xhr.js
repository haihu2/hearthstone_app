// Wrapped XMLHttpRequest Object definition
// Assumes that the returned object is in 
// Enables callbacks to be implemented after the request finishes

function parseIfJSON(data) {
  try {
    return JSON.parse(data);
  } catch (e) {
    return data;
  }
};

function xhr(method, url, data) {
	var request = XMLHttpRequest ?
    new XMLHttpRequest() :
    new ActiveXObject('MSXML2.XMLHTTP.3.0');

	var callbacks = {
		success: [],
		fail: []
	};

	request.open(method, url, true);
    request.setRequestHeader(
    	'Content-type',
    	'application/json'
  		);

	//	Tell the xhr to invoke callbacks after the request finishes
  	request.onreadystatechange = function() {
	  	if(request.readyState === 4) {
	  		if( request.status > 99 && request.status < 300 ) {
	  			//	Success
	  			callbacks.success.forEach(function(callback) {
	  				callback.call(undefined, parseIfJSON(request.responseText));		// Take response text as the first argument
	  			});
	  		}
	  		else {
	  			//	Failure
	  			callbacks.fail.forEach(function(callback) {
	  				callback.call();
	  			})
	  		}
	  	}
	};
	request.send(JSON.stringify(data));

	// Return modifiers to the callbacks
	return {
		success: function(callback) {
			callbacks.success.push(callback);
			return this;
		},
		fail: function(callback) {
			callbacks.success.push(callback);
			return this;
		}
	}
};
