/**
 * @jsx React.DOM
 */
var React = require('react');
//	Require the upper-most class
var DeckEditor = require('./components/DeckEditor/DeckEditor');
var HeadLine = require('./components/HeadLine/HeadLine');
var InvitationBox = require('./components/InvitationBox/InvitationBox');

React.render(
	<DeckEditor />,	//	Class name
	document.getElementById('app')
);

React.render(
	<HeadLine />,
	document.getElementById('username_change')
);

React.render(
	<InvitationBox />,
	document.getElementById('bottom')
);