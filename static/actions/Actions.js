var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');

var Actions = {
  
	add_card: function(data) {
		AppDispatcher.handleViewAction({
			actionType: Constants.ADD_CARD,
			data: data
		})
	},

	remove_card: function(id, name) {
		AppDispatcher.handleViewAction({
			actionType: Constants.REMOVE_CARD,
			id: id,
		});
	},

	set_search_result: function(data) {
		AppDispatcher.handleViewAction({
			actionType: Constants.SET_SEARCH_RESULT,
			data: data
		})
	},

	reset_view: function() {
		AppDispatcher.handleViewAction({
			actionType: Constants.RESET_VIEW
		})
	},

	set_cur_deck: function(data, deckid) {
		AppDispatcher.handleViewAction({
			actionType: Constants.SET_CUR_DECK,
			data: data,
			deckid: deckid
		})
	},

	set_deck_list: function(lst) {
		AppDispatcher.handleViewAction({
			actionType: Constants.SET_DECK_LIST,
			lst: lst
		})
	},

	deck_saved: function(lst) {
		AppDispatcher.handleViewAction({
			actionType: Constants.DECK_SAVED,
			lst: lst
		})
	},

	username_change: function(username, deck_list) {
		AppDispatcher.handleViewAction({
			actionType: Constants.USERNAME_CHANGE,
			username: username,
			deck_list: deck_list
		})
	},

	set_invitation_list: function(invitation_list) {
		AppDispatcher.handleViewAction({
			actionType: Constants.SET_INVITATION_LIST,
			invitation_list: invitation_list
		})
	},

	set_pending_list: function(pending_list) {
		AppDispatcher.handleViewAction({
			actionType: Constants.SET_PENDING_LIST,
			pending_list: pending_list
		})
	}

};

module.exports = Actions;
