var EventEmitter = require('events').EventEmitter;

var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');
var Actions = require('../actions/Actions');

var merge = require('react/lib/merge');

var cur_deck = {};
var count = 0;
var changed = false;

function reset() {
	cur_deck = {};
	count = 0;
	changed = false;
	return null;
};

function add(data) {
	if (count === 30) {
		return;
	}
	var id = data['cardID'];
	var rarity = data['rarity'];
	if (cur_deck[id]) {
		if (rarity.toLowerCase() === 'legendary') {
			return null;
		}
		if (cur_deck[id]['num'] === 1) {
			cur_deck[id]['num'] = 2;
			count += 1;
		}
	}
	else {
		card = Object.create(data);
		card['num'] = 1;
		cur_deck[id] = card;
		count += 1;
	}
	changed = changed || true;
	return null;
};

function remove(id) {
	switch(cur_deck[id]['num']) {
		case 2:
			cur_deck[id]['num'] = 1;
			count -= 1;
			break;
		case 1:
			delete cur_deck[id];
			count -= 1;
	}
	changed = changed || true;
};

function set(data) {
	reset();
	if (data) {
		for (var i = data.length - 1; i >= 0; i--) {
			var card = data[i];
			add({
				cardID: card['cardID'],
				cardName: card['cardName'],
				cost: card['cost'],
				attack: card['attack'],
				health: card['health'],
				durability: card['durability'],
				cardText: card['cardText'],
				cardType: card['cardType'],
				rarity: card['rarity'],
				className: card['className']
			});
			if (card.num == 2) {
				add({
					cardID: card['cardID'],
					cardName: card['cardName'],
					cost: card['cost'],
					attack: card['attack'],
					health: card['health'],
					durability: card['durability'],
					cardText: card['cardText'],
					cardType: card['cardType'],
					rarity: card['rarity'],
					className: card['className']
				});
			}
		};
		changed = false;
	};
	return null;
};

var DeckStore = merge(EventEmitter.prototype, {

  getDeck: function() {
  	return cur_deck;
  },

  changed: function() {
  	return changed;
  },

  getCount: function() {
  	return count;
  },

  emitChange: function() {
    this.emit(Constants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(Constants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(Constants.CHANGE_EVENT, callback);
  }

});

AppDispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.actionType) {
  	case Constants.ADD_CARD:
  		add(action.data);
  		break;
    case Constants.REMOVE_CARD:
    	remove(action.id);
    	break;
    case Constants.RESET_VIEW:
    	reset();
    	break;
    case Constants.USERNAME_CHANGE:
    	reset();
    	break;
    case Constants.SET_CUR_DECK:
    	set(action.data);
    	break;
    case Constants.DECK_SAVED:
    	changed = false;
    	break;
    default: 
      	return true;
  }

  DeckStore.emitChange();

  return true;
  });

module.exports = DeckStore;