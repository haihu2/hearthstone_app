var EventEmitter = require('events').EventEmitter;

var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');
var Actions = require('../actions/Actions');

var merge = require('react/lib/merge');

window.username = '';
window.cur_deck = 'None';
window.decks = [{deckID: 'None', name: 'None'}];

function reset() {
  window.username = '';
  window.cur_deck = 'None';
  window.decks = [{deckID: 'None', name: 'None'}];
  return null;
};

var HeadLineStore = merge(EventEmitter.prototype, {

  getUsername: function() {
  	return window.username;
  },

  getCurDeck: function() {
    return window.cur_deck;
  },

  getDeckList: function() {
    return window.decks;
  },

  emitChange: function() {
    this.emit(Constants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(Constants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(Constants.CHANGE_EVENT, callback);
  }

});

AppDispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.actionType) {
    case Constants.RESET_VIEW:
      reset();
      break;
    case Constants.USERNAME_CHANGE:
      window.username = action.username;
      window.cur_deck = 'None';
      window.decks = action.deck_list;
      break;
    case Constants.SET_CUR_DECK:
      window.cur_deck = action.deckid;
      break;
    case Constants.SET_DECK_LIST:
      window.decks = action.lst;
      window.cur_deck = 'None';
      break;
    case Constants.DECK_SAVED:
      window.decks = action.lst;
      console.log(window.decks)
      window.cur_deck = 'None';
      break;
    default: 
      return true;
  };

  HeadLineStore.emitChange();

  return true;
  });

module.exports = HeadLineStore;