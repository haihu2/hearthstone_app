var EventEmitter = require('events').EventEmitter;

var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');
var Actions = require('../actions/Actions');

var merge = require('react/lib/merge');

var received_invitations = [];
var pending_results = [];

function reset() {
	received_invitations = [];
  pending_results = [];
  return null;
};

function remove(invitationid) {
  var index = 0;
  while (index < received_invitations.length) {
    if (received_invitations[index].invitationID = invitationid) {
      break;
    }
  }
  if (index < received_invitations.length) {
    received_invitations.splice(index, 1);  
  }
  return null;
};

var InvitationStore = merge(EventEmitter.prototype, {

  getReceivedInvitations: function() {
  	return received_invitations;
  },

  getPendingResults: function() {
    return pending_results;
  },

  emitChange: function() {
    this.emit(Constants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(Constants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(Constants.CHANGE_EVENT, callback);
  }

});

AppDispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.actionType) {
    case Constants.SET_INVITATION_LIST:
      received_invitations = action.invitation_list;
      break;
    case Constants.SET_PENDING_LIST:
      pending_results = action.pending_list;
      break;
    case Constants.RESET_VIEW:
      reset();
      break;
    case Constants.USERNAME_CHANGE:
      reset();
      break;
    default: 
      return true;
  };

  InvitationStore.emitChange();

  return true;
  });

module.exports = InvitationStore;