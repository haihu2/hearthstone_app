var EventEmitter = require('events').EventEmitter;

var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');
var Actions = require('../actions/Actions');

var merge = require('react/lib/merge');

var results = [];

function reset() {
	results = [];
  return null;
};

function set(response) {
  results = response;
  return null;
};

var SearchStore = merge(EventEmitter.prototype, {

  getResult: function() {
  	return results;
  },

  emitChange: function() {
    this.emit(Constants.CHANGE_EVENT);
  },

  addChangeListener: function(callback) {
    this.on(Constants.CHANGE_EVENT, callback);
  },

  removeChangeListener: function(callback) {
    this.removeListener(Constants.CHANGE_EVENT, callback);
  }

});

AppDispatcher.register(function(payload) {
  var action = payload.action;

  switch(action.actionType) {
    case Constants.SET_SEARCH_RESULT:
      set(action.data);
      break;
    case Constants.RESET_VIEW:
      reset();
      break;
    case Constants.USERNAME_CHANGE:
      reset();
      break;
    default: 
      return true;
  };

  SearchStore.emitChange();

  return true;
  });

module.exports = SearchStore;