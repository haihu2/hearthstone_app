/**
 * @jsx React.DOM
 */
var React = require('react');
var Action = require('../../actions/Actions');

var CardLine = React.createClass({

	_handle_double_click: function(event) {
		event.preventDefault();
		var card = this.props.card;
		Action.remove_card(card.id);
		// xhr('POST', '/removeFromDeck/', {
		// 	cardID: card.id,
		// 	deckID: window.cur_deck
		// }).success(function(data) {
		// 	Action.remove_card(card.id);
		// 	return null;
		// })
		return null;
	},

	render: function() {
		var card = this.props.card;
		return (
			<tr onDoubleClick={this._handle_double_click} className={card.rarity}>
				<td>{card.name}</td>
				<td>{card.num}</td>
			</tr>
		);
	}

});

module.exports = CardLine;