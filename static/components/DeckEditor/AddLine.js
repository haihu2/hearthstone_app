/**
 * @jsx React.DOM
 */
var React = require('react');
var Action = require('../../actions/Actions');

var AddLine = React.createClass({

	_handle_add_card: function(event) {
		event.preventDefault();
		var card = this.props.card;
		Action.add_card(card);
		// xhr('POST', '/addToDeck/', {
		// 	cardID: card.cardID,
		// 	deckID: window.cur_deck
		// }).success(function() {
		// 	Action.add_card(card);
		// 	return null;
		// })
		return null;
	},

	render: function() {
		var card = this.props.card;
		switch(card.cardType) {
			case 'Minion':
				var description = ' - ' + card.cost + '/' + card.attack + '/' + card.health;
				break;
			case 'Spell':
				var description = ' - ' + card.cost;
				break;
			case 'Weapon':
				var description = ' - ' + card.cost + '/' + card.attack + '/' + card.durability;
				break;
		}
		if (card.className) {
			var classtag = " - " + card.className;
		}
		return (
			<li className={card.rarity} onClick={this._handle_add_card}>{card.cardType}{classtag}{description} - {card.cardName} </li>
		);
	}

});

module.exports = AddLine;