/**
 * @jsx React.DOM
 */
var React = require('react');
var SearchStore = require('../../stores/SearchStore');

var AddLine = require('./AddLine');

function getState() {
	return {
		cards: SearchStore.getResult(),
	};
}

var SearchResult = React.createClass({

	getInitialState: function() {
		return getState();
	},

	componentDidMount: function() {
		SearchStore.addChangeListener(this._onChange);
		SearchStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		SearchStore.removeChangeListener(this._onChange);
		SearchStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
    	this.setState(getState());
	},

	_render_addline: function(card) {
		return (
			<AddLine card={card} />
		)
	},

	render: function() {
		var cards = this.state.cards;
		return (
			<div id="search_result" className="noselect">
				<ul>
					{cards.map(this._render_addline)}
				</ul>
			</div>
		);
	}

});

module.exports = SearchResult;