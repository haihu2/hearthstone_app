/**
 * @jsx React.DOM
 */
var React = require('react');
var CardLine = require('./CardLine');

var Display = React.createClass({

	_render_cardline: function(card) {
		return (
			<CardLine card = {card} />
		);
	},

	render: function() {
		var cards = this.props.cards;
		var lines = [];
		for (var key in cards) {
			lines.push({
				id: cards[key]['cardID'],
				name: cards[key]['cardName'],
				num: cards[key]['num'],
				rarity: cards[key]['rarity'],
				cost: cards[key]['cost']
			});
		};
		lines = lines.map(this._render_cardline);
		return ( 
			<div id="monitor" className="noselect">
				<table>
					<tr>
						<td>Name</td>
						<td>Num</td>
					</tr>
					{lines}
					<tr>
						<td></td>
						<td>{this.props.count}</td>
					</tr>
				</table>
				<form onSubmit={this.props.handleSaveDeck} >
					<input type="submit" value="Save current deck" id="save_button" className="noselect"/>
				</form>
				<form onSubmit={this.props.handleSearchOpponent} >
					<input type="submit" value="Search for opponent!" id="search_opponent" className="noselect"/>
				</form>
			</div>
		);
	}

});

module.exports = Display;