/**
 * @jsx React.DOM
 */
var React = require('react');
var Action = require('../../actions/Actions');
var SearchResult = require('./SearchResult');

var Selection = React.createClass({

	_handle_form_submission: function(event) {
		event.preventDefault();
		var data = {};
		data.name = document.getElementById('card_name_input').value;
		data.attack_comp = document.getElementById('attack_sel').value;
		data.attack = document.getElementById('attack_input').value;
		data.health_comp = document.getElementById('health_sel').value;
		data.health = document.getElementById('health_input').value;
		data.cost_comp = document.getElementById('cost_sel').value;
		data.cost = document.getElementById('cost_input').value;
		data.cls = document.getElementById('class_sel').value;
		xhr('POST', '/searchCards/', data).success(function(data) {
			Action.set_search_result(data);
		});
	},

	render: function() {
		return ( 
			<div id="selector">
				<form onSubmit = {this._handle_form_submission} id="searching_conditions">
					Name: <input type="text" name="card_name_input" id="card_name_input" /><br />
					Attack <select id="attack_sel">
						<option value=">">{">"}</option>
						<option value=">=">{">="}</option>
						<option value="=">{"="}</option>
						<option value="!=">{"!="}</option>
						<option value="<=">{"<="}</option>
						<option value="<">{"<"}</option>
					</select> <input type="number" name="attack_input" id="attack_input" /><br />
					Health <select id="health_sel">
						<option value=">">{">"}</option>
						<option value=">=">{">="}</option>
						<option value="=">{"="}</option>
						<option value="!=">{"!="}</option>
						<option value="<=">{"<="}</option>
						<option value="<">{"<"}</option>
					</select> <input type="number" name="health_input" id="health_input" /><br />
					Cost <select id="cost_sel">
						<option value=">">{">"}</option>
						<option value=">=">{">="}</option>
						<option value="=">{"="}</option>
						<option value="!=">{"!="}</option>
						<option value="<=">{"<="}</option>
						<option value="<">{"<"}</option>
					</select> <input type="number" name="cost_input" id="cost_input" /><br />
					Class <select id="class_sel"> 
						<option value="All">All</option>
						<option value="Priest">Priest</option>
						<option value="Druid">Druid</option>
						<option value="Mage">Mage</option>
						<option value="Shaman">Shaman</option>
						<option value="Warlock">Warlock</option>
						<option value="Rogue">Rogue</option>
						<option value="Hunter">Hunter</option>
						<option value="Paladin">Paladin</option>
						<option value="Warrior">Warrior</option>
					</select>
					<input type="submit" value="Search Card!"/>
				</form>
				<SearchResult />
			</div>
		);
	}

});

module.exports = Selection;