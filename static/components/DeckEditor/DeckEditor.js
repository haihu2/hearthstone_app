/**
 * @jsx React.DOM
 */
var React = require('react');
var Display = require('./Display');
var Selection = require('./Selection');
var DeckStore = require('../../stores/DeckStore');
var Actions = require('../../actions/Actions');

function getState() {
	return {
		cards: DeckStore.getDeck(),
		changed: DeckStore.changed(),
		count: DeckStore.getCount()
	};
}

var DeckEditor = React.createClass({

	_handle_save_deck: function(event) {
		event.preventDefault();
		if ( this.state.changed && window.cur_deck ) {
			xhr('POST', '/SaveDeck/', {
				cards: this.state.cards,
				deckId: window.cur_deck,
				username: window.username
			}).success(function(data) {
				data.unshift({deckID: 'None', name: 'None'});
				Actions.deck_saved(data);
				window.alert('Deck saved');
				return null;
			})
		};
		return null;
	},

	_handle_search_opponent: function(event) {
		event.preventDefault();
		if ( this.state.count === 30 ) {
			var search_window = window.open('/search_window.html', 
				'search_opponent', 
				'height=180,width=370,left=200,top=150');
			if (window.focus) {search_window.focus()};
		};
		return null;
	},

	getInitialState: function() {
		return getState();
	},

	componentDidMount: function() {
		DeckStore.addChangeListener(this._onChange);
		DeckStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		DeckStore.removeChangeListener(this._onChange);
		DeckStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
    	this.setState(getState());
	},

	render: function() {
		return ( 
			<div id="DeckEditor">
				<Selection />
				<Display cards = {this.state.cards} count = {this.state.count} handleSearchOpponent = {this._handle_search_opponent} handleSaveDeck = {this._handle_save_deck}/>
			</div>
		);
	}

});

module.exports = DeckEditor;