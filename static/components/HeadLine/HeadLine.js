/**
 * @jsx React.DOM
 */
var React = require('react');
var Actions = require('../../actions/Actions');
var HeadLineStore = require('../../stores/HeadLineStore');

function getState() {
	return {
		username: HeadLineStore.getUsername(),
		cur_deck: HeadLineStore.getCurDeck(),
		decks: HeadLineStore.getDeckList()
	};
};

var HeadLine = React.createClass({

	getInitialState: function() {
		return getState();
	},

	componentDidMount: function() {
		HeadLineStore.addChangeListener(this._onChange);
		HeadLineStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		HeadLineStore.removeChangeListener(this._onChange);
		HeadLineStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
    	this.setState(getState());
	},

	_handle_username_submission: function(event) {
		event.preventDefault();
		var usrname = document.getElementById('username_input').value;
		var password = document.getElementById('password_input').value;
		if ( usrname ) {
			xhr('POST', '/allDecks/' + usrname, {password: password}).success(function(result) {
				if (result.success) {
					deck_list = result.deck_list;
					deck_list.unshift({deckID: 'None', name: 'None'});
					Actions.username_change(usrname, deck_list);
					xhr('GET', '/receivedInvitations/' + usrname).success(function(invitaion_list) {
						Actions.set_invitation_list(invitaion_list);
						xhr('GET', '/AllPendingResults/' + usrname).success(function(pending_list) {
							Actions.set_pending_list(pending_list);
							return null;
						});
						return null;
					});
				}
				else {
					Actions.reset_view();
				};
				return null;
			});
		}
		else {
			Actions.reset_view();
		}
		return null;
	},

	_handle_deck_change: function(event) {
		var cur_deck = document.getElementById('deck_name_sel').value;
		if ( cur_deck === 'None' ) {
			Actions.set_cur_deck({}, cur_deck);
		}
		else {
			xhr('GET', '/CardsOfDeck/' + cur_deck).success(function(data) {
				Actions.set_cur_deck(data, cur_deck);
				return null;
			});
		};
		return null;
	},

	_handle_deck_creation: function(event) {
		event.preventDefault();
		var deck_name = document.getElementById('deck_creation_name_input').value;
		if( deck_name ) {
			xhr('POST', '/CreateDeck/', {
				deck_name: deck_name,
				username: this.state.username
			}).success(function(data) {
				data.unshift({deckID: 'None', name: 'None'});
				Actions.set_deck_list(data);
				return null;
			})
		}
	},

	_handle_edit_profile: function(event) {
		event.preventDefault();
		var edit_window = window.open('/edit_profile.html', 'edit_profile', 'height=150,width=300,left=200,top=150');
		if (window.focus) {edit_window.focus()};
		return null;
	},

	_render_option: function(deck) {
		return (
			<option value={deck.deckID}>{deck.name}</option>
		)
	},

	render: function() {
		var decks = this.state.decks;
		var visibility_block = 'block';
		var visibility_inline = 'inline';
		if ( this.state.username === '' ) {
			visibility_block = 'none';
			visibility_inline = 'none';
		}

		return (
			<div id="headline">
				<div id="usr" >
					<form onSubmit={this._handle_username_submission} className="center float_left">
						Username: <input type="text" name="username_input" id="username_input" />
						Password: <input type="password" name="password_input" id="password_input" />
						<input type="submit" value="Submit" />
					</form>
					<form onSubmit={this._handle_edit_profile} style={{'display': visibility_inline}} id="edit_profile_button">
						<input type="submit" value="Edit profile" name="change_user_profile" className="noselect" />
					</form>
					<form onSubmit={this._handle_deck_creation} id="deck_creation" className="center" style={{'display': visibility_inline}}>
						Create new deck: <input type="text" name="deck_creation_name_input" id="deck_creation_name_input" />
						<input type="submit" value="Submit" />
					</form>
				</div>
				<select id="deck_name_sel" onChange={this._handle_deck_change} className="float_right">
					{decks.map(this._render_option)}
				</select>
			</div>
		);
	}

});

module.exports = HeadLine;