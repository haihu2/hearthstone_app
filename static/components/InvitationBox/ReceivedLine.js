/**
 * @jsx React.DOM
 */
var React = require('react');
var Actions = require('../../actions/Actions');

var ReceivedLine = React.createClass({

	_handle_report: function(event) {
		event.preventDefault();
		if (window.cur_deck === 'None') {
			window.alert('Please select the chosen deck for this battle!');
		}
		else {
			var new_window = window.open('/report_result.html', this.props.invitation.invitationID, 'width=200, height=100, left=200, top=150');
			if (window.focus) {new_window.focus()};
		}
		return null;
	},

	_handle_decline: function(event) {
		event.preventDefault();
		xhr('POST', '/declineInvitation/', {
			invitationID: this.props.invitation.invitationID,
			username: window.username
		}).success(function(remaining) {
			Actions.set_invitation_list(remaining);
			return null;
		});
		return null;
	},

	_handle_get_email: function(event) {
		event.preventDefault();
		xhr('GET', '/EmailOf/' + this.props.invitation.initiator).success(function(email) {
			var new_window = window.open('/empty.html', 'Email', 'width=200, height=100, left=200, top=150');
			if (email.length) {
				new_window.document.write(email[0].email);
			}
			else {
				new_window.document.write('User not found');
			}
			if (window.focus) {new_window.focus()};
		})
	},

	_handle_report: function(won) {
		console.log(window.cur_deck);
		if (window.cur_deck && window.cur_deck !== 'None') {
			xhr('POST', '/ReportResult/', {
				result: won,
				invitationID: this.props.invitation.invitationID,
				used_deck: window.cur_deck,
				username: window.username
			}).success(function(invitation_list) {
				Actions.set_invitation_list(invitation_list);
				return null;
			});
		}
		else {
			window.alert('Please select deck used!');
		}
	},

	_handle_report_won: function(event) {
		event.preventDefault();
		this._handle_report(true);
		return null;
	},

	_handle_report_lost: function(event) {
		event.preventDefault();
		this._handle_report(false);
		return null;
	},

	render: function() {
		var invitation = this.props.invitation;
		return (
			<li>
				{invitation.message}<br />
				<button type="button" className="invitation_button" onClick={this._handle_report_won} > Report Won </button>
				<button type="button" className="invitation_button" onClick={this._handle_report_lost} > Report Lost </button>
				<button type="button" className="invitation_button" onClick={this._handle_get_email} > Get Email </button>
				<button type="button" className="invitation_button" onClick={this._handle_decline} > Decline </button><br />
			</li>
		);
	}

});

module.exports = ReceivedLine;