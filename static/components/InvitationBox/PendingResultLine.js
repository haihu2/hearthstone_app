/**
 * @jsx React.DOM
 */
var React = require('react');
var Actions = require('../../actions/Actions');

var PendingResultLine = React.createClass({

	_handle_accept: function(event) {
		event.preventDefault();
		this._report_acceptance(true);
	},

	_handle_reject: function(event) {
		event.preventDefault();
		this._report_acceptance(false);
	},

	_report_acceptance: function(accepted) {
		xhr('POST', '/AcceptOrRejectResult/', {
			pendingresultid: this.props.pendingresult.pendingresultid,
			acceptance: accepted,
			username: window.username
		}).success(function(pending_list) {
			Actions.set_pending_list(pending_list);
			return null;
		});
		return null;
	},

	render: function() {
		var pendingresult = this.props.pendingresult;
		return (
			<li>
				{pendingresult.winnerName} beats {pendingresult.loserName}<br />
				<button type="button" className="pending_result_button" onClick={this._handle_accept} > Accept </button>
				<button type="button" className="pending_result_button" onClick={this._handle_reject} > Reject </button><br />
			</li>
		);
	}

});

module.exports = PendingResultLine;