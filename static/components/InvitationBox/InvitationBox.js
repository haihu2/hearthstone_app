/**
 * @jsx React.DOM
 */
var React = require('react');
var InvitationStore = require('../../stores/InvitationStore');
var PendingResultLine = require('./PendingResultLine');
var ReceivedLine = require('./ReceivedLine');
var Actions = require('../../actions/Actions');

function getState() {
	return {
		received_invitations: InvitationStore.getReceivedInvitations(),
		pending_results: InvitationStore.getPendingResults()
	};
}

var InvitationBox = React.createClass({

	getInitialState: function() {
		return getState();
	},

	componentDidMount: function() {
		InvitationStore.addChangeListener(this._onChange);
		InvitationStore.addChangeListener(this._onChange);
	},

	componentWillUnmount: function() {
		InvitationStore.removeChangeListener(this._onChange);
		InvitationStore.removeChangeListener(this._onChange);
	},

	_onChange: function() {
    	this.setState(getState());
	},

	_render_received_invitation_line: function(invitation) {
		return (
			<ReceivedLine invitation = {invitation} />
		);
	},

	_render_pending_result_line: function(pendingresult) {
		return (
			<PendingResultLine pendingresult = {pendingresult} />
		);
	},

	render: function() {
		if (this.state.pending_results.length > 0) {
			var pending_result_lines = this.state.pending_results.map(this._render_pending_result_line);
		}
		if (this.state.received_invitations.length > 0) {
			var invitation_lines = this.state.received_invitations.map(this._render_received_invitation_line);
		}
		return ( 
			<div id="InvitationBox">
				<div id="received_invitations">
					Challenge Invitations:
					<ul>
						{invitation_lines}
					</ul>
				</div>
				<div id="pending_results">
					Pending Results:
					<ul>
						{pending_result_lines}
					</ul>
				</div>
			</div>
		);
	}

});

module.exports = InvitationBox;