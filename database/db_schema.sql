DROP DATABASE IF EXISTS hearthstone_app;
CREATE DATABASE IF NOT EXISTS hearthstone_app;

use hearthstone_app;

DROP TABLE IF EXISTS Card;
CREATE TABLE IF NOT EXISTS Card(
	cardID CHAR(100) NOT NULL,
	cardName CHAR(40) NOT NULL,
	cost INTEGER NOT NULL,
	attack INTEGER, 
	health INTEGER,
	durability INTEGER,
	cardText CHAR(255) NOT NULL,
	cardType CHAR(20) NOT NULL,
	rarity CHAR(20),
	className CHAR(20) DEFAULT NULL,
	PRIMARY KEY (cardID)
);

DROP TABLE IF EXISTS User;
CREATE TABLE IF NOT EXISTS User(
	username CHAR(20) NOT NULL,
	rank INTEGER NOT NULL,
	region CHAR(20) NOT NULL, 
	spendings INTEGER NOT NULL,
	PRIMARY KEY (username)
);

DROP TABLE IF EXISTS HeroPower;
CREATE TABLE IF NOT EXISTS HeroPower(
	ClassName CHAR(20) NOT NULL,
	HeroPowerID CHAR(100) NOT NULL,
	HeroPowerName CHAR(40) NOT NULL,
	HeroPowerCost INTEGER NOT NULL,
	HeroPowerText CHAR(255) NOT NULL,
	PRIMARY KEY (HeroPowerID)
);

DROP TABLE IF EXISTS Deck;
CREATE TABLE IF NOT EXISTS Deck(
	deckID INTEGER NOT NULL AUTO_INCREMENT,
	name CHAR(20) NOT NULL,
	PRIMARY KEY (deckID)
);

DROP TABLE IF EXISTS ConsistOf;
CREATE TABLE IF NOT EXISTS ConsistOf(
	cardID char(100) NOT NULL,
	deckID INTEGER NOT NULL,
	PRIMARY KEY (cardID, deckID)
);

DROP TABLE IF EXISTS Challenge;
CREATE TABLE IF NOT EXISTS Challenge(
	WinningID CHAR(20) NOT NULL,
	LosingID CHAR(20) NOT NULL,
	PRIMARY KEY (WinningID, LosingID)
);

DROP TABLE IF EXISTS Favourite;
CREATE TABLE IF NOT EXISTS Favourite(
	username CHAR(20) NOT NULL,
	deckID INTEGER NOT NULL,
	deckName CHAR(20) NOT NULL,
	PRIMARY KEY (username, deckID)
);

DROP TABLE IF EXISTS UserHasCard;
CREATE TABLE IF NOT EXISTS UserHasCard(
	username CHAR(20) NOT NULL,
	cardID char(100) NOT NULL,
	PRIMARY KEY (username, cardID)
);

DROP TABLE IF EXISTS UserHasDeck;
CREATE TABLE IF NOT EXISTS UserHasDeck(
	username CHAR(20) NOT NULL,
	deckID INTEGER NOT NULL,
	PRIMARY KEY (username, deckID)
);