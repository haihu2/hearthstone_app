-- MySQL dump 10.13  Distrib 5.6.22, for osx10.10 (x86_64)
--
-- Host: localhost    Database: hearthstone_app
-- ------------------------------------------------------
-- Server version	5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Card`
--

DROP TABLE IF EXISTS `Card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Card` (
  `cardID` char(100) NOT NULL,
  `cardName` char(40) NOT NULL,
  `cost` int(11) NOT NULL,
  `attack` int(11) DEFAULT NULL,
  `health` int(11) DEFAULT NULL,
  `durability` int(11) DEFAULT NULL,
  `cardText` char(255) NOT NULL,
  `cardType` char(20) NOT NULL,
  `rarity` char(20) DEFAULT NULL,
  `className` char(20) DEFAULT NULL,
  PRIMARY KEY (`cardID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Card`
--

LOCK TABLES `Card` WRITE;
/*!40000 ALTER TABLE `Card` DISABLE KEYS */;
INSERT INTO `Card` VALUES ('BRMA01_3','Dark Iron Bouncer',6,4,8,NULL,'Always wins Brawls.','Minion',NULL,NULL),('BRMA01_4','Get \'em!',3,NULL,NULL,NULL,'Summon four 1/1 Dwarves with <b>Taunt</b>.','Spell',NULL,NULL),('BRMA01_4t','Guzzler',1,1,1,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('BRMA02_2t','Dark Iron Spectator',1,1,1,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('BRMA03_3','Moira Bronzebeard',2,1,3,NULL,'Thaurissan\'s Hero Power can\'t be used.\nNever attacks minions unless they have <b>Taunt</b>.','Minion',NULL,NULL),('BRMA03_3H','Moira Bronzebeard',2,3,1,NULL,'Thaurissan\'s Hero Power can\'t be used.\nNever attacks minions unless they have <b>Taunt</b>.','Minion',NULL,NULL),('BRMA04_3','Firesworn',0,0,5,NULL,'<b>Deathrattle:</b> Deal 1 damage to the enemy hero for each Firesworn that died this turn.','Minion',NULL,NULL),('BRMA04_3H','Firesworn',0,0,5,NULL,'<b>Deathrattle:</b> Deal 3 damage to the enemy hero for each Firesworn that died this turn.','Minion',NULL,NULL),('BRMA04_4','Rock Out',3,NULL,NULL,NULL,'Summon 3 Firesworn. <b>Overload:</b> (2)','Spell',NULL,NULL),('BRMA04_4H','Rock Out',3,NULL,NULL,NULL,'Summon 3 Firesworn. <b>Overload:</b> (2)','Spell',NULL,NULL),('BRMA05_3','Living Bomb',4,NULL,NULL,NULL,'Choose an enemy minion. If it lives until your next turn, deal $5 damage to all enemies.','Spell',NULL,NULL),('BRMA05_3H','Living Bomb',3,NULL,NULL,NULL,'Choose an enemy minion. If it lives until your next turn, deal $10 damage to all enemies.','Spell',NULL,NULL),('BRMA06_4','Flamewaker Acolyte',2,1,3,NULL,'','Minion',NULL,NULL),('BRMA06_4H','Flamewaker Acolyte',2,3,3,NULL,'','Minion',NULL,NULL),('BRMA07_3','TIME FOR SMASH',4,NULL,NULL,NULL,'Deal $5 damage to a random enemy. Gain 5 Armor.','Spell',NULL,NULL),('BRMA08_3','Drakkisath\'s Command',1,NULL,NULL,NULL,'Destroy a minion. Gain 10 Armor.','Spell',NULL,NULL),('BRMA09_2Ht','Whelp',1,2,2,NULL,'','Minion',NULL,NULL),('BRMA09_2t','Whelp',1,1,1,NULL,'','Minion',NULL,NULL),('BRMA09_3Ht','Old Horde Orc',1,2,2,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('BRMA09_3t','Old Horde Orc',1,1,1,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('BRMA09_4Ht','Dragonkin',1,5,4,NULL,'','Minion',NULL,NULL),('BRMA09_4t','Dragonkin',1,3,1,NULL,'','Minion',NULL,NULL),('BRMA09_5Ht','Gyth',3,8,8,NULL,'','Minion','Legendary',NULL),('BRMA09_5t','Gyth',3,8,4,NULL,'','Minion','Legendary',NULL),('BRMA09_6','The True Warchief',1,NULL,NULL,NULL,'Destroy a Legendary minion.','Spell',NULL,NULL),('BRMA10_4','Corrupted Egg',1,0,1,NULL,'When this minion has 4 or more Health, it hatches.','Minion',NULL,NULL),('BRMA10_4H','Corrupted Egg',1,0,3,NULL,'When this minion has 5 or more Health, it hatches.','Minion',NULL,NULL),('BRMA10_5','Chromatic Drake',4,7,3,NULL,'','Minion',NULL,NULL),('BRMA10_5H','Chromatic Drake',4,7,7,NULL,'','Minion',NULL,NULL),('BRMA10_6','Razorgore\'s Claws',1,1,NULL,5,'Whenever a Corrupted Egg dies, gain +1 Attack.','Weapon',NULL,NULL),('BRMA11_3','Burning Adrenaline',0,NULL,NULL,NULL,'Deal $2 damage to the enemy hero.','Spell',NULL,NULL),('BRMA12_3','Brood Affliction: Red',1,NULL,NULL,NULL,'While this is in your hand, take 1 damage at the start of your turn.','Spell',NULL,NULL),('BRMA12_3H','Brood Affliction: Red',3,NULL,NULL,NULL,'While this is in your hand, take 3 damage at the start of your turn.','Spell',NULL,NULL),('BRMA12_4','Brood Affliction: Green',1,NULL,NULL,NULL,'While this is in your hand, restore 2 health to your opponent at the start of your turn.','Spell',NULL,NULL),('BRMA12_4H','Brood Affliction: Green',3,NULL,NULL,NULL,'While this is in your hand, restore 6 health to your opponent at the start of your turn.','Spell',NULL,NULL),('BRMA12_5','Brood Affliction: Blue',1,NULL,NULL,NULL,'While this is in your hand, Chromaggus\' spells cost (1) less.','Spell',NULL,NULL),('BRMA12_5H','Brood Affliction: Blue',3,NULL,NULL,NULL,'While this is in your hand, Chromaggus\' spells cost (3) less.','Spell',NULL,NULL),('BRMA12_6','Brood Affliction: Black',1,NULL,NULL,NULL,'While this is in your hand, whenever Chromaggus draws a card, he gets another copy of it.','Spell',NULL,NULL),('BRMA12_6H','Brood Affliction: Black',3,NULL,NULL,NULL,'While this is in your hand, whenever Chromaggus draws a card, he gets another copy of it.','Spell',NULL,NULL),('BRMA12_7','Brood Affliction: Bronze',1,NULL,NULL,NULL,'While this is in your hand, Chromaggus\' minions cost (1) less.','Spell',NULL,NULL),('BRMA12_7H','Brood Affliction: Bronze',3,NULL,NULL,NULL,'While this is in your hand, Chromaggus\' minions cost (3) less.','Spell',NULL,NULL),('BRMA12_8','Chromatic Mutation',2,NULL,NULL,NULL,'Transform a minion into a 2/2 Chromatic Dragonkin.','Spell',NULL,NULL),('BRMA12_8t','Chromatic Dragonkin',2,2,3,NULL,'Whenever your opponent casts a spell, gain +2/+2.','Minion',NULL,NULL),('BRMA13_5','Son of the Flame',0,6,3,NULL,'<b>Battlecry:</b> Deal 6 damage.','Minion',NULL,NULL),('BRMA13_6','Living Lava',0,6,6,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('BRMA13_7','Whirling Ash',0,4,5,NULL,'<b>Windfury</b>','Minion',NULL,NULL),('BRMA13_8','DIE, INSECT!',0,NULL,NULL,NULL,'Deal $8 damage to a random enemy.','Spell',NULL,NULL),('BRMA14_11','Recharge',0,NULL,NULL,NULL,'Fill all empty Mana Crystals.','Spell',NULL,NULL),('BRMA14_12','Magmaw',5,10,2,NULL,'<b>Taunt</b>','Minion','Legendary',NULL),('BRMA14_3','Arcanotron',0,2,2,NULL,'Both players have <b>Spell Damage +2</b>.','Minion','Legendary',NULL),('BRMA14_5','Toxitron',1,3,3,NULL,'At the start of your turn, deal 1 damage to all other minions.','Minion','Legendary',NULL),('BRMA14_5H','Toxitron',1,4,4,NULL,'At the start of your turn, deal 1 damage to all other minions.','Minion','Legendary',NULL),('BRMA14_7','Electron',3,5,5,NULL,'All spells cost (3) less.','Minion','Legendary',NULL),('BRMA14_7H','Electron',3,6,6,NULL,'All spells cost (3) less.','Minion','Legendary',NULL),('BRMA14_9','Magmatron',5,7,7,NULL,'Whenever a player plays a card, Magmatron deals 2 damage to them.','Minion','Legendary',NULL),('BRMA14_9H','Magmatron',5,8,8,NULL,'Whenever a player plays a card, Magmatron deals 2 damage to them.','Minion','Legendary',NULL),('BRMA15_3','Release the Aberrations!',2,NULL,NULL,NULL,'Summon 3 Aberrations.','Spell',NULL,NULL),('BRMA15_4','Aberration',1,1,1,NULL,'<b>Charge</b>','Minion',NULL,NULL),('BRMA16_3','Sonic Breath',4,NULL,NULL,NULL,'Deal $3 damage to a minion. Give your weapon +3 Attack.','Spell',NULL,NULL),('BRMA16_4','Reverberating Gong',1,NULL,NULL,NULL,'Destroy your opponent\'s weapon.','Spell',NULL,NULL),('BRMA16_5','Dragonteeth',1,0,NULL,6,'Whenever your opponent plays a card, gain +1 Attack.','Weapon',NULL,NULL),('BRMA17_4','LAVA!',2,NULL,NULL,NULL,'Deal $2 damage to all minions.','Spell',NULL,NULL),('BRMA17_6','Bone Construct',1,2,1,NULL,'','Minion',NULL,NULL),('BRMA17_6H','Bone Construct',1,4,2,NULL,'','Minion',NULL,NULL),('BRMA17_7','Chromatic Prototype',2,1,4,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('BRMA17_9','Onyxiclaw',2,2,NULL,6,'','Weapon',NULL,NULL),('BRMA_01','Flameheart',3,NULL,NULL,NULL,'Draw 2 cards.\nGain 4 Armor.','Spell',NULL,NULL),('BRM_001','Solemn Vigil',5,NULL,NULL,NULL,'Draw 2 cards. Costs (1) less for each minion that died this turn.','Spell','Common','Paladin'),('BRM_002','Flamewaker',3,2,4,NULL,'After you cast a spell, deal 2 damage randomly split among all enemies.','Minion','Rare','Mage'),('BRM_003','Dragon\'s Breath',5,NULL,NULL,NULL,'Deal $4 damage. Costs (1) less for each minion that died this turn.','Spell','Common','Mage'),('BRM_004','Twilight Whelp',1,2,1,NULL,'<b>Battlecry:</b> If you\'re holding a Dragon, gain +2 Health.','Minion','Common','Priest'),('BRM_004t','Whelp',1,1,1,NULL,'','Minion',NULL,NULL),('BRM_005','Demonwrath',3,NULL,NULL,NULL,'Deal $2 damage to all non-Demon minions.','Spell','Rare','Warlock'),('BRM_006','Imp Gang Boss',3,2,4,NULL,'Whenever this minion takes damage, summon a 1/1 Imp.','Minion','Common','Warlock'),('BRM_006t','Imp',1,1,1,NULL,'','Minion',NULL,'Warlock'),('BRM_007','Gang Up',2,NULL,NULL,NULL,'Choose a minion. Shuffle 3 copies of it into your deck.','Spell','Common','Rogue'),('BRM_008','Dark Iron Skulker',5,4,3,NULL,'<b>Battlecry:</b> Deal 2 damage to all undamaged enemy minions.','Minion','Rare','Rogue'),('BRM_009','Volcanic Lumberer',9,7,8,NULL,'<b>Taunt</b>\nCosts (1) less for each minion that died this turn.','Minion','Rare','Druid'),('BRM_010','Druid of the Flame',3,2,2,NULL,'<b>Choose One</b> - Transform into a 5/2 minion; or a 2/5 minion.','Minion','Common','Druid'),('BRM_010t','Druid of the Flame',3,5,2,NULL,'','Minion','Common','Druid'),('BRM_010t2','Druid of the Flame',3,2,5,NULL,'','Minion','Common','Druid'),('BRM_011','Lava Shock',2,NULL,NULL,NULL,'Deal $2 damage.\nUnlock your <b>Overloaded</b> Mana Crystals.','Spell','Rare','Shaman'),('BRM_012','Fireguard Destroyer',4,3,6,NULL,'<b>Battlecry:</b> Gain 1-4 Attack. <b>Overload:</b> (1)','Minion','Common','Shaman'),('BRM_013','Quick Shot',2,NULL,NULL,NULL,'Deal $3 damage.\nIf your hand is empty, draw a card.','Spell','Common','Hunter'),('BRM_014','Core Rager',4,4,4,NULL,'<b>Battlecry:</b> If your hand is empty, gain +3/+3.','Minion','Rare','Hunter'),('BRM_015','Revenge',2,NULL,NULL,NULL,'Deal $1 damage to all minions. If you have 12 or less Health, deal $3 damage instead.','Spell','Rare','Warrior'),('BRM_016','Axe Flinger',4,2,5,NULL,'Whenever this minion takes damage, deal 2 damage to the enemy hero.','Minion','Common','Warrior'),('BRM_017','Resurrect',2,NULL,NULL,NULL,'Summon a random friendly minion that died this game.','Spell','Rare','Priest'),('BRM_018','Dragon Consort',5,5,5,NULL,'<b>Battlecry:</b> The next Dragon you play costs (2) less.','Minion','Rare','Paladin'),('BRM_019','Grim Patron',5,3,3,NULL,'Whenever this minion survives damage, summon another Grim Patron.','Minion','Rare',NULL),('BRM_020','Dragonkin Sorcerer',4,3,5,NULL,'Whenever you target this minion with a spell, gain +1/+1.','Minion','Common',NULL),('BRM_022','Dragon Egg',1,0,2,NULL,'Whenever this minion takes damage, summon a 2/1 Whelp.','Minion','Rare',NULL),('BRM_022t','Black Whelp',1,2,1,NULL,'','Minion','Common',NULL),('BRM_024','Drakonid Crusher',6,6,6,NULL,'<b>Battlecry:</b> If your opponent has 15 or less Health, gain +3/+3.','Minion','Common',NULL),('BRM_025','Volcanic Drake',6,6,4,NULL,'Costs (1) less for each minion that died this turn.','Minion','Common',NULL),('BRM_026','Hungry Dragon',4,5,6,NULL,'<b>Battlecry:</b> Summon a random 1-Cost minion for your opponent.','Minion','Common',NULL),('BRM_027','Majordomo Executus',9,9,7,NULL,'<b>Deathrattle:</b> Replace your hero with Ragnaros, the Firelord.','Minion','Legendary',NULL),('BRM_028','Emperor Thaurissan',6,5,5,NULL,'At the end of your turn, reduce the Cost of cards in your hand by (1).','Minion','Legendary',NULL),('BRM_029','Rend Blackhand',7,8,4,NULL,'<b>Battlecry:</b> If you\'re holding a Dragon, destroy a <b>Legendary</b> minion.','Minion','Legendary',NULL),('BRM_030','Nefarian',9,8,8,NULL,'<b>Battlecry:</b> Add 2 random spells to your hand <i>(from your opponent\'s class)</i>.','Minion','Legendary',NULL),('BRM_030t','Tail Swipe',4,NULL,NULL,NULL,'Deal $4 damage.','Spell',NULL,NULL),('BRM_031','Chromaggus',8,6,8,NULL,'Whenever you draw a card, put another copy into your hand.','Minion','Legendary',NULL),('BRM_033','Blackwing Technician',3,2,4,NULL,'<b>Battlecry:</b> If you\'re holding a Dragon, gain +1/+1.','Minion','Common',NULL),('BRM_034','Blackwing Corruptor',5,5,4,NULL,'<b>Battlecry</b>: If you\'re holding a Dragon, deal 3 damage.','Minion','Common',NULL),('CS1_042','Goldshire Footman',1,1,2,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS1_069','Fen Creeper',5,3,6,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS1_112','Holy Nova',5,NULL,NULL,NULL,'Deal $2 damage to all enemies. Restore #2 Health to all friendly characters.','Spell','Common','Priest'),('CS1_113','Mind Control',10,NULL,NULL,NULL,'Take control of an enemy minion.','Spell','Common','Priest'),('CS1_129','Inner Fire',1,NULL,NULL,NULL,'Change a minion\'s Attack to be equal to its Health.','Spell','Common','Priest'),('CS1_130','Holy Smite',1,NULL,NULL,NULL,'Deal $2 damage.','Spell','Free','Priest'),('CS2_003','Mind Vision',1,NULL,NULL,NULL,'Put a copy of a random card in your opponent\'s hand into your hand.','Spell','Common','Priest'),('CS2_004','Power Word: Shield',1,NULL,NULL,NULL,'Give a minion +2 Health.\nDraw a card.','Spell','Free','Priest'),('CS2_005','Claw',1,NULL,NULL,NULL,'Give your hero +2 Attack this turn and 2 Armor.','Spell','Free','Druid'),('CS2_007','Healing Touch',3,NULL,NULL,NULL,'Restore #8 Health.','Spell','Free','Druid'),('CS2_008','Moonfire',0,NULL,NULL,NULL,'Deal $1 damage.','Spell','Common','Druid'),('CS2_009','Mark of the Wild',2,NULL,NULL,NULL,'Give a minion <b>Taunt</b> and +2/+2.<i> (+2 Attack/+2 Health)</i>','Spell','Free','Druid'),('CS2_011','Savage Roar',3,NULL,NULL,NULL,'Give your characters +2 Attack this turn.','Spell','Common','Druid'),('CS2_012','Swipe',4,NULL,NULL,NULL,'Deal $4 damage to an enemy and $1 damage to all other enemies.','Spell','Common','Druid'),('CS2_013','Wild Growth',2,NULL,NULL,NULL,'Gain an empty Mana Crystal.','Spell','Free','Druid'),('CS2_013t','Excess Mana',0,NULL,NULL,NULL,'Draw a card. <i>(You can only have 10 Mana in your tray.)</i>','Spell',NULL,'Druid'),('CS2_022','Polymorph',4,NULL,NULL,NULL,'Transform a minion into a 1/1 Sheep.','Spell','Free','Mage'),('CS2_023','Arcane Intellect',3,NULL,NULL,NULL,'Draw 2 cards.','Spell','Free','Mage'),('CS2_024','Frostbolt',2,NULL,NULL,NULL,'Deal $3 damage to a character and <b>Freeze</b> it.','Spell','Common','Mage'),('CS2_025','Arcane Explosion',2,NULL,NULL,NULL,'Deal $1 damage to all enemy minions.','Spell','Free','Mage'),('CS2_026','Frost Nova',3,NULL,NULL,NULL,'<b>Freeze</b> all enemy minions.','Spell','Common','Mage'),('CS2_027','Mirror Image',1,NULL,NULL,NULL,'Summon two 0/2 minions with <b>Taunt</b>.','Spell','Common','Mage'),('CS2_028','Blizzard',6,NULL,NULL,NULL,'Deal $2 damage to all enemy minions and <b>Freeze</b> them.','Spell','Rare','Mage'),('CS2_029','Fireball',4,NULL,NULL,NULL,'Deal $6 damage.','Spell','Free','Mage'),('CS2_031','Ice Lance',1,NULL,NULL,NULL,'<b>Freeze</b> a character. If it was already <b>Frozen</b>, deal $4 damage instead.','Spell','Common','Mage'),('CS2_032','Flamestrike',7,NULL,NULL,NULL,'Deal $4 damage to all enemy minions.','Spell','Common','Mage'),('CS2_033','Water Elemental',4,3,6,NULL,'<b>Freeze</b> any character damaged by this minion.','Minion','Common','Mage'),('CS2_037','Frost Shock',1,NULL,NULL,NULL,'Deal $1 damage to an enemy character and <b>Freeze</b> it.','Spell','Free','Shaman'),('CS2_038','Ancestral Spirit',2,NULL,NULL,NULL,'Give a minion \"<b>Deathrattle:</b> Resummon this minion.\"','Spell','Rare','Shaman'),('CS2_039','Windfury',2,NULL,NULL,NULL,'Give a minion <b>Windfury</b>.','Spell','Free','Shaman'),('CS2_041','Ancestral Healing',0,NULL,NULL,NULL,'Restore a minion to full Health and give it <b>Taunt</b>.','Spell','Free','Shaman'),('CS2_042','Fire Elemental',6,6,5,NULL,'<b>Battlecry:</b> Deal 3 damage.','Minion','Common','Shaman'),('CS2_045','Rockbiter Weapon',1,NULL,NULL,NULL,'Give a friendly character +3 Attack this turn.','Spell','Free','Shaman'),('CS2_046','Bloodlust',5,NULL,NULL,NULL,'Give your minions +3 Attack this turn.','Spell','Common','Shaman'),('CS2_050','Searing Totem',1,1,1,NULL,'','Minion','Free','Shaman'),('CS2_051','Stoneclaw Totem',1,0,2,NULL,'<b>Taunt</b>','Minion','Free','Shaman'),('CS2_052','Wrath of Air Totem',1,0,2,NULL,'<b>Spell Damage +1</b>','Minion','Free','Shaman'),('CS2_053','Far Sight',3,NULL,NULL,NULL,'Draw a card. That card costs (3) less.','Spell','Epic','Shaman'),('CS2_057','Shadow Bolt',3,NULL,NULL,NULL,'Deal $4 damage to a minion.','Spell','Free','Warlock'),('CS2_059','Blood Imp',1,0,1,NULL,'<b>Stealth</b>. At the end of your turn, give another random friendly minion +1 Health.','Minion','Common','Warlock'),('CS2_061','Drain Life',3,NULL,NULL,NULL,'Deal $2 damage. Restore #2 Health to your hero.','Spell','Free','Warlock'),('CS2_062','Hellfire',4,NULL,NULL,NULL,'Deal $3 damage to ALL characters.','Spell','Free','Warlock'),('CS2_063','Corruption',1,NULL,NULL,NULL,'Choose an enemy minion. At the start of your turn, destroy it.','Spell','Common','Warlock'),('CS2_064','Dread Infernal',6,6,6,NULL,'<b>Battlecry:</b> Deal 1 damage to ALL other characters.','Minion','Common','Warlock'),('CS2_065','Voidwalker',1,1,3,NULL,'<b>Taunt</b>','Minion','Free','Warlock'),('CS2_072','Backstab',0,NULL,NULL,NULL,'Deal $2 damage to an undamaged minion.','Spell','Free','Rogue'),('CS2_073','Cold Blood',1,NULL,NULL,NULL,'Give a minion +2 Attack. <b>Combo:</b> +4 Attack instead.','Spell','Common','Rogue'),('CS2_074','Deadly Poison',1,NULL,NULL,NULL,'Give your weapon +2 Attack.','Spell','Free','Rogue'),('CS2_075','Sinister Strike',1,NULL,NULL,NULL,'Deal $3 damage to the enemy hero.','Spell','Free','Rogue'),('CS2_076','Assassinate',5,NULL,NULL,NULL,'Destroy an enemy minion.','Spell','Free','Rogue'),('CS2_077','Sprint',7,NULL,NULL,NULL,'Draw 4 cards.','Spell','Common','Rogue'),('CS2_080','Assassin\'s Blade',5,3,NULL,4,'','Weapon','Common','Rogue'),('CS2_082','Wicked Knife',1,1,NULL,2,'','Weapon','Free','Rogue'),('CS2_084','Hunter\'s Mark',0,NULL,NULL,NULL,'Change a minion\'s Health to 1.','Spell','Common','Hunter'),('CS2_087','Blessing of Might',1,NULL,NULL,NULL,'Give a minion +3 Attack.','Spell','Free','Paladin'),('CS2_088','Guardian of Kings',7,5,6,NULL,'<b>Battlecry:</b> Restore 6 Health to your hero.','Minion','Common','Paladin'),('CS2_089','Holy Light',2,NULL,NULL,NULL,'Restore #6 Health.','Spell','Free','Paladin'),('CS2_091','Light\'s Justice',1,1,NULL,4,'','Weapon','Free','Paladin'),('CS2_092','Blessing of Kings',4,NULL,NULL,NULL,'Give a minion +4/+4. <i>(+4 Attack/+4 Health)</i>','Spell','Common','Paladin'),('CS2_093','Consecration',4,NULL,NULL,NULL,'Deal $2 damage to all enemies.','Spell','Common','Paladin'),('CS2_094','Hammer of Wrath',4,NULL,NULL,NULL,'Deal $3 damage.\nDraw a card.','Spell','Free','Paladin'),('CS2_097','Truesilver Champion',4,4,NULL,2,'Whenever your hero attacks, restore 2 Health to it.','Weapon','Common','Paladin'),('CS2_101t','Silver Hand Recruit',1,1,1,NULL,'','Minion','Free','Paladin'),('CS2_103','Charge',3,NULL,NULL,NULL,'Give a friendly minion +2 Attack and <b>Charge</b>.','Spell','Free','Warrior'),('CS2_104','Rampage',2,NULL,NULL,NULL,'Give a damaged minion +3/+3.','Spell','Common','Warrior'),('CS2_105','Heroic Strike',2,NULL,NULL,NULL,'Give your hero +4 Attack this turn.','Spell','Free','Warrior'),('CS2_106','Fiery War Axe',2,3,NULL,2,'','Weapon','Free','Warrior'),('CS2_108','Execute',1,NULL,NULL,NULL,'Destroy a damaged enemy minion.','Spell','Free','Warrior'),('CS2_112','Arcanite Reaper',5,5,NULL,2,'','Weapon','Common','Warrior'),('CS2_114','Cleave',2,NULL,NULL,NULL,'Deal $2 damage to two random enemy minions.','Spell','Common','Warrior'),('CS2_117','Earthen Ring Farseer',3,3,3,NULL,'<b>Battlecry:</b> Restore 3 Health.','Minion','Common',NULL),('CS2_118','Magma Rager',3,5,1,NULL,'','Minion','Free',NULL),('CS2_119','Oasis Snapjaw',4,2,7,NULL,'','Minion','Free',NULL),('CS2_120','River Crocolisk',2,2,3,NULL,'','Minion','Free',NULL),('CS2_121','Frostwolf Grunt',2,2,2,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS2_122','Raid Leader',3,2,2,NULL,'Your other minions have +1 Attack.','Minion','Free',NULL),('CS2_124','Wolfrider',3,3,1,NULL,'<b>Charge</b>','Minion','Free',NULL),('CS2_125','Ironfur Grizzly',3,3,3,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS2_127','Silverback Patriarch',3,1,4,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS2_131','Stormwind Knight',4,2,5,NULL,'<b>Charge</b>','Minion','Common',NULL),('CS2_141','Ironforge Rifleman',3,2,2,NULL,'<b>Battlecry:</b> Deal 1 damage.','Minion','Common',NULL),('CS2_142','Kobold Geomancer',2,2,2,NULL,'<b>Spell Damage +1</b>','Minion','Common',NULL),('CS2_146','Southsea Deckhand',1,2,1,NULL,'Has <b>Charge</b> while you have a weapon equipped.','Minion','Common',NULL),('CS2_147','Gnomish Inventor',4,2,4,NULL,'<b>Battlecry:</b> Draw a card.','Minion','Common',NULL),('CS2_150','Stormpike Commando',5,4,2,NULL,'<b>Battlecry:</b> Deal 2 damage.','Minion','Common',NULL),('CS2_151','Silver Hand Knight',5,4,4,NULL,'<b>Battlecry:</b> Summon a 2/2 Squire.','Minion','Common',NULL),('CS2_152','Squire',1,2,2,NULL,'','Minion','Common',NULL),('CS2_155','Archmage',6,4,7,NULL,'<b>Spell Damage +1</b>','Minion','Common',NULL),('CS2_161','Ravenholdt Assassin',7,7,5,NULL,'<b>Stealth</b>','Minion','Rare',NULL),('CS2_162','Lord of the Arena',6,6,5,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS2_168','Murloc Raider',1,2,1,NULL,'','Minion','Free',NULL),('CS2_169','Young Dragonhawk',1,1,1,NULL,'<b>Windfury</b>','Minion','Common',NULL),('CS2_171','Stonetusk Boar',1,1,1,NULL,'<b>Charge</b>','Minion','Free',NULL),('CS2_172','Bloodfen Raptor',2,3,2,NULL,'','Minion','Free',NULL),('CS2_173','Bluegill Warrior',2,2,1,NULL,'<b>Charge</b>','Minion','Common',NULL),('CS2_179','Sen\'jin Shieldmasta',4,3,5,NULL,'<b>Taunt</b>','Minion','Free',NULL),('CS2_181','Injured Blademaster',3,4,7,NULL,'<b>Battlecry:</b> Deal 4 damage to HIMSELF.','Minion','Rare',NULL),('CS2_182','Chillwind Yeti',4,4,5,NULL,'','Minion','Common',NULL),('CS2_186','War Golem',7,7,7,NULL,'','Minion','Common',NULL),('CS2_187','Booty Bay Bodyguard',5,5,4,NULL,'<b>Taunt</b>','Minion','Common',NULL),('CS2_188','Abusive Sergeant',1,2,1,NULL,'<b>Battlecry:</b> Give a minion +2 Attack this turn.','Minion','Common',NULL),('CS2_189','Elven Archer',1,1,1,NULL,'<b>Battlecry:</b> Deal 1 damage.','Minion','Common',NULL),('CS2_196','Razorfen Hunter',3,2,3,NULL,'<b>Battlecry:</b> Summon a 1/1 Boar.','Minion','Common',NULL),('CS2_197','Ogre Magi',4,4,4,NULL,'<b>Spell Damage +1</b>','Minion','Common',NULL),('CS2_200','Boulderfist Ogre',6,6,7,NULL,'','Minion','Free',NULL),('CS2_201','Core Hound',7,9,5,NULL,'','Minion','Common',NULL),('CS2_203','Ironbeak Owl',2,2,1,NULL,'<b>Battlecry:</b> <b>Silence</b> a minion.','Minion','Common',NULL),('CS2_213','Reckless Rocketeer',6,5,2,NULL,'<b>Charge</b>','Minion','Free',NULL),('CS2_221','Spiteful Smith',5,4,6,NULL,'<b>Enrage:</b> Your weapon has +2 Attack.','Minion','Common',NULL),('CS2_222','Stormwind Champion',7,6,6,NULL,'Your other minions have +1/+1.','Minion','Common',NULL),('CS2_226','Frostwolf Warlord',5,4,4,NULL,'<b>Battlecry:</b> Gain +1/+1 for each other friendly minion on the battlefield.','Minion','Common',NULL),('CS2_227','Venture Co. Mercenary',5,7,6,NULL,'Your minions cost (3) more.','Minion','Common',NULL),('CS2_231','Wisp',0,1,1,NULL,'','Minion','Common',NULL),('CS2_232','Ironbark Protector',8,8,8,NULL,'<b>Taunt</b>','Minion','Common','Druid'),('CS2_233','Blade Flurry',2,NULL,NULL,NULL,'Destroy your weapon and deal its damage to all enemies.','Spell','Rare','Rogue'),('CS2_234','Shadow Word: Pain',2,NULL,NULL,NULL,'Destroy a minion with 3 or less Attack.','Spell','Free','Priest'),('CS2_235','Northshire Cleric',1,1,3,NULL,'Whenever a minion is healed, draw a card.','Minion','Free','Priest'),('CS2_236','Divine Spirit',2,NULL,NULL,NULL,'Double a minion\'s Health.','Spell','Common','Priest'),('CS2_237','Starving Buzzard',5,3,2,NULL,'Whenever you summon a Beast, draw a card.','Minion','Common','Hunter'),('CS2_boar','Boar',1,1,1,NULL,'','Minion','Common',NULL),('CS2_mirror','Mirror Image',0,0,2,NULL,'<b>Taunt</b>','Minion','Common','Mage'),('CS2_tk1','Sheep',0,1,1,NULL,'','Minion','Common',NULL),('DREAM_01','Laughing Sister',3,3,5,NULL,'Can\'t be targeted by spells or Hero Powers.','Minion',NULL,'Dream'),('DREAM_02','Ysera Awakens',2,NULL,NULL,NULL,'Deal $5 damage to all characters except Ysera.','Spell',NULL,'Dream'),('DREAM_03','Emerald Drake',4,7,6,NULL,'','Minion',NULL,'Dream'),('DREAM_04','Dream',0,NULL,NULL,NULL,'Return a minion to its owner\'s hand.','Spell',NULL,'Dream'),('DREAM_05','Nightmare',0,NULL,NULL,NULL,'Give a minion +5/+5. At the start of your next turn, destroy it.','Spell',NULL,'Dream'),('DS1_055','Darkscale Healer',5,4,5,NULL,'<b>Battlecry:</b> Restore 2 Health to all friendly characters.','Minion','Common',NULL),('DS1_070','Houndmaster',4,4,3,NULL,'<b>Battlecry:</b> Give a friendly Beast +2/+2 and <b>Taunt</b>.','Minion','Free','Hunter'),('DS1_175','Timber Wolf',1,1,1,NULL,'Your other Beasts have +1 Attack.','Minion','Free','Hunter'),('DS1_178','Tundra Rhino',5,2,5,NULL,'Your Beasts have <b>Charge</b>.','Minion','Common','Hunter'),('DS1_183','Multi-Shot',4,NULL,NULL,NULL,'Deal $3 damage to two random enemy minions.','Spell','Free','Hunter'),('DS1_184','Tracking',1,NULL,NULL,NULL,'Look at the top three cards of your deck. Draw one and discard the others.','Spell','Free','Hunter'),('DS1_185','Arcane Shot',1,NULL,NULL,NULL,'Deal $2 damage.','Spell','Free','Hunter'),('DS1_188','Gladiator\'s Longbow',7,5,NULL,2,'Your hero is <b>Immune</b> while attacking.','Weapon','Epic','Hunter'),('DS1_233','Mind Blast',2,NULL,NULL,NULL,'Deal $5 damage to the enemy hero.','Spell','Free','Priest'),('ds1_whelptoken','Whelp',1,1,1,NULL,'','Minion',NULL,NULL),('EX1_001','Lightwarden',1,1,2,NULL,'Whenever a character is healed, gain +2 Attack.','Minion','Rare',NULL),('EX1_002','The Black Knight',6,4,5,NULL,'<b>Battlecry:</b> Destroy an enemy minion with <b>Taunt</b>.','Minion','Legendary',NULL),('EX1_004','Young Priestess',1,2,1,NULL,'At the end of your turn, give another random friendly minion +1 Health.','Minion','Rare',NULL),('EX1_005','Big Game Hunter',3,4,2,NULL,'<b>Battlecry:</b> Destroy a minion with an Attack of 7 or more.','Minion','Epic',NULL),('EX1_006','Alarm-o-Bot',3,0,3,NULL,'At the start of your turn, swap this minion with a random one in your hand.','Minion','Rare',NULL),('EX1_007','Acolyte of Pain',3,1,3,NULL,'Whenever this minion takes damage, draw a card.','Minion','Common',NULL),('EX1_008','Argent Squire',1,1,1,NULL,'<b>Divine Shield</b>','Minion','Common',NULL),('EX1_009','Angry Chicken',1,1,1,NULL,'<b>Enrage:</b> +5 Attack.','Minion','Rare',NULL),('EX1_010','Worgen Infiltrator',1,2,1,NULL,'<b>Stealth</b>','Minion','Common',NULL),('EX1_011','Voodoo Doctor',1,2,1,NULL,'<b>Battlecry:</b> Restore 2 Health.','Minion','Free',NULL),('EX1_012','Bloodmage Thalnos',2,1,1,NULL,'<b>Spell Damage +1</b>. <b>Deathrattle:</b> Draw a card.','Minion','Legendary',NULL),('EX1_014','King Mukla',3,5,5,NULL,'<b>Battlecry:</b> Give your opponent 2 Bananas.','Minion','Legendary',NULL),('EX1_014t','Bananas',1,NULL,NULL,NULL,'Give a minion +1/+1.','Spell',NULL,NULL),('EX1_015','Novice Engineer',2,1,1,NULL,'<b>Battlecry:</b> Draw a card.','Minion','Free',NULL),('EX1_016','Sylvanas Windrunner',6,5,5,NULL,'<b>Deathrattle:</b> Take control of a random enemy minion.','Minion','Legendary',NULL),('EX1_017','Jungle Panther',3,4,2,NULL,'<b>Stealth</b>','Minion','Common',NULL),('EX1_019','Shattered Sun Cleric',3,3,2,NULL,'<b>Battlecry:</b> Give a friendly minion +1/+1.','Minion','Common',NULL),('EX1_020','Scarlet Crusader',3,3,1,NULL,'<b>Divine Shield</b>','Minion','Common',NULL),('EX1_021','Thrallmar Farseer',3,2,3,NULL,'<b>Windfury</b>','Minion','Common',NULL),('EX1_023','Silvermoon Guardian',4,3,3,NULL,'<b>Divine Shield</b>','Minion','Common',NULL),('EX1_025','Dragonling Mechanic',4,2,4,NULL,'<b>Battlecry:</b> Summon a 2/1 Mechanical Dragonling.','Minion','Common',NULL),('EX1_025t','Mechanical Dragonling',1,2,1,NULL,'','Minion','Common',NULL),('EX1_028','Stranglethorn Tiger',5,5,5,NULL,'<b>Stealth</b>','Minion','Common',NULL),('EX1_029','Leper Gnome',1,2,1,NULL,'<b>Deathrattle:</b> Deal 2 damage to the enemy hero.','Minion','Common',NULL),('EX1_032','Sunwalker',6,4,5,NULL,'<b>Taunt</b>. <b>Divine Shield</b>','Minion','Rare',NULL),('EX1_033','Windfury Harpy',6,4,5,NULL,'<b>Windfury</b>','Minion','Common',NULL),('EX1_043','Twilight Drake',4,4,1,NULL,'<b>Battlecry:</b> Gain +1 Health for each card in your hand.','Minion','Rare',NULL),('EX1_044','Questing Adventurer',3,2,2,NULL,'Whenever you play a card, gain +1/+1.','Minion','Rare',NULL),('EX1_045','Ancient Watcher',2,4,5,NULL,'Can\'t Attack.','Minion','Rare',NULL),('EX1_046','Dark Iron Dwarf',4,4,4,NULL,'<b>Battlecry:</b> Give a minion +2 Attack this turn.','Minion','Common',NULL),('EX1_048','Spellbreaker',4,4,3,NULL,'<b>Battlecry:</b> <b>Silence</b> a minion.','Minion','Common',NULL),('EX1_049','Youthful Brewmaster',2,3,2,NULL,'<b>Battlecry:</b> Return a friendly minion from the battlefield to your hand.','Minion','Common',NULL),('EX1_050','Coldlight Oracle',3,2,2,NULL,'<b>Battlecry:</b> Each player draws 2 cards.','Minion','Rare',NULL),('EX1_055','Mana Addict',2,1,3,NULL,'Whenever you cast a spell, gain +2 Attack this turn.','Minion','Rare',NULL),('EX1_057','Ancient Brewmaster',4,5,4,NULL,'<b>Battlecry:</b> Return a friendly minion from the battlefield to your hand.','Minion','Common',NULL),('EX1_058','Sunfury Protector',2,2,3,NULL,'<b>Battlecry:</b> Give adjacent minions <b>Taunt</b>.','Minion','Rare',NULL),('EX1_059','Crazed Alchemist',2,2,2,NULL,'<b>Battlecry:</b> Swap the Attack and Health of a minion.','Minion','Rare',NULL),('EX1_062','Old Murk-Eye',4,2,4,NULL,'<b>Charge</b>. Has +1 Attack for each other Murloc on the battlefield.','Minion','Legendary',NULL),('EX1_066','Acidic Swamp Ooze',2,3,2,NULL,'<b>Battlecry:</b> Destroy your opponent\'s weapon.','Minion','Common',NULL),('EX1_067','Argent Commander',6,4,2,NULL,'<b>Charge</b>, <b>Divine Shield</b>','Minion','Rare',NULL),('EX1_076','Pint-Sized Summoner',2,2,2,NULL,'The first minion you play each turn costs (1) less.','Minion','Rare',NULL),('EX1_080','Secretkeeper',1,1,2,NULL,'Whenever a <b>Secret</b> is played, gain +1/+1.','Minion','Rare',NULL),('EX1_082','Mad Bomber',2,3,2,NULL,'<b>Battlecry:</b> Deal 3 damage randomly split between all other characters.','Minion','Common',NULL),('EX1_083','Tinkmaster Overspark',3,3,3,NULL,'<b>Battlecry:</b> Transform another random minion into a 5/5 Devilsaur or a 1/1 Squirrel.','Minion','Legendary',NULL),('EX1_084','Warsong Commander',3,2,3,NULL,'Whenever you summon a minion with 3 or less Attack, give it <b>Charge</b>.','Minion','Free','Warrior'),('EX1_085','Mind Control Tech',3,3,3,NULL,'<b>Battlecry:</b> If your opponent has 4 or more minions, take control of one at random.','Minion','Rare',NULL),('EX1_089','Arcane Golem',3,4,2,NULL,'<b>Charge</b>. <b>Battlecry:</b> Give your opponent a Mana Crystal.','Minion','Rare',NULL),('EX1_091','Cabal Shadow Priest',6,4,5,NULL,'<b>Battlecry:</b> Take control of an enemy minion that has 2 or less Attack.','Minion','Epic','Priest'),('EX1_093','Defender of Argus',4,2,3,NULL,'<b>Battlecry:</b> Give adjacent minions +1/+1 and <b>Taunt</b>.','Minion','Rare',NULL),('EX1_095','Gadgetzan Auctioneer',6,4,4,NULL,'Whenever you cast a spell, draw a card.','Minion','Rare',NULL),('EX1_096','Loot Hoarder',2,2,1,NULL,'<b>Deathrattle:</b> Draw a card.','Minion','Common',NULL),('EX1_097','Abomination',5,4,4,NULL,'<b>Taunt</b>. <b>Deathrattle:</b> Deal 2 damage to ALL characters.','Minion','Rare',NULL),('EX1_100','Lorewalker Cho',2,0,4,NULL,'Whenever a player casts a spell, put a copy into the other player’s hand.','Minion','Legendary',NULL),('EX1_102','Demolisher',3,1,4,NULL,'At the start of your turn, deal 2 damage to a random enemy.','Minion','Rare',NULL),('EX1_103','Coldlight Seer',3,2,3,NULL,'<b>Battlecry:</b> Give ALL other Murlocs +2 Health.','Minion','Rare',NULL),('EX1_105','Mountain Giant',12,8,8,NULL,'Costs (1) less for each other card in your hand.','Minion','Epic',NULL),('EX1_110','Cairne Bloodhoof',6,4,5,NULL,'<b>Deathrattle:</b> Summon a 4/5 Baine Bloodhoof.','Minion','Legendary',NULL),('EX1_110t','Baine Bloodhoof',4,4,5,NULL,'','Minion','Legendary',NULL),('EX1_112','Gelbin Mekkatorque',6,6,6,NULL,'<b>Battlecry:</b> Summon an AWESOME invention.','Minion','Legendary',NULL),('EX1_116','Leeroy Jenkins',5,6,2,NULL,'<b>Charge</b>. <b>Battlecry:</b> Summon two 1/1 Whelps for your opponent.','Minion','Legendary',NULL),('EX1_116t','Whelp',1,1,1,NULL,'','Minion',NULL,NULL),('EX1_124','Eviscerate',2,NULL,NULL,NULL,'Deal $2 damage. <b>Combo:</b> Deal $4 damage instead.','Spell','Common','Rogue'),('EX1_126','Betrayal',2,NULL,NULL,NULL,'Force an enemy minion to deal its damage to the minions next to it.','Spell','Common','Rogue'),('EX1_128','Conceal',1,NULL,NULL,NULL,'Give your minions <b>Stealth</b> until your next turn.','Spell','Common','Rogue'),('EX1_129','Fan of Knives',3,NULL,NULL,NULL,'Deal $1 damage to all enemy minions. Draw a card.','Spell','Common','Rogue'),('EX1_130','Noble Sacrifice',1,NULL,NULL,NULL,'<b>Secret:</b> When an enemy attacks, summon a 2/1 Defender as the new target.','Spell','Common','Paladin'),('EX1_130a','Defender',1,2,1,NULL,'','Minion','Common','Paladin'),('EX1_131','Defias Ringleader',2,2,2,NULL,'<b>Combo:</b> Summon a 2/1 Defias Bandit.','Minion','Common','Rogue'),('EX1_131t','Defias Bandit',1,2,1,NULL,'','Minion',NULL,'Rogue'),('EX1_132','Eye for an Eye',1,NULL,NULL,NULL,'<b>Secret:</b> When your hero takes damage, deal that much damage to the enemy hero.','Spell','Common','Paladin'),('EX1_133','Perdition\'s Blade',3,2,NULL,2,'<b>Battlecry:</b> Deal 1 damage. <b>Combo:</b> Deal 2 instead.','Weapon','Rare','Rogue'),('EX1_134','SI:7 Agent',3,3,3,NULL,'<b>Combo:</b> Deal 2 damage.','Minion','Rare','Rogue'),('EX1_136','Redemption',1,NULL,NULL,NULL,'<b>Secret:</b> When one of your minions dies, return it to life with 1 Health.','Spell','Common','Paladin'),('EX1_137','Headcrack',3,NULL,NULL,NULL,'Deal $2 damage to the enemy hero. <b>Combo:</b> Return this to your hand next turn.','Spell','Rare','Rogue'),('EX1_144','Shadowstep',0,NULL,NULL,NULL,'Return a friendly minion to your hand. It costs (2) less.','Spell','Common','Rogue'),('EX1_145','Preparation',0,NULL,NULL,NULL,'The next spell you cast this turn costs (3) less.','Spell','Epic','Rogue'),('EX1_154','Wrath',2,NULL,NULL,NULL,'<b>Choose One</b> - Deal $3 damage to a minion; or $1 damage and draw a card.','Spell','Common','Druid'),('EX1_155','Mark of Nature',3,NULL,NULL,NULL,'<b>Choose One</b> - Give a minion +4 Attack; or +4 Health and <b>Taunt</b>.','Spell','Common','Druid'),('EX1_158','Soul of the Forest',4,NULL,NULL,NULL,'Give your minions \"<b>Deathrattle:</b> Summon a 2/2 Treant.\"','Spell','Common','Druid'),('EX1_158t','Treant',1,2,2,NULL,'','Minion',NULL,'Druid'),('EX1_160','Power of the Wild',2,NULL,NULL,NULL,'<b>Choose One</b> - Give your minions +1/+1; or Summon a 3/2 Panther.','Spell','Common','Druid'),('EX1_160t','Panther',2,3,2,NULL,'','Minion','Common','Druid'),('EX1_161','Naturalize',1,NULL,NULL,NULL,'Destroy a minion. Your opponent draws 2 cards.','Spell','Common','Druid'),('EX1_162','Dire Wolf Alpha',2,2,2,NULL,'Adjacent minions have +1 Attack.','Minion','Common',NULL),('EX1_164','Nourish',5,NULL,NULL,NULL,'<b>Choose One</b> - Gain 2 Mana Crystals; or Draw 3 cards.','Spell','Rare','Druid'),('EX1_165','Druid of the Claw',5,4,4,NULL,'<b>Choose One -</b> <b>Charge</b>; or +2 Health and <b>Taunt</b>.','Minion','Common','Druid'),('EX1_165t1','Druid of the Claw',5,4,4,NULL,'<b>Charge</b>','Minion','Common','Druid'),('EX1_165t2','Druid of the Claw',5,4,6,NULL,'<b>Taunt</b>','Minion','Common','Druid'),('EX1_166','Keeper of the Grove',4,2,4,NULL,'<b>Choose One</b> - Deal 2 damage; or <b>Silence</b> a minion.','Minion','Rare','Druid'),('EX1_169','Innervate',0,NULL,NULL,NULL,'Gain 2 Mana Crystals this turn only.','Spell','Free','Druid'),('EX1_170','Emperor Cobra',3,2,3,NULL,'Destroy any minion damaged by this minion.','Minion','Rare',NULL),('EX1_173','Starfire',6,NULL,NULL,NULL,'Deal $5 damage.\nDraw a card.','Spell','Common','Druid'),('EX1_178','Ancient of War',7,5,5,NULL,'<b>Choose One</b> -\n+5 Attack; or +5 Health and <b>Taunt</b>.','Minion','Epic','Druid'),('EX1_238','Lightning Bolt',1,NULL,NULL,NULL,'Deal $3 damage. <b>Overload:</b> (1)','Spell','Common','Shaman'),('EX1_241','Lava Burst',3,NULL,NULL,NULL,'Deal $5 damage. <b>Overload:</b> (2)','Spell','Rare','Shaman'),('EX1_243','Dust Devil',1,3,1,NULL,'<b>Windfury</b>. <b>Overload:</b> (2)','Minion','Common','Shaman'),('EX1_244','Totemic Might',0,NULL,NULL,NULL,'Give your Totems +2 Health.','Spell','Common','Shaman'),('EX1_245','Earth Shock',1,NULL,NULL,NULL,'<b>Silence</b> a minion, then deal $1 damage to it.','Spell','Common','Shaman'),('EX1_246','Hex',3,NULL,NULL,NULL,'Transform a minion into a 0/1 Frog with <b>Taunt</b>.','Spell','Free','Shaman'),('EX1_247','Stormforged Axe',2,2,NULL,3,'<b>Overload:</b> (1)','Weapon','Common','Shaman'),('EX1_248','Feral Spirit',3,NULL,NULL,NULL,'Summon two 2/3 Spirit Wolves with <b>Taunt</b>. <b>Overload:</b> (2)','Spell','Rare','Shaman'),('EX1_249','Baron Geddon',7,7,5,NULL,'At the end of your turn, deal 2 damage to ALL other characters.','Minion','Legendary',NULL),('EX1_250','Earth Elemental',5,7,8,NULL,'<b>Taunt</b>. <b>Overload:</b> (3)','Minion','Epic','Shaman'),('EX1_251','Forked Lightning',1,NULL,NULL,NULL,'Deal $2 damage to 2 random enemy minions. <b>Overload:</b> (2)','Spell','Common','Shaman'),('EX1_258','Unbound Elemental',3,2,4,NULL,'Whenever you play a card with <b>Overload</b>, gain +1/+1.','Minion','Common','Shaman'),('EX1_259','Lightning Storm',3,NULL,NULL,NULL,'Deal $2-$3 damage to all enemy minions. <b>Overload:</b> (2)','Spell','Rare','Shaman'),('EX1_274','Ethereal Arcanist',4,3,3,NULL,'If you control a <b>Secret</b> at the end of your turn, gain +2/+2.','Minion','Rare','Mage'),('EX1_275','Cone of Cold',4,NULL,NULL,NULL,'<b>Freeze</b> a minion and the minions next to it, and deal $1 damage to them.','Spell','Common','Mage'),('EX1_277','Arcane Missiles',1,NULL,NULL,NULL,'Deal $3 damage randomly split among all enemies.','Spell','Free','Mage'),('EX1_278','Shiv',2,NULL,NULL,NULL,'Deal $1 damage. Draw a card.','Spell','Common','Rogue'),('EX1_279','Pyroblast',10,NULL,NULL,NULL,'Deal $10 damage.','Spell','Epic','Mage'),('EX1_283','Frost Elemental',6,5,5,NULL,'<b>Battlecry:</b> <b>Freeze</b> a character.','Minion','Common',NULL),('EX1_284','Azure Drake',5,4,4,NULL,'<b>Spell Damage +1</b>. <b>Battlecry:</b> Draw a card.','Minion','Rare',NULL),('EX1_287','Counterspell',3,NULL,NULL,NULL,'<b>Secret:</b> When your opponent casts a spell, <b>Counter</b> it.','Spell','Rare','Mage'),('EX1_289','Ice Barrier',3,NULL,NULL,NULL,'<b>Secret:</b> When your hero is attacked, gain 8 Armor.','Spell','Common','Mage'),('EX1_294','Mirror Entity',3,NULL,NULL,NULL,'<b>Secret:</b> When your opponent plays a minion, summon a copy of it.','Spell','Common','Mage'),('EX1_295','Ice Block',3,NULL,NULL,NULL,'<b>Secret:</b> When your hero takes fatal damage, prevent it and become <b>Immune</b> this turn.','Spell','Epic','Mage'),('EX1_298','Ragnaros the Firelord',8,8,8,NULL,'Can\'t Attack. At the end of your turn, deal 8 damage to a random enemy.','Minion','Legendary',NULL),('EX1_301','Felguard',3,3,5,NULL,'<b>Taunt</b>. <b>Battlecry:</b> Destroy one of your Mana Crystals.','Minion','Rare','Warlock'),('EX1_302','Mortal Coil',1,NULL,NULL,NULL,'Deal $1 damage to a minion. If that kills it, draw a card.','Spell','Common','Warlock'),('EX1_303','Shadowflame',4,NULL,NULL,NULL,'Destroy a friendly minion and deal its Attack damage to all enemy minions.','Spell','Rare','Warlock'),('EX1_304','Void Terror',3,3,3,NULL,'<b>Battlecry:</b> Destroy the minions on either side of this minion and gain their Attack and Health.','Minion','Rare','Warlock'),('EX1_306','Succubus',2,4,3,NULL,'<b>Battlecry:</b> Discard a random card.','Minion','Free','Warlock'),('EX1_308','Soulfire',1,NULL,NULL,NULL,'Deal $4 damage. Discard a random card.','Spell','Common','Warlock'),('EX1_309','Siphon Soul',6,NULL,NULL,NULL,'Destroy a minion. Restore #3 Health to your hero.','Spell','Rare','Warlock'),('EX1_310','Doomguard',5,5,7,NULL,'<b>Charge</b>. <b>Battlecry:</b> Discard two random cards.','Minion','Rare','Warlock'),('EX1_312','Twisting Nether',8,NULL,NULL,NULL,'Destroy all minions.','Spell','Epic','Warlock'),('EX1_313','Pit Lord',4,5,6,NULL,'<b>Battlecry:</b> Deal 5 damage to your hero.','Minion','Epic','Warlock'),('EX1_315','Summoning Portal',4,0,4,NULL,'Your minions cost (2) less, but not less than (1).','Minion','Common','Warlock'),('EX1_316','Power Overwhelming',1,NULL,NULL,NULL,'Give a friendly minion +4/+4 until end of turn. Then, it dies. Horribly.','Spell','Common','Warlock'),('EX1_317','Sense Demons',3,NULL,NULL,NULL,'Put 2 random Demons from your deck into your hand.','Spell','Common','Warlock'),('EX1_317t','Worthless Imp',1,1,1,NULL,'<i>You are out of demons! At least there are always imps...</i>','Minion','Common','Warlock'),('EX1_319','Flame Imp',1,3,2,NULL,'<b>Battlecry:</b> Deal 3 damage to your hero.','Minion','Common','Warlock'),('EX1_320','Bane of Doom',5,NULL,NULL,NULL,'Deal $2 damage to a character. If that kills it, summon a random Demon.','Spell','Epic','Warlock'),('EX1_323','Lord Jaraxxus',9,3,15,NULL,'<b>Battlecry:</b> Destroy your hero and replace it with Lord Jaraxxus.','Minion','Legendary','Warlock'),('EX1_323w','Blood Fury',3,3,NULL,8,'','Weapon',NULL,'Warlock'),('EX1_332','Silence',0,NULL,NULL,NULL,'<b>Silence</b> a minion.','Spell','Common','Priest'),('EX1_334','Shadow Madness',4,NULL,NULL,NULL,'Gain control of an enemy minion with 3 or less Attack until end of turn.','Spell','Rare','Priest'),('EX1_335','Lightspawn',4,0,5,NULL,'This minion\'s Attack is always equal to its Health.','Minion','Common','Priest'),('EX1_339','Thoughtsteal',3,NULL,NULL,NULL,'Copy 2 cards from your opponent\'s deck and put them into your hand.','Spell','Common','Priest'),('EX1_341','Lightwell',2,0,5,NULL,'At the start of your turn, restore 3 Health to a damaged friendly character.','Minion','Rare','Priest'),('EX1_345','Mindgames',4,NULL,NULL,NULL,'Put a copy of a random minion from your opponent\'s deck into the battlefield.','Spell','Epic','Priest'),('EX1_345t','Shadow of Nothing',0,0,1,NULL,'Mindgames whiffed! Your opponent had no minions!','Minion','Epic','Priest'),('EX1_349','Divine Favor',3,NULL,NULL,NULL,'Draw cards until you have as many in hand as your opponent.','Spell','Rare','Paladin'),('EX1_350','Prophet Velen',7,7,7,NULL,'Double the damage and healing of your spells and Hero Power.','Minion','Legendary','Priest'),('EX1_354','Lay on Hands',8,NULL,NULL,NULL,'Restore #8 Health. Draw 3 cards.','Spell','Epic','Paladin'),('EX1_355','Blessed Champion',5,NULL,NULL,NULL,'Double a minion\'s Attack.','Spell','Rare','Paladin'),('EX1_360','Humility',1,NULL,NULL,NULL,'Change a minion\'s Attack to 1.','Spell','Common','Paladin'),('EX1_362','Argent Protector',2,2,2,NULL,'<b>Battlecry:</b> Give a friendly minion <b>Divine Shield</b>.','Minion','Common','Paladin'),('EX1_363','Blessing of Wisdom',1,NULL,NULL,NULL,'Choose a minion. Whenever it attacks, draw a card.','Spell','Common','Paladin'),('EX1_365','Holy Wrath',5,NULL,NULL,NULL,'Draw a card and deal damage equal to its cost.','Spell','Rare','Paladin'),('EX1_366','Sword of Justice',3,1,NULL,5,'Whenever you summon a minion, give it +1/+1 and this loses 1 Durability.','Weapon','Epic','Paladin'),('EX1_371','Hand of Protection',1,NULL,NULL,NULL,'Give a minion <b>Divine Shield</b>.','Spell','Free','Paladin'),('EX1_379','Repentance',1,NULL,NULL,NULL,'<b>Secret:</b> When your opponent plays a minion, reduce its Health to 1.','Spell','Common','Paladin'),('EX1_382','Aldor Peacekeeper',3,3,3,NULL,'<b>Battlecry:</b> Change an enemy minion\'s Attack to 1.','Minion','Rare','Paladin'),('EX1_383','Tirion Fordring',8,6,6,NULL,'<b>Divine Shield</b>. <b>Taunt</b>. <b>Deathrattle:</b> Equip a 5/3 Ashbringer.','Minion','Legendary','Paladin'),('EX1_383t','Ashbringer',5,5,NULL,3,'','Weapon','Legendary','Paladin'),('EX1_384','Avenging Wrath',6,NULL,NULL,NULL,'Deal $8 damage randomly split among all enemies.','Spell','Epic','Paladin'),('EX1_390','Tauren Warrior',3,2,3,NULL,'<b>Taunt</b>. <b>Enrage:</b> +3 Attack','Minion','Common',NULL),('EX1_391','Slam',2,NULL,NULL,NULL,'Deal $2 damage to a minion. If it survives, draw a card.','Spell','Common','Warrior'),('EX1_392','Battle Rage',2,NULL,NULL,NULL,'Draw a card for each damaged friendly character.','Spell','Common','Warrior'),('EX1_393','Amani Berserker',2,2,3,NULL,'<b>Enrage:</b> +3 Attack','Minion','Common',NULL),('EX1_396','Mogu\'shan Warden',4,1,7,NULL,'<b>Taunt</b>','Minion','Common',NULL),('EX1_398','Arathi Weaponsmith',4,3,3,NULL,'<b>Battlecry:</b> Equip a 2/2 weapon.','Minion','Common','Warrior'),('EX1_398t','Battle Axe',1,2,NULL,2,'','Weapon',NULL,'Warrior'),('EX1_399','Gurubashi Berserker',5,2,7,NULL,'Whenever this minion takes damage, gain +3 Attack.','Minion','Common',NULL),('EX1_400','Whirlwind',1,NULL,NULL,NULL,'Deal $1 damage to ALL minions.','Spell','Common','Warrior'),('EX1_402','Armorsmith',2,1,4,NULL,'Whenever a friendly minion takes damage, gain 1 Armor.','Minion','Rare','Warrior'),('EX1_405','Shieldbearer',1,0,4,NULL,'<b>Taunt</b>','Minion','Common',NULL),('EX1_407','Brawl',5,NULL,NULL,NULL,'Destroy all minions except one. <i>(chosen randomly)</i>','Spell','Epic','Warrior'),('EX1_408','Mortal Strike',4,NULL,NULL,NULL,'Deal $4 damage. If you have 12 or less Health, deal $6 instead.','Spell','Rare','Warrior'),('EX1_409','Upgrade!',1,NULL,NULL,NULL,'If you have a weapon, give it +1/+1. Otherwise equip a 1/3 weapon.','Spell','Rare','Warrior'),('EX1_409t','Heavy Axe',1,1,NULL,3,'','Weapon',NULL,'Warrior'),('EX1_410','Shield Slam',1,NULL,NULL,NULL,'Deal 1 damage to a minion for each Armor you have.','Spell','Epic','Warrior'),('EX1_411','Gorehowl',7,7,NULL,1,'Attacking a minion costs 1 Attack instead of 1 Durability.','Weapon','Epic','Warrior'),('EX1_412','Raging Worgen',3,3,3,NULL,'<b>Enrage:</b> <b>Windfury</b> and +1 Attack','Minion','Common',NULL),('EX1_414','Grommash Hellscream',8,4,9,NULL,'<b>Charge</b>\n<b>Enrage:</b> +6 Attack','Minion','Legendary','Warrior'),('EX1_506','Murloc Tidehunter',2,2,1,NULL,'<b>Battlecry:</b> Summon a 1/1 Murloc Scout.','Minion','Common',NULL),('EX1_506a','Murloc Scout',0,1,1,NULL,'','Minion','Common',NULL),('EX1_507','Murloc Warleader',3,3,3,NULL,'ALL other Murlocs have +2/+1.','Minion','Epic',NULL),('EX1_508','Grimscale Oracle',1,1,1,NULL,'ALL other Murlocs have +1 Attack.','Minion','Common',NULL),('EX1_509','Murloc Tidecaller',1,1,2,NULL,'Whenever a Murloc is summoned, gain +1 Attack.','Minion','Rare',NULL),('EX1_522','Patient Assassin',2,1,1,NULL,'<b>Stealth</b>. Destroy any minion damaged by this minion.','Minion','Epic','Rogue'),('EX1_531','Scavenging Hyena',2,2,2,NULL,'Whenever a friendly Beast dies, gain +2/+1.','Minion','Common','Hunter'),('EX1_533','Misdirection',2,NULL,NULL,NULL,'<b>Secret:</b> When a character attacks your hero, instead he attacks another random character.','Spell','Rare','Hunter'),('EX1_534','Savannah Highmane',6,6,5,NULL,'<b>Deathrattle:</b> Summon two 2/2 Hyenas.','Minion','Rare','Hunter'),('EX1_534t','Hyena',2,2,2,NULL,'','Minion','Rare','Hunter'),('EX1_536','Eaglehorn Bow',3,3,NULL,2,'Whenever a friendly <b>Secret</b> is revealed, gain +1 Durability.','Weapon','Rare','Hunter'),('EX1_537','Explosive Shot',5,NULL,NULL,NULL,'Deal $5 damage to a minion and $2 damage to adjacent ones.','Spell','Rare','Hunter'),('EX1_538','Unleash the Hounds',3,NULL,NULL,NULL,'For each enemy minion, summon a 1/1 Hound with <b>Charge</b>.','Spell','Common','Hunter'),('EX1_538t','Hound',1,1,1,NULL,'<b>Charge</b>','Minion',NULL,'Hunter'),('EX1_539','Kill Command',3,NULL,NULL,NULL,'Deal $3 damage. If you have a Beast, deal $5 damage instead.','Spell','Common','Hunter'),('EX1_543','King Krush',9,8,8,NULL,'<b>Charge</b>','Minion','Legendary','Hunter'),('EX1_544','Flare',2,NULL,NULL,NULL,'All minions lose <b>Stealth</b>. Destroy all enemy <b>Secrets</b>. Draw a card.','Spell','Rare','Hunter'),('EX1_549','Bestial Wrath',1,NULL,NULL,NULL,'Give a friendly Beast +2 Attack and <b>Immune</b> this turn.','Spell','Epic','Hunter'),('EX1_554','Snake Trap',2,NULL,NULL,NULL,'<b>Secret:</b> When one of your minions is attacked, summon three 1/1 Snakes.','Spell','Epic','Hunter'),('EX1_554t','Snake',0,1,1,NULL,'','Minion','Common','Hunter'),('EX1_556','Harvest Golem',3,2,3,NULL,'<b>Deathrattle:</b> Summon a 2/1 Damaged Golem.','Minion','Common',NULL),('EX1_557','Nat Pagle',2,0,4,NULL,'At the start of your turn, you have a 50% chance to draw an extra card.','Minion','Legendary',NULL),('EX1_558','Harrison Jones',5,5,4,NULL,'<b>Battlecry:</b> Destroy your opponent\'s weapon and draw cards equal to its Durability.','Minion','Legendary',NULL),('EX1_559','Archmage Antonidas',7,5,7,NULL,'Whenever you cast a spell, add a \'Fireball\' spell to your hand.','Minion','Legendary','Mage'),('EX1_560','Nozdormu',9,8,8,NULL,'Players only have 15 seconds to take their turns.','Minion','Legendary',NULL),('EX1_561','Alexstrasza',9,8,8,NULL,'<b>Battlecry:</b> Set a hero\'s remaining Health to 15.','Minion','Legendary',NULL),('EX1_562','Onyxia',9,8,8,NULL,'<b>Battlecry:</b> Summon 1/1 Whelps until your side of the battlefield is full.','Minion','Legendary',NULL),('EX1_563','Malygos',9,4,12,NULL,'<b>Spell Damage +5</b>','Minion','Legendary',NULL),('EX1_564','Faceless Manipulator',5,3,3,NULL,'<b>Battlecry:</b> Choose a minion and become a copy of it.','Minion','Epic',NULL),('EX1_565','Flametongue Totem',2,0,3,NULL,'Adjacent minions have +2 Attack.','Minion','Common','Shaman'),('EX1_567','Doomhammer',5,2,NULL,8,'<b>Windfury, Overload:</b> (2)','Weapon','Epic','Shaman'),('EX1_570','Bite',4,NULL,NULL,NULL,'Give your hero +4 Attack this turn and 4 Armor.','Spell','Rare','Druid'),('EX1_571','Force of Nature',6,NULL,NULL,NULL,'Summon three 2/2 Treants with <b>Charge</b> that die at the end of the turn.','Spell','Epic','Druid'),('EX1_572','Ysera',9,4,12,NULL,'At the end of your turn, add a Dream Card to your hand.','Minion','Legendary',NULL),('EX1_573','Cenarius',9,5,8,NULL,'<b>Choose One</b> - Give your other minions +2/+2; or Summon two 2/2 Treants with <b>Taunt</b>.','Minion','Legendary','Druid'),('EX1_573t','Treant',1,2,2,NULL,'<b>Taunt</b>','Minion',NULL,'Druid'),('EX1_575','Mana Tide Totem',3,0,3,NULL,'At the end of your turn, draw a card.','Minion','Rare','Shaman'),('EX1_577','The Beast',6,9,7,NULL,'<b>Deathrattle:</b> Summon a 3/3 Finkle Einhorn for your opponent.','Minion','Legendary',NULL),('EX1_578','Savagery',1,NULL,NULL,NULL,'Deal damage equal to your hero\'s Attack to a minion.','Spell','Rare','Druid'),('EX1_581','Sap',2,NULL,NULL,NULL,'Return an enemy minion to your opponent\'s hand.','Spell','Free','Rogue'),('EX1_582','Dalaran Mage',3,1,4,NULL,'<b>Spell Damage +1</b>','Minion','Common',NULL),('EX1_583','Priestess of Elune',6,5,4,NULL,'<b>Battlecry:</b> Restore 4 Health to your hero.','Minion','Common',NULL),('EX1_584','Ancient Mage',4,2,5,NULL,'<b>Battlecry:</b> Give adjacent minions <b>Spell Damage +1</b>.','Minion','Rare',NULL),('EX1_586','Sea Giant',10,8,8,NULL,'Costs (1) less for each other minion on the battlefield.','Minion','Epic',NULL),('EX1_587','Windspeaker',4,3,3,NULL,'<b>Battlecry:</b> Give a friendly minion <b>Windfury</b>.','Minion','Common','Shaman'),('EX1_590','Blood Knight',3,3,3,NULL,'<b>Battlecry:</b> All minions lose <b>Divine Shield</b>. Gain +3/+3 for each Shield lost.','Minion','Epic',NULL),('EX1_591','Auchenai Soulpriest',4,3,5,NULL,'Your cards and powers that restore Health now deal damage instead.','Minion','Rare','Priest'),('EX1_593','Nightblade',5,4,4,NULL,'<b>Battlecry: </b>Deal 3 damage to the enemy hero.','Minion','Free',NULL),('EX1_594','Vaporize',3,NULL,NULL,NULL,'<b>Secret:</b> When a minion attacks your hero, destroy it.','Spell','Rare','Mage'),('EX1_595','Cult Master',4,4,2,NULL,'Whenever one of your other minions dies, draw a card.','Minion','Common',NULL),('EX1_596','Demonfire',2,NULL,NULL,NULL,'Deal $2 damage to a minion. If it’s a friendly Demon, give it +2/+2 instead.','Spell','Common','Warlock'),('EX1_597','Imp Master',3,1,5,NULL,'At the end of your turn, deal 1 damage to this minion and summon a 1/1 Imp.','Minion','Rare',NULL),('EX1_598','Imp',1,1,1,NULL,'','Minion','Rare',NULL),('EX1_603','Cruel Taskmaster',2,2,2,NULL,'<b>Battlecry:</b> Deal 1 damage to a minion and give it +2 Attack.','Minion','Common','Warrior'),('EX1_604','Frothing Berserker',3,2,4,NULL,'Whenever a minion takes damage, gain +1 Attack.','Minion','Rare','Warrior'),('EX1_606','Shield Block',3,NULL,NULL,NULL,'Gain 5 Armor.\nDraw a card.','Spell','Common','Warrior'),('EX1_607','Inner Rage',0,NULL,NULL,NULL,'Deal $1 damage to a minion and give it +2 Attack.','Spell','Common','Warrior'),('EX1_608','Sorcerer\'s Apprentice',2,3,2,NULL,'Your spells cost (1) less.','Minion','Common','Mage'),('EX1_609','Snipe',2,NULL,NULL,NULL,'<b>Secret:</b> When your opponent plays a minion, deal $4 damage to it.','Spell','Common','Hunter'),('EX1_610','Explosive Trap',2,NULL,NULL,NULL,'<b>Secret:</b> When your hero is attacked, deal $2 damage to all enemies.','Spell','Common','Hunter'),('EX1_611','Freezing Trap',2,NULL,NULL,NULL,'<b>Secret:</b> When an enemy minion attacks, return it to its owner\'s hand and it costs (2) more.','Spell','Common','Hunter'),('EX1_612','Kirin Tor Mage',3,4,3,NULL,'<b>Battlecry:</b> The next <b>Secret</b> you play this turn costs (0).','Minion','Rare','Mage'),('EX1_613','Edwin VanCleef',3,2,2,NULL,'<b>Combo:</b> Gain +2/+2 for each card played earlier this turn.','Minion','Legendary','Rogue'),('EX1_614','Illidan Stormrage',6,7,5,NULL,'Whenever you play a card, summon a 2/1 Flame of Azzinoth.','Minion','Legendary',NULL),('EX1_614t','Flame of Azzinoth',1,2,1,NULL,'','Minion',NULL,NULL),('EX1_616','Mana Wraith',2,2,2,NULL,'ALL minions cost (1) more.','Minion','Rare',NULL),('EX1_617','Deadly Shot',3,NULL,NULL,NULL,'Destroy a random enemy minion.','Spell','Common','Hunter'),('EX1_619','Equality',2,NULL,NULL,NULL,'Change the Health of ALL minions to 1.','Spell','Rare','Paladin'),('EX1_620','Molten Giant',20,8,8,NULL,'Costs (1) less for each damage your hero has taken.','Minion','Epic',NULL),('EX1_621','Circle of Healing',0,NULL,NULL,NULL,'Restore #4 Health to ALL minions.','Spell','Common','Priest'),('EX1_622','Shadow Word: Death',3,NULL,NULL,NULL,'Destroy a minion with an Attack of 5 or more.','Spell','Common','Priest'),('EX1_623','Temple Enforcer',6,6,6,NULL,'<b>Battlecry:</b> Give a friendly minion +3 Health.','Minion','Common','Priest'),('EX1_624','Holy Fire',6,NULL,NULL,NULL,'Deal $5 damage. Restore #5 Health to your hero.','Spell','Rare','Priest'),('EX1_625','Shadowform',3,NULL,NULL,NULL,'Your Hero Power becomes \'Deal 2 damage\'. If already in Shadowform: 3 damage.','Spell','Epic','Priest'),('EX1_626','Mass Dispel',4,NULL,NULL,NULL,'<b>Silence</b> all enemy minions. Draw a card.','Spell','Rare','Priest'),('EX1_finkle','Finkle Einhorn',2,3,3,NULL,'','Minion','Legendary',NULL),('EX1_tk11','Spirit Wolf',2,2,3,NULL,'<b>Taunt</b>','Minion','Rare','Shaman'),('EX1_tk28','Squirrel',1,1,1,NULL,'','Minion','Common',NULL),('EX1_tk29','Devilsaur',5,5,5,NULL,'','Minion','Common',NULL),('EX1_tk34','Infernal',6,6,6,NULL,'','Minion','Common','Warlock'),('EX1_tk9','Treant',1,2,2,NULL,'<b>Charge</b>.  At the end of the turn, destroy this minion.','Minion','Common','Druid'),('FP1_001','Zombie Chow',1,2,3,NULL,'<b>Deathrattle:</b> Restore 5 Health to the enemy hero.','Minion','Common',NULL),('FP1_002','Haunted Creeper',2,1,2,NULL,'<b>Deathrattle:</b> Summon two 1/1 Spectral Spiders.','Minion','Common',NULL),('FP1_002t','Spectral Spider',1,1,1,NULL,'','Minion',NULL,NULL),('FP1_003','Echoing Ooze',2,1,2,NULL,'<b>Battlecry:</b> Summon an exact copy of this minion at the end of the turn.','Minion','Epic',NULL),('FP1_004','Mad Scientist',2,2,2,NULL,'<b>Deathrattle:</b> Put a <b>Secret</b> from your deck into the battlefield.','Minion','Common',NULL),('FP1_005','Shade of Naxxramas',3,2,2,NULL,'<b>Stealth.</b> At the start of your turn, gain +1/+1.','Minion','Epic',NULL),('FP1_006','Deathcharger',1,2,3,NULL,'<b>Charge. Deathrattle:</b> Deal 3 damage to your hero.','Minion',NULL,NULL),('FP1_007','Nerubian Egg',2,0,2,NULL,'<b>Deathrattle:</b> Summon a 4/4 Nerubian.','Minion','Rare',NULL),('FP1_007t','Nerubian',3,4,4,NULL,'','Minion','Rare',NULL),('FP1_008','Spectral Knight',5,4,6,NULL,'Can\'t be targeted by spells or Hero Powers.','Minion','Common',NULL),('FP1_009','Deathlord',3,2,8,NULL,'<b>Taunt. Deathrattle:</b> Your opponent puts a minion from their deck into the battlefield.','Minion','Rare',NULL),('FP1_010','Maexxna',6,2,8,NULL,'Destroy any minion damaged by this minion.','Minion','Legendary',NULL),('FP1_011','Webspinner',1,1,1,NULL,'<b>Deathrattle:</b> Add a random Beast card to your hand.','Minion','Common','Hunter'),('FP1_012','Sludge Belcher',5,3,5,NULL,'<b>Taunt.\nDeathrattle:</b> Summon a 1/2 Slime with <b>Taunt</b>.','Minion','Rare',NULL),('FP1_012t','Slime',1,1,2,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('FP1_013','Kel\'Thuzad',8,6,8,NULL,'At the end of each turn, summon all friendly minions that died this turn.','Minion','Legendary',NULL),('FP1_014','Stalagg',5,7,4,NULL,'<b>Deathrattle:</b> If Feugen also died this game, summon Thaddius.','Minion','Legendary',NULL),('FP1_014t','Thaddius',10,11,11,NULL,'','Minion','Legendary',NULL),('FP1_015','Feugen',5,4,7,NULL,'<b>Deathrattle:</b> If Stalagg also died this game, summon Thaddius.','Minion','Legendary',NULL),('FP1_016','Wailing Soul',4,3,5,NULL,'<b>Battlecry: Silence</b> your other minions.','Minion','Rare',NULL),('FP1_017','Nerub\'ar Weblord',2,1,4,NULL,'Minions with <b>Battlecry</b> cost (2) more.','Minion','Common',NULL),('FP1_018','Duplicate',3,NULL,NULL,NULL,'<b>Secret:</b> When a friendly minion dies, put 2 copies of it into your hand.','Spell','Common','Mage'),('FP1_019','Poison Seeds',4,NULL,NULL,NULL,'Destroy all minions and summon 2/2 Treants to replace them.','Spell','Common','Druid'),('FP1_019t','Treant',1,2,2,NULL,'','Minion',NULL,'Druid'),('FP1_020','Avenge',1,NULL,NULL,NULL,'<b>Secret:</b> When one of your minions dies, give a random friendly minion +3/+2.','Spell','Common','Paladin'),('FP1_021','Death\'s Bite',4,4,NULL,2,'<b>Deathrattle:</b> Deal 1 damage to all minions.','Weapon','Common','Warrior'),('FP1_022','Voidcaller',4,3,4,NULL,'<b>Deathrattle:</b> Put a random Demon from your hand into the battlefield.','Minion','Common','Warlock'),('FP1_023','Dark Cultist',3,3,4,NULL,'<b>Deathrattle:</b> Give a random friendly minion +3 Health.','Minion','Common','Priest'),('FP1_024','Unstable Ghoul',2,1,3,NULL,'<b>Taunt</b>. <b>Deathrattle:</b> Deal 1 damage to all minions.','Minion','Common',NULL),('FP1_025','Reincarnate',2,NULL,NULL,NULL,'Destroy a minion, then return it to life with full Health.','Spell','Common','Shaman'),('FP1_026','Anub\'ar Ambusher',4,5,5,NULL,'<b>Deathrattle:</b> Return a random friendly minion to your hand.','Minion','Common','Rogue'),('FP1_027','Stoneskin Gargoyle',3,1,4,NULL,'At the start of your turn, restore this minion to full Health.','Minion','Common',NULL),('FP1_028','Undertaker',1,1,2,NULL,'Whenever you summon a minion with <b>Deathrattle</b>, gain +1 Attack.','Minion','Common',NULL),('FP1_029','Dancing Swords',3,4,4,NULL,'<b>Deathrattle:</b> Your opponent draws a card.','Minion','Common',NULL),('FP1_030','Loatheb',5,5,5,NULL,'<b>Battlecry:</b> Enemy spells cost (5) more next turn.','Minion','Legendary',NULL),('FP1_031','Baron Rivendare',4,1,7,NULL,'Your minions trigger their <b>Deathrattles</b> twice.','Minion','Legendary',NULL),('GAME_002','Avatar of the Coin',0,1,1,NULL,'<i>You lost the coin flip, but gained a friend.</i>','Minion','Free',NULL),('GAME_006','NOOOOOOOOOOOO',2,NULL,NULL,NULL,'Somehow, the card you USED to have has been deleted.  Here, have this one instead!','Spell',NULL,NULL),('GVG_001','Flamecannon',2,NULL,NULL,NULL,'Deal $4 damage to a random enemy minion.','Spell','Common','Mage'),('GVG_002','Snowchugger',2,2,3,NULL,'<b>Freeze</b> any character damaged by this minion.','Minion','Common','Mage'),('GVG_003','Unstable Portal',2,NULL,NULL,NULL,'Add a random minion to your hand. It costs (3) less.','Spell','Rare','Mage'),('GVG_004','Goblin Blastmage',4,5,4,NULL,'<b>Battlecry:</b> If you have a Mech, deal 4 damage randomly split among all enemies.','Minion','Rare','Mage'),('GVG_005','Echo of Medivh',4,NULL,NULL,NULL,'Put a copy of each friendly minion into your hand.','Spell','Epic','Mage'),('GVG_006','Mechwarper',2,2,3,NULL,'Your Mechs cost (1) less.','Minion','Common',NULL),('GVG_007','Flame Leviathan',7,7,7,NULL,'When you draw this, deal 2 damage to all characters.','Minion','Legendary','Mage'),('GVG_008','Lightbomb',6,NULL,NULL,NULL,'Deal damage to each minion equal to its Attack.','Spell','Epic','Priest'),('GVG_009','Shadowbomber',1,2,1,NULL,'<b>Battlecry:</b> Deal 3 damage to each hero.','Minion','Epic','Priest'),('GVG_010','Velen\'s Chosen',3,NULL,NULL,NULL,'Give a minion +2/+4 and <b>Spell Damage +1</b>.','Spell','Common','Priest'),('GVG_011','Shrinkmeister',2,3,2,NULL,'<b>Battlecry:</b> Give a minion -2 Attack this turn.','Minion','Common','Priest'),('GVG_012','Light of the Naaru',1,NULL,NULL,NULL,'Restore #3 Health. If the target is still damaged, summon a Lightwarden.','Spell','Rare','Priest'),('GVG_013','Cogmaster',1,1,2,NULL,'Has +2 Attack while you have a Mech.','Minion','Common',NULL),('GVG_014','Vol\'jin',5,6,2,NULL,'<b>Battlecry:</b> Swap Health with another minion.','Minion','Legendary','Priest'),('GVG_015','Darkbomb',2,NULL,NULL,NULL,'Deal $3 damage.','Spell','Common','Warlock'),('GVG_016','Fel Reaver',5,8,8,NULL,'Whenever your opponent plays a card, discard the top 3 cards of your deck.','Minion','Epic',NULL),('GVG_017','Call Pet',2,NULL,NULL,NULL,'Draw a card.\nIf it\'s a Beast, it costs (4) less.','Spell','Rare','Hunter'),('GVG_018','Mistress of Pain',2,1,4,NULL,'Whenever this minion deals damage, restore that much Health to your hero.','Minion','Rare','Warlock'),('GVG_019','Demonheart',5,NULL,NULL,NULL,'Deal $5 damage to a minion.  If it\'s a friendly Demon, give it +5/+5 instead.','Spell','Epic','Warlock'),('GVG_020','Fel Cannon',4,3,5,NULL,'At the end of your turn, deal 2 damage to a non-Mech minion.','Minion','Rare','Warlock'),('GVG_021','Mal\'Ganis',9,9,7,NULL,'Your other Demons have +2/+2.\nYour hero is <b>Immune</b>.','Minion','Legendary','Warlock'),('GVG_022','Tinker\'s Sharpsword Oil',4,NULL,NULL,NULL,'Give your weapon +3 Attack. <b>Combo:</b> Give a random friendly minion +3 Attack.','Spell','Common','Rogue'),('GVG_023','Goblin Auto-Barber',2,3,2,NULL,'<b>Battlecry</b>: Give your weapon +1 Attack.','Minion','Common','Rogue'),('GVG_024','Cogmaster\'s Wrench',3,1,NULL,3,'Has +2 Attack while you have a Mech.','Weapon','Epic','Rogue'),('GVG_025','One-eyed Cheat',2,4,1,NULL,'Whenever you summon a Pirate, gain <b>Stealth</b>.','Minion','Rare','Rogue'),('GVG_026','Feign Death',2,NULL,NULL,NULL,'Trigger all <b>Deathrattles</b> on your minions.','Spell','Epic','Hunter'),('GVG_027','Iron Sensei',3,2,2,NULL,'At the end of your turn, give another friendly Mech +2/+2.','Minion','Rare','Rogue'),('GVG_028','Trade Prince Gallywix',6,5,8,NULL,'Whenever your opponent casts a spell, gain a copy of it and give them a Coin.','Minion','Legendary','Rogue'),('GVG_028t','Gallywix\'s Coin',0,NULL,NULL,NULL,'Gain 1 Mana Crystal this turn only.\n<i>(Won\'t trigger Gallywix.)</i>','Spell',NULL,NULL),('GVG_029','Ancestor\'s Call',4,NULL,NULL,NULL,'Put a random minion from each player\'s hand into the battlefield.','Spell','Epic','Shaman'),('GVG_030','Anodized Robo Cub',2,2,2,NULL,'<b>Taunt</b>. <b>Choose One -</b>\n+1 Attack; or +1 Health.','Minion','Common','Druid'),('GVG_031','Recycle',6,NULL,NULL,NULL,'Shuffle an enemy minion into your opponent\'s deck.','Spell','Rare','Druid'),('GVG_032','Grove Tender',3,2,4,NULL,'<b>Choose One -</b> Give each player a Mana Crystal; or Each player draws a card.','Minion','Rare','Druid'),('GVG_033','Tree of Life',9,NULL,NULL,NULL,'Restore all characters to full Health.','Spell','Epic','Druid'),('GVG_034','Mech-Bear-Cat',6,7,6,NULL,'Whenever this minion takes damage, add a <b>Spare Part</b> card to your hand.','Minion','Rare','Druid'),('GVG_035','Malorne',7,9,7,NULL,'<b>Deathrattle:</b> Shuffle this minion into your deck.','Minion','Legendary','Druid'),('GVG_036','Powermace',3,3,NULL,2,'<b>Deathrattle</b>: Give a random friendly Mech +2/+2.','Weapon','Rare','Shaman'),('GVG_037','Whirling Zap-o-matic',2,3,2,NULL,'<b>Windfury</b>','Minion','Common','Shaman'),('GVG_038','Crackle',2,NULL,NULL,NULL,'Deal $3-$6 damage. <b>Overload:</b> (1)','Spell','Common','Shaman'),('GVG_039','Vitality Totem',2,0,3,NULL,'At the end of your turn, restore 4 Health to your hero.','Minion','Rare','Shaman'),('GVG_040','Siltfin Spiritwalker',4,2,5,NULL,'Whenever another friendly Murloc dies, draw a card. <b>Overload</b>: (1)','Minion','Epic','Shaman'),('GVG_041','Dark Wispers',6,NULL,NULL,NULL,'<b>Choose One -</b> Summon 5 Wisps; or Give a minion +5/+5 and <b>Taunt</b>.','Spell','Epic','Druid'),('GVG_042','Neptulon',7,7,7,NULL,'<b>Battlecry:</b> Add 4 random Murlocs to your hand. <b>Overload:</b> (3)','Minion','Legendary','Shaman'),('GVG_043','Glaivezooka',2,2,NULL,2,'<b>Battlecry:</b> Give a random friendly minion +1 Attack.','Weapon','Common','Hunter'),('GVG_044','Spider Tank',3,3,4,NULL,'','Minion','Common',NULL),('GVG_045','Imp-losion',4,NULL,NULL,NULL,'Deal $2-$4 damage to a minion. Summon a 1/1 Imp for each damage dealt.','Spell','Rare','Warlock'),('GVG_045t','Imp',1,1,1,NULL,'','Minion',NULL,'Warlock'),('GVG_046','King of Beasts',5,2,6,NULL,'<b>Taunt</b>. <b>Battlecry:</b> Gain +1 Attack for each other Beast you have.','Minion','Rare','Hunter'),('GVG_047','Sabotage',4,NULL,NULL,NULL,'Destroy a random enemy minion. <b>Combo</b>: And your opponent\'s weapon.','Spell','Epic','Rogue'),('GVG_048','Metaltooth Leaper',3,3,3,NULL,'<b>Battlecry</b>: Give your other Mechs +2 Attack.','Minion','Rare','Hunter'),('GVG_049','Gahz\'rilla',7,6,9,NULL,'Whenever this minion takes damage, double its Attack.','Minion','Legendary','Hunter'),('GVG_050','Bouncing Blade',3,NULL,NULL,NULL,'Deal $1 damage to a random minion. Repeat until a minion dies.','Spell','Epic','Warrior'),('GVG_051','Warbot',1,1,3,NULL,'<b>Enrage:</b> +1 Attack.','Minion','Common','Warrior'),('GVG_052','Crush',7,NULL,NULL,NULL,'Destroy a minion. If you have a damaged minion, this costs (4) less.','Spell','Epic','Warrior'),('GVG_053','Shieldmaiden',6,5,5,NULL,'<b>Battlecry:</b> Gain 5 Armor.','Minion','Rare','Warrior'),('GVG_054','Ogre Warmaul',3,4,NULL,2,'50% chance to attack the wrong enemy.','Weapon','Common','Warrior'),('GVG_055','Screwjank Clunker',4,2,5,NULL,'<b>Battlecry</b>: Give a friendly Mech +2/+2.','Minion','Rare','Warrior'),('GVG_056','Iron Juggernaut',6,6,5,NULL,'<b>Battlecry:</b> Shuffle a Mine into your opponent\'s deck. When drawn, it explodes for 10 damage.','Minion','Legendary','Warrior'),('GVG_056t','Burrowing Mine',0,NULL,NULL,NULL,'When you draw this, it explodes. You take 10 damage and draw a card.','Spell',NULL,'Warrior'),('GVG_057','Seal of Light',2,NULL,NULL,NULL,'Restore #4 Health to your hero and gain +2 Attack this turn.','Spell','Common','Paladin'),('GVG_058','Shielded Minibot',2,2,2,NULL,'<b>Divine Shield</b>','Minion','Common','Paladin'),('GVG_059','Coghammer',3,2,NULL,3,'<b>Battlecry:</b> Give a random friendly minion <b>Divine Shield</b> and <b>Taunt</b>.','Weapon','Epic','Paladin'),('GVG_060','Quartermaster',5,2,5,NULL,'<b>Battlecry:</b> Give your Silver Hand Recruits +2/+2.','Minion','Epic','Paladin'),('GVG_061','Muster for Battle',3,NULL,NULL,NULL,'Summon three 1/1 Silver Hand Recruits. Equip a 1/4 Weapon.','Spell','Rare','Paladin'),('GVG_062','Cobalt Guardian',5,6,3,NULL,'Whenever you summon a Mech, gain <b>Divine Shield</b>.','Minion','Rare','Paladin'),('GVG_063','Bolvar Fordragon',5,1,7,NULL,'Whenever a friendly minion dies while this is in your hand, gain +1 Attack.','Minion','Legendary','Paladin'),('GVG_064','Puddlestomper',2,3,2,NULL,'','Minion','Common',NULL),('GVG_065','Ogre Brute',3,4,4,NULL,'50% chance to attack the wrong enemy.','Minion','Common',NULL),('GVG_066','Dunemaul Shaman',4,5,4,NULL,'<b>Windfury, Overload: (1)</b>\n50% chance to attack the wrong enemy.','Minion','Rare','Shaman'),('GVG_067','Stonesplinter Trogg',2,2,3,NULL,'Whenever your opponent casts a spell, gain +1 Attack.','Minion','Common',NULL),('GVG_068','Burly Rockjaw Trogg',4,3,5,NULL,'Whenever your opponent casts a spell, gain +2 Attack.','Minion','Common',NULL),('GVG_069','Antique Healbot',5,3,3,NULL,'<b>Battlecry:</b> Restore 8 Health to your hero.','Minion','Common',NULL),('GVG_070','Salty Dog',5,7,4,NULL,'','Minion','Common',NULL),('GVG_071','Lost Tallstrider',4,5,4,NULL,'','Minion','Common',NULL),('GVG_072','Shadowboxer',2,2,3,NULL,'Whenever a character is healed, deal 1 damage to a random enemy.','Minion','Rare','Priest'),('GVG_073','Cobra Shot',5,NULL,NULL,NULL,'Deal $3 damage to a minion and the enemy hero.','Spell','Common','Hunter'),('GVG_074','Kezan Mystic',4,4,3,NULL,'<b>Battlecry:</b> Take control of a random enemy <b>Secret</b>.','Minion','Rare',NULL),('GVG_075','Ship\'s Cannon',2,2,3,NULL,'Whenever you summon a Pirate, deal 2 damage to a random enemy.','Minion','Common',NULL),('GVG_076','Explosive Sheep',2,1,1,NULL,'<b>Deathrattle:</b> Deal 2 damage to all minions.','Minion','Common',NULL),('GVG_077','Anima Golem',6,9,9,NULL,'At the end of each turn, destroy this minion if it\'s your only one.','Minion','Epic','Warlock'),('GVG_078','Mechanical Yeti',4,4,5,NULL,'<b>Deathrattle:</b> Give each player a <b>Spare Part.</b>','Minion','Common',NULL),('GVG_079','Force-Tank MAX',8,7,7,NULL,'<b>Divine Shield</b>','Minion','Common',NULL),('GVG_080','Druid of the Fang',5,4,4,NULL,'<b>Battlecry:</b> If you have a Beast, transform this minion into a 7/7.','Minion','Common','Druid'),('GVG_080t','Druid of the Fang',5,7,7,NULL,'','Minion',NULL,'Druid'),('GVG_081','Gilblin Stalker',2,2,3,NULL,'<b>Stealth</b>','Minion','Common',NULL),('GVG_082','Clockwork Gnome',1,2,1,NULL,'<b>Deathrattle:</b> Add a <b>Spare Part</b> card to your hand.','Minion','Common',NULL),('GVG_083','Upgraded Repair Bot',5,5,5,NULL,'<b>Battlecry:</b> Give a friendly Mech +4 Health.','Minion','Rare','Priest'),('GVG_084','Flying Machine',3,1,4,NULL,'<b>Windfury</b>','Minion','Common',NULL),('GVG_085','Annoy-o-Tron',2,1,2,NULL,'<b>Taunt</b>\n<b>Divine Shield</b>','Minion','Common',NULL),('GVG_086','Siege Engine',5,5,5,NULL,'Whenever you gain Armor, give this minion +1 Attack.','Minion','Rare','Warrior'),('GVG_087','Steamwheedle Sniper',2,2,3,NULL,'Your Hero Power can target minions.','Minion','Epic','Hunter'),('GVG_088','Ogre Ninja',5,6,6,NULL,'<b>Stealth</b>\n50% chance to attack the wrong enemy.','Minion','Rare','Rogue'),('GVG_089','Illuminator',3,2,4,NULL,'If you control a <b>Secret</b> at the end of your turn, restore 4 health to your hero.','Minion','Rare',NULL),('GVG_090','Madder Bomber',5,5,4,NULL,'<b>Battlecry:</b> Deal 6 damage randomly split between all other characters.','Minion','Rare',NULL),('GVG_091','Arcane Nullifier X-21',4,2,5,NULL,'<b>Taunt</b>\nCan\'t be targeted by spells or Hero Powers.','Minion','Rare',NULL),('GVG_092','Gnomish Experimenter',3,3,2,NULL,'<b>Battlecry:</b> Draw a card. If it\'s a minion, transform it into a Chicken.','Minion','Rare',NULL),('GVG_092t','Chicken',1,1,1,NULL,'','Minion',NULL,NULL),('GVG_093','Target Dummy',0,0,2,NULL,'<b>Taunt</b>','Minion','Rare',NULL),('GVG_094','Jeeves',4,1,4,NULL,'At the end of each player\'s turn, that player draws until they have 3 cards.','Minion','Rare',NULL),('GVG_095','Goblin Sapper',3,2,4,NULL,'Has +4 Attack while your opponent has 6 or more cards in hand.','Minion','Rare',NULL),('GVG_096','Piloted Shredder',4,4,3,NULL,'<b>Deathrattle:</b> Summon a random 2-Cost minion.','Minion','Common',NULL),('GVG_097','Lil\' Exorcist',3,2,3,NULL,'<b>Taunt</b>\n<b>Battlecry:</b> Gain +1/+1 for each enemy <b>Deathrattle</b> minion.','Minion','Rare',NULL),('GVG_098','Gnomeregan Infantry',3,1,4,NULL,'<b>Charge</b>\n<b>Taunt</b>','Minion','Common',NULL),('GVG_099','Bomb Lobber',5,3,3,NULL,'<b>Battlecry:</b> Deal 4 damage to a random enemy minion.','Minion','Rare',NULL),('GVG_100','Floating Watcher',5,4,4,NULL,'Whenever your hero takes damage on your turn, gain +2/+2.','Minion','Common','Warlock'),('GVG_101','Scarlet Purifier',3,4,3,NULL,'<b>Battlecry</b>: Deal 2 damage to all minions with <b>Deathrattle</b>.','Minion','Rare','Paladin'),('GVG_102','Tinkertown Technician',3,3,3,NULL,'<b>Battlecry:</b> If you have a Mech, gain +1/+1 and add a <b>Spare Part</b> to your hand.','Minion','Common',NULL),('GVG_103','Micro Machine',2,1,2,NULL,'At the start of each turn, gain +1 Attack.','Minion','Common',NULL),('GVG_104','Hobgoblin',3,2,3,NULL,'Whenever you play a 1-Attack minion, give it +2/+2.','Minion','Epic',NULL),('GVG_105','Piloted Sky Golem',6,6,4,NULL,'<b>Deathrattle:</b> Summon a random 4-Cost minion.','Minion','Epic',NULL),('GVG_106','Junkbot',5,1,5,NULL,'Whenever a friendly Mech dies, gain +2/+2.','Minion','Epic',NULL),('GVG_107','Enhance-o Mechano',4,3,2,NULL,'<b>Battlecry:</b> Give your other minions <b>Windfury</b>, <b>Taunt</b>, or <b>Divine Shield</b>.\n<i>(at random)</i>','Minion','Epic',NULL),('GVG_108','Recombobulator',2,3,2,NULL,'<b>Battlecry:</b> Transform a friendly minion into a random minion with the same Cost.','Minion','Epic',NULL),('GVG_109','Mini-Mage',4,4,1,NULL,'<b>Stealth</b>\n<b>Spell Damage +1</b>','Minion','Epic',NULL),('GVG_110','Dr. Boom',7,7,7,NULL,'<b>Battlecry</b>: Summon two 1/1 Boom Bots. <i>WARNING: Bots may explode.</i>','Minion','Legendary',NULL),('GVG_110t','Boom Bot',1,1,1,NULL,'<b>Deathrattle</b>: Deal 1-4 damage to a random enemy.','Minion',NULL,NULL),('GVG_111','Mimiron\'s Head',5,4,5,NULL,'At the start of your turn, if you have at least 3 Mechs, destroy them all and form V-07-TR-0N.','Minion','Legendary',NULL),('GVG_111t','V-07-TR-0N',8,4,8,NULL,'<b>Charge</b>\n<b>Mega-Windfury</b> <i>(Can attack four times a turn.)</i>','Minion','Legendary',NULL),('GVG_112','Mogor the Ogre',6,7,6,NULL,'All minions have a 50% chance to attack the wrong enemy.','Minion','Legendary',NULL),('GVG_113','Foe Reaper 4000',8,6,9,NULL,'Also damages the minions next to whomever he attacks.','Minion','Legendary',NULL),('GVG_114','Sneed\'s Old Shredder',8,5,7,NULL,'<b>Deathrattle:</b> Summon a random legendary minion.','Minion','Legendary',NULL),('GVG_115','Toshley',6,5,7,NULL,'<b>Battlecry and Deathrattle:</b> Add a <b>Spare Part</b> card to your hand.','Minion','Legendary',NULL),('GVG_116','Mekgineer Thermaplugg',9,9,7,NULL,'Whenever an enemy minion dies, summon a Leper Gnome.','Minion','Legendary',NULL),('GVG_117','Gazlowe',6,3,6,NULL,'Whenever you cast a 1-mana spell, add a random Mech to your hand.','Minion','Legendary',NULL),('GVG_118','Troggzor the Earthinator',7,6,6,NULL,'Whenever your opponent casts a spell, summon a Burly Rockjaw Trogg.','Minion','Legendary',NULL),('GVG_119','Blingtron 3000',5,3,4,NULL,'<b>Battlecry:</b> Equip a random weapon for each player.','Minion','Legendary',NULL),('GVG_120','Hemet Nesingwary',5,6,3,NULL,'<b>Battlecry:</b> Destroy a Beast.','Minion','Legendary',NULL),('GVG_121','Clockwork Giant',12,8,8,NULL,'Costs (1) less for each card in your opponent\'s hand.','Minion','Epic',NULL),('GVG_122','Wee Spellstopper',4,2,5,NULL,'Adjacent minions can\'t be targeted by spells or Hero Powers.','Minion','Epic','Mage'),('GVG_123','Soot Spewer',3,3,3,NULL,'<b>Spell Damage +1</b>','Minion','Rare','Mage'),('hexfrog','Frog',0,0,1,NULL,'<b>Taunt</b>','Minion','Common',NULL),('Mekka1','Homing Chicken',1,0,1,NULL,'At the start of your turn, destroy this minion and draw 3 cards.','Minion','Common',NULL),('Mekka2','Repair Bot',1,0,3,NULL,'At the end of your turn, restore 6 Health to a damaged character.','Minion','Common',NULL),('Mekka3','Emboldener 3000',1,0,4,NULL,'At the end of your turn, give a random minion +1/+1.','Minion','Common',NULL),('Mekka4','Poultryizer',1,0,3,NULL,'At the start of your turn, transform a random minion into a 1/1 Chicken.','Minion','Common',NULL),('Mekka4t','Chicken',0,1,1,NULL,'<i>Hey Chicken!</i>','Minion',NULL,NULL),('NAX10_02','Hook',3,5,NULL,8,'<b>Deathrattle:</b> Put this weapon into your hand.','Weapon',NULL,NULL),('NAX10_02H','Hook',3,4,NULL,8,'<b>Windfury</b>\n<b>Deathrattle:</b> Put this weapon into your hand.','Weapon',NULL,NULL),('NAX11_03','Fallout Slime',1,2,2,NULL,'','Minion',NULL,NULL),('NAX11_04','Mutating Injection',3,NULL,NULL,NULL,'Give a minion +4/+4 and <b>Taunt</b>.','Spell',NULL,NULL),('NAX12_03','Jaws',1,1,NULL,5,'Whenever a minion with <b>Deathrattle</b> dies, gain +2 Attack.','Weapon',NULL,NULL),('NAX12_03H','Jaws',1,3,NULL,5,'Whenever a minion with <b>Deathrattle</b> dies, gain +2 Attack.','Weapon',NULL,NULL),('NAX12_04','Enrage',3,NULL,NULL,NULL,'Give your hero +6 Attack this turn.','Spell',NULL,NULL),('NAX13_03','Supercharge',2,NULL,NULL,NULL,'Give your minions +2 Health.','Spell',NULL,NULL),('NAX13_04H','Feugen',5,4,7,NULL,'','Minion','Legendary',NULL),('NAX13_05H','Stalagg',5,7,4,NULL,'','Minion','Legendary',NULL),('NAX14_03','Frozen Champion',5,2,10,NULL,'Permanently Frozen.  Adjacent minions are Immune to Frost Breath.','Minion',NULL,NULL),('NAX14_04','Pure Cold',5,NULL,NULL,NULL,'Deal $8 damage to the enemy hero, and <b>Freeze</b> it.','Spell',NULL,NULL),('NAX15_03n','Guardian of Icecrown',4,3,3,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('NAX15_03t','Guardian of Icecrown',4,5,5,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('NAX15_05','Mr. Bigglesworth',0,1,1,NULL,'<i>This is Kel\'Thuzad\'s kitty.</i>','Minion','Legendary',NULL),('NAX1h_03','Nerubian',2,4,4,NULL,'','Minion',NULL,NULL),('NAX1_03','Nerubian',2,3,1,NULL,'','Minion',NULL,NULL),('NAX1_05','Locust Swarm',7,NULL,NULL,NULL,'Deal $3 damage to all enemy minions. Restore #3 Health to your hero.','Spell',NULL,NULL),('NAX2_05','Worshipper',3,1,4,NULL,'Your hero has +1 Attack on your turn.','Minion',NULL,NULL),('NAX2_05H','Worshipper',3,2,4,NULL,'Your hero has +3 Attack on your turn.','Minion',NULL,NULL),('NAX3_03','Necrotic Poison',2,NULL,NULL,NULL,'Destroy a minion.','Spell',NULL,NULL),('NAX4_03','Skeleton',1,1,1,NULL,'','Minion',NULL,NULL),('NAX4_03H','Skeleton',5,5,5,NULL,'','Minion',NULL,NULL),('NAX4_05','Plague',6,NULL,NULL,NULL,'Destroy all non-Skeleton minions.','Spell',NULL,NULL),('NAX5_03','Mindpocalypse',2,NULL,NULL,NULL,'Both players draw 2 cards and gain a Mana Crystal.','Spell',NULL,NULL),('NAX6_03','Deathbloom',4,NULL,NULL,NULL,'Deal $5 damage to a minion. Summon a Spore.','Spell',NULL,NULL),('NAX6_03t','Spore',0,0,1,NULL,'<b>Deathrattle:</b> Give all enemy minions +8 Attack.','Minion',NULL,NULL),('NAX6_04','Sporeburst',1,NULL,NULL,NULL,'Deal $1 damage to all enemy minions. Summon a Spore.','Spell',NULL,NULL),('NAX7_02','Understudy',2,0,7,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('NAX7_04','Massive Runeblade',3,5,NULL,2,'Deals double damage to heroes.','Weapon',NULL,NULL),('NAX7_04H','Massive Runeblade',3,10,NULL,2,'Deals double damage to heroes.','Weapon',NULL,NULL),('NAX7_05','Mind Control Crystal',1,NULL,NULL,NULL,'Activate the Crystal to control the Understudies!','Spell',NULL,NULL),('NAX8_03','Unrelenting Trainee',1,2,2,NULL,'<b>Deathrattle:</b> Summon a Spectral Trainee for your opponent.','Minion',NULL,NULL),('NAX8_03t','Spectral Trainee',1,0,2,NULL,'At the start of your turn, deal 1 damage to your hero.','Minion',NULL,NULL),('NAX8_04','Unrelenting Warrior',3,3,4,NULL,'<b>Deathrattle:</b> Summon a Spectral Warrior for your opponent.','Minion',NULL,NULL),('NAX8_04t','Spectral Warrior',3,0,4,NULL,'At the start of your turn, deal 1 damage to your hero.','Minion',NULL,NULL),('NAX8_05','Unrelenting Rider',6,5,6,NULL,'<b>Deathrattle:</b> Summon a Spectral Rider for your opponent.','Minion',NULL,NULL),('NAX8_05t','Spectral Rider',5,0,6,NULL,'At the start of your turn, deal 1 damage to your hero.','Minion',NULL,NULL),('NAX9_02','Lady Blaumeux',3,1,7,NULL,'Your hero is <b>Immune</b>.','Minion',NULL,NULL),('NAX9_02H','Lady Blaumeux',3,2,7,NULL,'Your hero is <b>Immune</b>.','Minion',NULL,NULL),('NAX9_03','Thane Korth\'azz',3,1,7,NULL,'Your hero is <b>Immune</b>.','Minion',NULL,NULL),('NAX9_03H','Thane Korth\'azz',3,2,7,NULL,'Your hero is <b>Immune</b>.','Minion',NULL,NULL),('NAX9_04','Sir Zeliek',3,1,7,NULL,'Your hero is <b>Immune</b>.','Minion',NULL,NULL),('NAX9_04H','Sir Zeliek',3,2,7,NULL,'Your hero is <b>Immune</b>.','Minion',NULL,NULL),('NAX9_05','Runeblade',3,1,NULL,3,'Has +3 Attack if the other Horsemen are dead.','Weapon',NULL,NULL),('NAX9_05H','Runeblade',3,2,NULL,3,'Has +6 Attack if the other Horsemen are dead.','Weapon',NULL,NULL),('NAX9_07','Mark of the Horsemen',5,NULL,NULL,NULL,'Give your minions and your weapon +1/+1.','Spell',NULL,NULL),('NAXM_001','Necroknight',4,5,6,NULL,'<b>Deathrattle:</b> Destroy the minions next to this one as well.','Minion',NULL,NULL),('NAXM_002','Skeletal Smith',3,4,3,NULL,'<b>Deathrattle:</b> Destroy your opponent\'s weapon.','Minion',NULL,NULL),('NEW1_003','Sacrificial Pact',0,NULL,NULL,NULL,'Destroy a Demon. Restore #5 Health to your hero.','Spell','Common','Warlock'),('NEW1_004','Vanish',6,NULL,NULL,NULL,'Return all minions to their owner\'s hand.','Spell','Common','Rogue'),('NEW1_005','Kidnapper',6,5,3,NULL,'<b>Combo:</b> Return a minion to its owner\'s hand.','Minion','Epic','Rogue'),('NEW1_007','Starfall',5,NULL,NULL,NULL,'<b>Choose One -</b> Deal $5 damage to a minion; or $2 damage to all enemy minions.','Spell','Rare','Druid'),('NEW1_008','Ancient of Lore',7,5,5,NULL,'<b>Choose One -</b> Draw 2 cards; or Restore 5 Health.','Minion','Epic','Druid'),('NEW1_009','Healing Totem',1,0,2,NULL,'At the end of your turn, restore 1 Health to all friendly minions.','Minion','Free','Shaman'),('NEW1_010','Al\'Akir the Windlord',8,3,5,NULL,'<b>Windfury, Charge, Divine Shield, Taunt</b>','Minion','Legendary','Shaman'),('NEW1_011','Kor\'kron Elite',4,4,3,NULL,'<b>Charge</b>','Minion','Common','Warrior'),('NEW1_012','Mana Wyrm',1,1,3,NULL,'Whenever you cast a spell, gain +1 Attack.','Minion','Common','Mage'),('NEW1_014','Master of Disguise',4,4,4,NULL,'<b>Battlecry:</b> Give a friendly minion <b>Stealth</b>.','Minion','Rare','Rogue'),('NEW1_016','Captain\'s Parrot',2,1,1,NULL,'<b>Battlecry:</b> Put a random Pirate from your deck into your hand.','Minion','Epic',NULL),('NEW1_017','Hungry Crab',1,1,2,NULL,'<b>Battlecry:</b> Destroy a Murloc and gain +2/+2.','Minion','Epic',NULL),('NEW1_018','Bloodsail Raider',2,2,3,NULL,'<b>Battlecry:</b> Gain Attack equal to the Attack of your weapon.','Minion','Common',NULL),('NEW1_019','Knife Juggler',2,3,2,NULL,'After you summon a minion, deal 1 damage to a random enemy.','Minion','Rare',NULL),('NEW1_020','Wild Pyromancer',2,3,2,NULL,'After you cast a spell, deal 1 damage to ALL minions.','Minion','Rare',NULL),('NEW1_021','Doomsayer',2,0,7,NULL,'At the start of your turn, destroy ALL minions.','Minion','Epic',NULL),('NEW1_022','Dread Corsair',4,3,3,NULL,'<b>Taunt.</b> Costs (1) less per Attack of your weapon.','Minion','Common',NULL),('NEW1_023','Faerie Dragon',2,3,2,NULL,'Can\'t be targeted by spells or Hero Powers.','Minion','Common',NULL),('NEW1_024','Captain Greenskin',5,5,4,NULL,'<b>Battlecry:</b> Give your weapon +1/+1.','Minion','Legendary',NULL),('NEW1_025','Bloodsail Corsair',1,1,2,NULL,'<b>Battlecry:</b> Remove 1 Durability from your opponent\'s weapon.','Minion','Rare',NULL),('NEW1_026','Violet Teacher',4,3,5,NULL,'Whenever you cast a spell, summon a 1/1 Violet Apprentice.','Minion','Rare',NULL),('NEW1_026t','Violet Apprentice',0,1,1,NULL,'','Minion',NULL,NULL),('NEW1_027','Southsea Captain',3,3,3,NULL,'Your other Pirates have +1/+1.','Minion','Epic',NULL),('NEW1_029','Millhouse Manastorm',2,4,4,NULL,'<b>Battlecry:</b> Enemy spells cost (0) next turn.','Minion','Legendary',NULL),('NEW1_030','Deathwing',10,12,12,NULL,'<b>Battlecry:</b> Destroy all other minions and discard your hand.','Minion','Legendary',NULL),('NEW1_031','Animal Companion',3,NULL,NULL,NULL,'Summon a random Beast Companion.','Spell','Common','Hunter'),('NEW1_032','Misha',3,4,4,NULL,'<b>Taunt</b>','Minion','Common','Hunter'),('NEW1_033','Leokk',3,2,4,NULL,'Other friendly minions have +1 Attack.','Minion','Common','Hunter'),('NEW1_034','Huffer',3,4,2,NULL,'<b>Charge</b>','Minion','Common','Hunter'),('NEW1_036','Commanding Shout',2,NULL,NULL,NULL,'Your minions can\'t be reduced below 1 Health this turn. Draw a card.','Spell','Rare','Warrior'),('NEW1_037','Master Swordsmith',2,1,3,NULL,'At the end of your turn, give another random friendly minion +1 Attack.','Minion','Rare',NULL),('NEW1_038','Gruul',8,7,7,NULL,'At the end of each turn, gain +1/+1 .','Minion','Legendary',NULL),('NEW1_040','Hogger',6,4,4,NULL,'At the end of your turn, summon a 2/2 Gnoll with <b>Taunt</b>.','Minion','Legendary',NULL),('NEW1_040t','Gnoll',2,2,2,NULL,'<b>Taunt</b>','Minion',NULL,NULL),('NEW1_041','Stampeding Kodo',5,3,5,NULL,'<b>Battlecry:</b> Destroy a random enemy minion with 2 or less Attack.','Minion','Rare',NULL),('PART_001','Armor Plating',1,NULL,NULL,NULL,'Give a minion +1 Health.','Spell',NULL,NULL),('PART_002','Time Rewinder',1,NULL,NULL,NULL,'Return a friendly minion to your hand.','Spell',NULL,NULL),('PART_003','Rusty Horn',1,NULL,NULL,NULL,'Give a minion <b>Taunt</b>.','Spell',NULL,NULL),('PART_004','Finicky Cloakfield',1,NULL,NULL,NULL,'Give a friendly minion <b>Stealth</b> until your next turn.','Spell',NULL,NULL),('PART_005','Emergency Coolant',1,NULL,NULL,NULL,'<b>Freeze</b> a minion.','Spell',NULL,NULL),('PART_006','Reversing Switch',1,NULL,NULL,NULL,'Swap a minion\'s Attack and Health.','Spell',NULL,NULL),('PART_007','Whirling Blades',1,NULL,NULL,NULL,'Give a minion +1 Attack.','Spell',NULL,NULL),('PRO_001','Elite Tauren Chieftain',5,5,5,NULL,'<b>Battlecry:</b> Give both players the power to ROCK! (with a Power Chord card)','Minion','Legendary',NULL),('PRO_001a','I Am Murloc',4,NULL,NULL,NULL,'Summon three, four, or five 1/1 Murlocs.','Spell',NULL,NULL),('PRO_001at','Murloc',1,1,1,NULL,'','Minion',NULL,NULL),('PRO_001b','Rogues Do It...',4,NULL,NULL,NULL,'Deal $4 damage. Draw a card.','Spell',NULL,NULL),('PRO_001c','Power of the Horde',4,NULL,NULL,NULL,'Summon a random Horde Warrior.','Spell',NULL,NULL),('skele11','Skeleton',1,1,1,NULL,'<b></b>','Minion','Common',NULL),('skele21','Damaged Golem',1,2,1,NULL,'','Minion','Common',NULL),('tt_004','Flesheating Ghoul',3,2,3,NULL,'Whenever a minion dies, gain +1 Attack.','Minion','Common',NULL),('tt_010','Spellbender',3,NULL,NULL,NULL,'<b>Secret:</b> When an enemy casts a spell on a minion, summon a 1/3 as the new target.','Spell','Epic','Mage'),('tt_010a','Spellbender',0,1,3,NULL,'','Minion','Epic','Mage');
/*!40000 ALTER TABLE `Card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Challenge`
--

DROP TABLE IF EXISTS `Challenge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Challenge` (
  `WinningID` int(11) NOT NULL,
  `LosingID` int(11) NOT NULL,
  `ChallengeID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ChallengeID`),
  KEY `challenge_ibfk_1_idx` (`WinningID`),
  KEY `challenge_ibfk_2_idx` (`LosingID`),
  CONSTRAINT `challenge_ibfk_1` FOREIGN KEY (`WinningID`) REFERENCES `Deck` (`deckID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `challenge_ibfk_2` FOREIGN KEY (`LosingID`) REFERENCES `Deck` (`deckID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Challenge`
--

LOCK TABLES `Challenge` WRITE;
/*!40000 ALTER TABLE `Challenge` DISABLE KEYS */;
INSERT INTO `Challenge` VALUES (26,27,4),(24,27,5),(26,10,6),(24,10,7),(24,10,8),(26,10,9),(24,10,10),(10,26,11),(24,27,12),(24,27,13),(10,26,14),(10,26,15),(10,38,16),(10,38,17),(10,38,18),(26,34,19),(26,36,20),(26,10,21),(26,10,22),(26,41,23),(26,41,24),(26,40,25),(34,26,26),(34,26,27),(34,38,28),(38,34,29),(38,34,30),(38,34,31),(36,38,32),(41,38,33),(38,41,34),(38,40,35),(40,38,36),(38,40,37),(40,38,38),(10,34,39),(10,26,40),(26,10,41),(34,10,42),(34,40,43),(36,10,44),(36,10,45),(36,40,46),(36,40,47),(38,10,48),(40,38,49),(41,26,50),(40,26,51),(41,38,52),(10,36,53),(26,36,54),(34,41,55),(34,41,56),(34,40,57),(36,10,58),(36,10,59),(34,38,60),(36,41,61),(36,40,62),(36,40,63),(40,36,64),(36,26,65),(38,36,66),(38,34,67),(36,38,68),(38,36,69),(36,38,70),(40,34,71),(36,40,72),(10,38,73),(38,10,74),(40,26,75),(40,26,76),(40,38,77),(26,40,78),(40,38,79),(26,40,80),(38,41,81),(36,26,82),(26,34,83),(26,34,84),(26,36,85),(26,36,86),(36,38,87),(38,36,88),(10,34,89),(10,34,90),(40,34,91),(41,36,92),(41,36,93),(34,40,94),(47,45,95),(47,45,96),(47,45,97),(45,47,98),(45,40,99),(45,40,100),(45,40,101),(45,48,102),(45,41,103),(45,41,104),(45,41,105),(45,41,106),(10,42,107),(10,43,108),(10,43,109),(10,44,110),(10,44,111),(26,42,112),(26,43,113),(26,43,114),(26,44,115),(26,44,116),(26,44,117),(26,44,118),(42,10,119),(42,10,120),(42,10,121),(42,40,122),(42,40,123),(42,40,124),(42,41,125),(42,41,126),(42,41,127),(42,41,128),(42,26,129),(42,26,130),(42,26,131),(43,10,132),(43,10,133),(43,48,134),(43,41,135),(43,41,136),(43,41,137),(43,41,138),(43,26,139),(43,26,140),(43,45,141),(43,45,142),(43,45,143),(43,45,144),(44,10,145),(44,46,146),(44,40,147),(44,48,148),(44,48,149),(44,41,150),(44,41,151),(44,41,152),(44,41,153),(44,41,154),(46,44,155),(46,44,156),(48,42,157),(48,43,158),(48,43,159),(48,43,160),(48,44,161),(48,44,162),(48,44,163),(44,41,164),(44,26,165),(44,26,166),(45,42,167),(45,42,168),(45,42,169),(45,42,170),(45,44,171),(45,44,172),(44,45,173),(44,45,174),(45,44,175),(44,45,176),(43,45,177),(45,43,178),(45,43,179),(45,43,180),(44,45,181),(44,45,182),(44,45,183),(44,45,184);
/*!40000 ALTER TABLE `Challenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConsistOf`
--

DROP TABLE IF EXISTS `ConsistOf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConsistOf` (
  `cardID` char(100) NOT NULL,
  `deckID` int(11) NOT NULL,
  `num` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`cardID`,`deckID`),
  KEY `fk_deckID` (`deckID`),
  CONSTRAINT `fk_cardID` FOREIGN KEY (`cardID`) REFERENCES `Card` (`cardID`),
  CONSTRAINT `fk_deckID` FOREIGN KEY (`deckID`) REFERENCES `Deck` (`deckID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConsistOf`
--

LOCK TABLES `ConsistOf` WRITE;
/*!40000 ALTER TABLE `ConsistOf` DISABLE KEYS */;
INSERT INTO `ConsistOf` VALUES ('BRM_013',38,2),('BRM_013',45,2),('BRM_028',10,1),('BRM_028',26,1),('BRM_028',27,1),('BRM_028',41,1),('BRM_028',46,1),('BRM_028',47,1),('CS2_022',44,1),('CS2_023',36,2),('CS2_023',44,2),('CS2_024',36,2),('CS2_024',44,2),('CS2_024',48,2),('CS2_026',36,2),('CS2_026',40,1),('CS2_026',44,2),('CS2_027',36,2),('CS2_027',44,2),('CS2_028',36,2),('CS2_028',44,2),('CS2_029',36,2),('CS2_029',40,1),('CS2_029',44,2),('CS2_029',48,2),('CS2_031',36,2),('CS2_031',44,2),('CS2_032',36,2),('CS2_032',40,1),('CS2_032',44,2),('CS2_032',48,1),('CS2_033',40,2),('CS2_033',48,2),('CS2_042',26,1),('CS2_045',26,2),('CS2_072',33,2),('CS2_072',34,2),('CS2_072',42,2),('CS2_072',43,2),('CS2_074',33,2),('CS2_074',34,2),('CS2_074',42,2),('CS2_074',43,2),('CS2_075',43,2),('CS2_077',33,2),('CS2_077',34,2),('CS2_077',42,2),('CS2_077',43,1),('CS2_093',10,2),('CS2_093',46,2),('CS2_093',47,2),('CS2_097',10,2),('CS2_097',46,2),('CS2_097',47,2),('CS2_117',33,2),('CS2_117',34,2),('CS2_117',42,2),('CS2_117',43,2),('CS2_146',33,1),('CS2_161',27,2),('CS2_161',41,2),('CS2_179',26,2),('CS2_186',27,2),('CS2_186',41,2),('CS2_188',37,2),('CS2_188',38,2),('CS2_188',45,2),('CS2_200',27,2),('CS2_200',41,2),('CS2_203',24,1),('CS2_203',37,1),('CS2_203',38,1),('CS2_203',45,1),('CS2_222',27,2),('CS2_222',41,2),('CS2_233',33,2),('CS2_233',34,2),('CS2_233',42,2),('CS2_233',43,2),('CS2_mirror',27,2),('CS2_mirror',41,2),('DS1_070',24,1),('DS1_070',37,1),('DS1_070',38,1),('DS1_070',45,1),('EX1_005',10,1),('EX1_005',46,1),('EX1_005',47,1),('EX1_007',10,1),('EX1_007',36,2),('EX1_007',44,2),('EX1_007',46,1),('EX1_007',47,2),('EX1_012',33,1),('EX1_012',34,1),('EX1_012',36,1),('EX1_012',42,1),('EX1_012',43,1),('EX1_012',44,1),('EX1_016',10,1),('EX1_016',26,1),('EX1_016',27,1),('EX1_016',41,1),('EX1_016',46,1),('EX1_016',47,1),('EX1_029',37,2),('EX1_029',38,2),('EX1_029',45,2),('EX1_032',26,2),('EX1_093',10,1),('EX1_093',46,1),('EX1_093',47,1),('EX1_096',36,1),('EX1_105',41,1),('EX1_116',38,1),('EX1_116',45,1),('EX1_124',33,2),('EX1_124',34,2),('EX1_124',42,2),('EX1_124',43,2),('EX1_129',33,2),('EX1_129',34,2),('EX1_129',42,2),('EX1_129',43,2),('EX1_131',43,1),('EX1_134',33,2),('EX1_134',34,2),('EX1_134',42,2),('EX1_134',43,2),('EX1_145',33,2),('EX1_145',34,2),('EX1_145',42,2),('EX1_145',43,2),('EX1_246',26,1),('EX1_248',26,2),('EX1_259',26,2),('EX1_275',27,2),('EX1_275',41,2),('EX1_278',42,1),('EX1_279',36,1),('EX1_279',44,1),('EX1_283',27,2),('EX1_283',41,2),('EX1_284',33,2),('EX1_284',34,2),('EX1_284',36,2),('EX1_284',42,1),('EX1_284',44,2),('EX1_289',36,1),('EX1_289',40,2),('EX1_295',36,2),('EX1_295',40,1),('EX1_295',44,2),('EX1_354',10,1),('EX1_354',46,1),('EX1_354',47,1),('EX1_382',10,2),('EX1_382',46,2),('EX1_382',47,2),('EX1_383',10,1),('EX1_383',46,1),('EX1_383',47,1),('EX1_534',24,2),('EX1_534',37,2),('EX1_536',24,2),('EX1_536',37,2),('EX1_536',38,2),('EX1_536',45,2),('EX1_538',24,2),('EX1_538',37,2),('EX1_538',38,2),('EX1_538',45,2),('EX1_539',24,2),('EX1_539',37,2),('EX1_539',38,2),('EX1_539',45,2),('EX1_554',24,1),('EX1_554',37,1),('EX1_554',38,1),('EX1_554',45,1),('EX1_558',10,1),('EX1_558',46,1),('EX1_559',40,1),('EX1_559',48,1),('EX1_560',27,1),('EX1_560',41,1),('EX1_561',27,1),('EX1_561',36,1),('EX1_561',41,1),('EX1_561',44,1),('EX1_562',27,1),('EX1_562',41,1),('EX1_575',26,1),('EX1_581',33,2),('EX1_581',34,2),('EX1_581',42,1),('EX1_581',43,1),('EX1_586',27,2),('EX1_586',41,2),('EX1_594',27,2),('EX1_594',36,1),('EX1_594',41,2),('EX1_608',48,1),('EX1_610',45,1),('EX1_611',24,2),('EX1_613',42,1),('EX1_613',43,1),('EX1_619',10,1),('EX1_619',46,1),('EX1_619',47,1),('EX1_620',27,2),('EX1_620',41,2),('FP1_001',10,1),('FP1_001',26,1),('FP1_002',24,2),('FP1_002',26,2),('FP1_002',37,2),('FP1_002',38,2),('FP1_002',45,2),('FP1_004',24,2),('FP1_004',37,2),('FP1_004',38,2),('FP1_004',45,2),('FP1_005',33,1),('FP1_005',34,1),('FP1_005',42,1),('FP1_011',24,2),('FP1_012',10,2),('FP1_012',24,2),('FP1_012',26,2),('FP1_012',46,1),('FP1_012',47,1),('FP1_013',26,1),('FP1_013',27,1),('FP1_013',41,1),('FP1_025',26,2),('FP1_030',24,1),('FP1_030',26,1),('FP1_030',33,1),('FP1_030',34,1),('FP1_030',37,1),('FP1_030',42,1),('FP1_030',43,1),('GVG_001',44,1),('GVG_002',27,1),('GVG_002',41,1),('GVG_003',27,1),('GVG_003',41,1),('GVG_004',40,2),('GVG_004',48,2),('GVG_006',40,2),('GVG_006',48,2),('GVG_007',27,2),('GVG_007',41,1),('GVG_013',40,2),('GVG_013',48,2),('GVG_022',33,2),('GVG_022',34,2),('GVG_022',42,2),('GVG_022',43,2),('GVG_023',34,1),('GVG_023',42,1),('GVG_023',43,2),('GVG_038',26,1),('GVG_043',37,2),('GVG_043',38,2),('GVG_043',45,2),('GVG_044',40,2),('GVG_044',46,2),('GVG_044',47,2),('GVG_044',48,2),('GVG_058',10,2),('GVG_058',46,2),('GVG_058',47,2),('GVG_060',10,2),('GVG_060',46,2),('GVG_060',47,2),('GVG_061',10,2),('GVG_061',46,2),('GVG_061',47,2),('GVG_069',10,1),('GVG_069',26,1),('GVG_069',46,1),('GVG_069',47,1),('GVG_074',24,1),('GVG_074',37,1),('GVG_074',38,1),('GVG_078',40,2),('GVG_078',48,1),('GVG_082',40,2),('GVG_082',48,2),('GVG_085',40,2),('GVG_085',48,1),('GVG_096',10,2),('GVG_096',24,2),('GVG_096',26,2),('GVG_096',37,2),('GVG_096',38,2),('GVG_096',40,2),('GVG_096',45,2),('GVG_096',46,2),('GVG_096',47,2),('GVG_096',48,2),('GVG_102',40,2),('GVG_102',48,2),('GVG_105',26,1),('GVG_110',10,1),('GVG_110',24,1),('GVG_110',26,1),('GVG_110',37,1),('GVG_110',38,1),('GVG_110',45,1),('GVG_110',46,1),('GVG_110',47,1),('GVG_110',48,1),('GVG_123',40,2),('GVG_123',48,2),('NEW1_012',40,1),('NEW1_012',44,1),('NEW1_012',48,2),('NEW1_019',10,2),('NEW1_019',24,2),('NEW1_019',37,2),('NEW1_019',38,2),('NEW1_019',45,2),('NEW1_019',46,2),('NEW1_019',47,2),('NEW1_021',36,2),('NEW1_021',44,2),('NEW1_026',33,2),('NEW1_026',34,2),('NEW1_026',42,2),('NEW1_026',43,2),('NEW1_031',24,2),('NEW1_031',37,2),('NEW1_031',38,2),('NEW1_031',45,2);
/*!40000 ALTER TABLE `ConsistOf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CoreCard`
--

DROP TABLE IF EXISTS `CoreCard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CoreCard` (
  `cardID` char(100) NOT NULL,
  `templateID` int(11) NOT NULL,
  PRIMARY KEY (`cardID`,`templateID`),
  KEY `templateID` (`templateID`),
  CONSTRAINT `corecard_ibfk_1` FOREIGN KEY (`cardID`) REFERENCES `card` (`cardID`),
  CONSTRAINT `corecard_ibfk_2` FOREIGN KEY (`templateID`) REFERENCES `template` (`templateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CoreCard`
--

LOCK TABLES `CoreCard` WRITE;
/*!40000 ALTER TABLE `CoreCard` DISABLE KEYS */;
INSERT INTO `CoreCard` VALUES ('CS1_113',1),('CS2_003',1),('EX1_016',1),('EX1_085',1),('EX1_091',1),('EX1_334',1),('EX1_339',1),('EX1_345',1),('GVG_011',1),('NEW1_020',1),('CS1_112',2),('CS2_004',2),('CS2_181',2),('CS2_235',2),('EX1_621',2),('EX1_624',2),('FP1_009',2),('GVG_008',2),('GVG_010',2),('GVG_012',2),('CS2_093',3),('CS2_097',3),('EX1_354',3),('EX1_382',3),('EX1_383',3),('EX1_619',3),('GVG_058',3),('GVG_060',3),('GVG_061',3),('NEW1_019',3),('CS1_112',4),('CS2_181',4),('CS2_235',4),('EX1_091',4),('EX1_334',4),('EX1_339',4),('EX1_591',4),('EX1_621',4),('FP1_023',4),('CS2_064',5),('CS2_065',5),('EX1_319',5),('EX1_323',5),('EX1_596',5),('FP1_022',5),('GVG_018',5),('GVG_019',5),('GVG_021',5),('GVG_100',5),('CS2_105',6),('CS2_106',6),('CS2_112',6),('CS2_124',6),('CS2_203',6),('EX1_029',6),('EX1_408',6),('FP1_021',6),('NEW1_011',6),('NEW1_022',6),('CS2_062',7),('EX1_043',7),('EX1_045',7),('EX1_058',7),('EX1_093',7),('EX1_105',7),('EX1_309',7),('EX1_323',7),('EX1_620',7),('GVG_069',7),('CS2_024',8),('CS2_026',8),('CS2_028',8),('CS2_031',8),('EX1_279',8),('EX1_295',8),('EX1_561',8),('NEW1_021',8),('CS2_106',9),('CS2_108',9),('EX1_007',9),('EX1_407',9),('EX1_410',9),('EX1_414',9),('EX1_561',9),('EX1_603',9),('EX1_606',9),('FP1_021',9),('CS2_045',10),('EX1_096',10),('EX1_238',10),('EX1_241',10),('EX1_245',10),('EX1_259',10),('EX1_284',10),('EX1_563',10),('GVG_029',10),('GVG_038',10),('EX1_559',11),('GVG_004',11),('GVG_006',11),('GVG_013',11),('GVG_044',11),('GVG_078',11),('GVG_082',11),('GVG_085',11),('GVG_096',11),('GVG_102',11),('BRM_028',12),('CS2_011',12),('CS2_012',12),('CS2_013',12),('EX1_154',12),('EX1_165',12),('EX1_166',12),('EX1_169',12),('EX1_571',12),('NEW1_008',12),('CS2_084',13),('DS1_070',13),('EX1_534',13),('EX1_544',13),('EX1_611',13),('FP1_011',13),('FP1_012',13),('FP1_030',13),('GVG_096',13),('GVG_110',13),('CS2_074',14),('CS2_077',14),('CS2_233',14),('EX1_124',14),('EX1_129',14),('EX1_145',14),('EX1_284',14),('GVG_022',14),('GVG_023',14),('NEW1_026',14),('CS2_007',15),('EX1_049',15),('EX1_050',15),('EX1_161',15),('EX1_166',15),('FP1_019',15),('GVG_032',15),('GVG_033',15),('GVG_069',15),('NEW1_007',15),('CS2_038',16),('EX1_016',16),('EX1_110',16),('FP1_012',16),('FP1_013',16),('FP1_025',16),('FP1_031',16),('GVG_096',16),('GVG_105',16),('GVG_114',16),('BRM_013',17),('CS2_188',17),('CS2_203',17),('EX1_029',17),('FP1_002',17),('FP1_004',17),('GVG_043',17),('NEW1_019',17),('CS2_007',18),('EX1_002',18),('EX1_016',18),('EX1_085',18),('EX1_165',18),('EX1_178',18),('EX1_573',18),('FP1_012',18),('FP1_013',18),('CS2_188',19),('CS2_203',19),('EX1_046',19),('EX1_162',19),('EX1_310',19),('EX1_316',19),('EX1_319',19),('FP1_002',19),('FP1_007',19),('NEW1_019',19);
/*!40000 ALTER TABLE `CoreCard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Deck`
--

DROP TABLE IF EXISTS `Deck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Deck` (
  `deckID` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL,
  PRIMARY KEY (`deckID`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Deck`
--

LOCK TABLES `Deck` WRITE;
/*!40000 ALTER TABLE `Deck` DISABLE KEYS */;
INSERT INTO `Deck` VALUES (10,'Control Paladin'),(20,'Another deck'),(23,'T7 Hunter'),(24,'T7 Hunter'),(25,'Costume Shaman'),(26,'Costume Shaman'),(27,'Typical Losing Mage'),(33,'Nonsensical Rogue'),(34,'Nonsensical Rogue'),(35,'Pyroblast-on-the-face'),(36,'Pyroblast-on-the-face'),(37,'T7 Hunter'),(38,'T7 Hunter'),(39,'Mech Mage'),(40,'Mech Mage'),(41,'Typical Losing Mage'),(42,'Nonsensical Rogue'),(43,'Nonsensical Rogue'),(44,'Pyroblast-on-the-face'),(45,'T7 Hunter'),(46,'Control Paladin'),(47,'Control Paladin'),(48,'Mech Mage');
/*!40000 ALTER TABLE `Deck` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger deck_cleaner
before delete on Deck
for each row
begin
	delete from ConsistOf where deckID = old.deckID;
    delete from userhasdeck where deckid = old.deckid;
    delete from invitations where sender = old.deckid;
    delete from pendingresult where winner = old.deckid or loser = old.deckid;
    delete from challenge where winningid = old.deckid or losingid = old.deckid;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Favourite`
--

DROP TABLE IF EXISTS `Favourite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Favourite` (
  `username` char(20) NOT NULL,
  `deckID` int(11) NOT NULL,
  `UserGivenDeckName` char(20) NOT NULL,
  PRIMARY KEY (`username`,`deckID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Favourite`
--

LOCK TABLES `Favourite` WRITE;
/*!40000 ALTER TABLE `Favourite` DISABLE KEYS */;
INSERT INTO `Favourite` VALUES ('Yijun',1,'My little Sweetheart');
/*!40000 ALTER TABLE `Favourite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HeroPower`
--

DROP TABLE IF EXISTS `HeroPower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HeroPower` (
  `ClassName` char(20) NOT NULL,
  `HeroPowerID` char(100) NOT NULL,
  `HeroPowerName` char(40) NOT NULL,
  `HeroPowerCost` int(11) NOT NULL,
  `HeroPowerText` char(255) NOT NULL,
  PRIMARY KEY (`HeroPowerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HeroPower`
--

LOCK TABLES `HeroPower` WRITE;
/*!40000 ALTER TABLE `HeroPower` DISABLE KEYS */;
INSERT INTO `HeroPower` VALUES ('Priest','CS1h_001','Lesser Heal',2,'<b>Hero Power</b>\nRestore 2 Health.'),('Druid','CS2_017','Shapeshift',2,'<b>Hero Power</b>\n+1 Attack this turn.\n+1 Armor.'),('Mage','CS2_034','Fireblast',2,'<b>Hero Power</b>\nDeal 1 damage.'),('Shaman','CS2_049','Totemic Call',2,'<b>Hero Power</b>\nSummon a random Totem.'),('Warlock','CS2_056','Life Tap',2,'<b>Hero Power</b>\nDraw a card and take 2 damage.'),('Rogue','CS2_083b','Dagger Mastery',2,'<b>Hero Power</b>\nEquip a 1/2 Dagger.'),('Paladin','CS2_101','Reinforce',2,'<b>Hero Power</b>\nSummon a 1/1 Silver Hand Recruit.'),('Warrior','CS2_102','Armor Up!',2,'<b>Hero Power</b>\nGain 2 Armor.'),('Hunter','DS1h_292','Steady Shot',2,'<b>Hero Power</b>\nDeal 2 damage to the enemy hero.'),('Priest','EX1_625t','Mind Spike',2,'<b>Hero Power</b>\nDeal 2 damage.'),('Priest','EX1_625t2','Mind Shatter',2,'<b>Hero Power</b>\nDeal 3 damage.'),('Warlock','EX1_tk33','INFERNO!',2,'<b>Hero Power</b>\nSummon a 6/6 Infernal.');
/*!40000 ALTER TABLE `HeroPower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Invitations`
--

DROP TABLE IF EXISTS `Invitations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Invitations` (
  `sender` int(11) DEFAULT NULL,
  `receiver` char(20) DEFAULT NULL,
  `message` char(255) DEFAULT NULL,
  `invitationID` int(11) NOT NULL AUTO_INCREMENT,
  `initiator` char(20) DEFAULT NULL,
  PRIMARY KEY (`invitationID`),
  UNIQUE KEY `invitationID_UNIQUE` (`invitationID`),
  KEY `invitations_ibfk_1` (`sender`),
  KEY `invitations_ibfk_2_idx` (`receiver`),
  KEY `invitations_ibfj_3_idx` (`initiator`),
  CONSTRAINT `invitations_ibfj_3` FOREIGN KEY (`initiator`) REFERENCES `User` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `invitations_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `Deck` (`deckID`),
  CONSTRAINT `invitations_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `User` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Invitations`
--

LOCK TABLES `Invitations` WRITE;
/*!40000 ALTER TABLE `Invitations` DISABLE KEYS */;
/*!40000 ALTER TABLE `Invitations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PendingResult`
--

DROP TABLE IF EXISTS `PendingResult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PendingResult` (
  `winner` int(11) DEFAULT NULL,
  `loser` int(11) DEFAULT NULL,
  `waitingfor` char(20) DEFAULT NULL,
  `pendingresultid` int(11) NOT NULL AUTO_INCREMENT,
  `reporter` char(20) DEFAULT NULL,
  PRIMARY KEY (`pendingresultid`),
  UNIQUE KEY `pendingresultid_UNIQUE` (`pendingresultid`),
  KEY `pendingresult_ibfk_2_idx` (`loser`),
  KEY `pendingresult_ibfk_1` (`winner`),
  KEY `pendingresult_ibfk_3_idx` (`waitingfor`),
  KEY `pendingresult_ibfk_4_idx` (`reporter`),
  CONSTRAINT `pendingresult_ibfk_1` FOREIGN KEY (`winner`) REFERENCES `Deck` (`deckID`),
  CONSTRAINT `pendingresult_ibfk_2` FOREIGN KEY (`loser`) REFERENCES `Deck` (`deckID`),
  CONSTRAINT `pendingresult_ibfk_3` FOREIGN KEY (`waitingfor`) REFERENCES `User` (`username`),
  CONSTRAINT `pendingresult_ibfk_4` FOREIGN KEY (`reporter`) REFERENCES `User` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PendingResult`
--

LOCK TABLES `PendingResult` WRITE;
/*!40000 ALTER TABLE `PendingResult` DISABLE KEYS */;
/*!40000 ALTER TABLE `PendingResult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Template`
--

DROP TABLE IF EXISTS `Template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Template` (
  `name` char(45) DEFAULT NULL,
  `templateID` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`templateID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Template`
--

LOCK TABLES `Template` WRITE;
/*!40000 ALTER TABLE `Template` DISABLE KEYS */;
INSERT INTO `Template` VALUES ('94Priest',1),('ChosenPriest',2),('ControlPaladin',3),('ControlPriest',4),('DevilWarlock',5),('FaceWarrior',6),('HandlockWarlock',7),('IceMage',8),('LegendWarrior',9),('MalygosShaman',10),('MechMage',11),('MidrangeDruid',12),('MidrangeHunter',13),('OilRogue',14),('OverDrawDruid',15),('ReincarnateShaman',16),('T7Hunter',17),('TauntDruid',18),('ZooWarlock',19);
/*!40000 ALTER TABLE `Template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `username` char(20) NOT NULL,
  `rank` int(11) NOT NULL,
  `region` char(20) NOT NULL,
  `spendings` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES ('i am xiao',25,'Americas',0,'',''),('John',15,'americas',50,'john@illinois.edu','i am john'),('Mary',13,'europe',60,'iammary@gmail.com','i am mary'),('Mike',20,'americas',200,'mickey@disney.com','i am mike'),('morpheusss',20,'americas',10,'morpheus.hu14@gmail.com','no password'),('SB',25,'americas',0,'sb@sb.sb','test'),('xiao',21,'americas',10,'xiao@uiuc.edu','i am xiao'),('Yijun',9,'americas',200,'ylou4@illinois.edu','i am sb');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger clean_decks
before delete on user
for each row
	delete from userhasdeck where username = old.username */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `UserHasDeck`
--

DROP TABLE IF EXISTS `UserHasDeck`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserHasDeck` (
  `username` char(20) NOT NULL,
  `deckID` int(11) NOT NULL,
  PRIMARY KEY (`username`,`deckID`),
  KEY `fk_deckId_uhd` (`deckID`),
  CONSTRAINT `fk_deckId_uhd` FOREIGN KEY (`deckID`) REFERENCES `Deck` (`deckID`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `User` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserHasDeck`
--

LOCK TABLES `UserHasDeck` WRITE;
/*!40000 ALTER TABLE `UserHasDeck` DISABLE KEYS */;
INSERT INTO `UserHasDeck` VALUES ('yijun',20),('xiao',26),('yijun',41),('morpheusss',43),('morpheusss',44),('xiao',45),('yijun',47),('yijun',48);
/*!40000 ALTER TABLE `UserHasDeck` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `cardmatchcount`
--

DROP TABLE IF EXISTS `cardmatchcount`;
/*!50001 DROP VIEW IF EXISTS `cardmatchcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `cardmatchcount` AS SELECT 
 1 AS `cardID`,
 1 AS `num`,
 1 AS `WinningCount`,
 1 AS `LosingCount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `cardmatchcountgiventemplate`
--

DROP TABLE IF EXISTS `cardmatchcountgiventemplate`;
/*!50001 DROP VIEW IF EXISTS `cardmatchcountgiventemplate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `cardmatchcountgiventemplate` AS SELECT 
 1 AS `cardID`,
 1 AS `num`,
 1 AS `templateID`,
 1 AS `WinningCount`,
 1 AS `LosingCount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `cardwinrategiventemplate`
--

DROP TABLE IF EXISTS `cardwinrategiventemplate`;
/*!50001 DROP VIEW IF EXISTS `cardwinrategiventemplate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `cardwinrategiventemplate` AS SELECT 
 1 AS `cardID`,
 1 AS `num`,
 1 AS `templateID`,
 1 AS `winrate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `corecardcounts`
--

DROP TABLE IF EXISTS `corecardcounts`;
/*!50001 DROP VIEW IF EXISTS `corecardcounts`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `corecardcounts` AS SELECT 
 1 AS `count`,
 1 AS `templateID`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `deckcontainscorecards`
--

DROP TABLE IF EXISTS `deckcontainscorecards`;
/*!50001 DROP VIEW IF EXISTS `deckcontainscorecards`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `deckcontainscorecards` AS SELECT 
 1 AS `deckID`,
 1 AS `templateID`,
 1 AS `count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `deckderivedfromtemplate`
--

DROP TABLE IF EXISTS `deckderivedfromtemplate`;
/*!50001 DROP VIEW IF EXISTS `deckderivedfromtemplate`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `deckderivedfromtemplate` AS SELECT 
 1 AS `deckID`,
 1 AS `templateID`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `deckmatchcount`
--

DROP TABLE IF EXISTS `deckmatchcount`;
/*!50001 DROP VIEW IF EXISTS `deckmatchcount`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `deckmatchcount` AS SELECT 
 1 AS `deckID`,
 1 AS `LosingCount`,
 1 AS `WinningCount`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `deckmetainfo`
--

DROP TABLE IF EXISTS `deckmetainfo`;
/*!50001 DROP VIEW IF EXISTS `deckmetainfo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `deckmetainfo` AS SELECT 
 1 AS `deckID`,
 1 AS `numCards`,
 1 AS `avgCost`,
 1 AS `avgAttack`,
 1 AS `avgHealth`,
 1 AS `numWeapon`,
 1 AS `numSpell`,
 1 AS `className`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `losingcount_partial`
--

DROP TABLE IF EXISTS `losingcount_partial`;
/*!50001 DROP VIEW IF EXISTS `losingcount_partial`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `losingcount_partial` AS SELECT 
 1 AS `LosingID`,
 1 AS `count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `templatechallenge`
--

DROP TABLE IF EXISTS `templatechallenge`;
/*!50001 DROP VIEW IF EXISTS `templatechallenge`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `templatechallenge` AS SELECT 
 1 AS `WinningTemplate`,
 1 AS `LosingTemplate`,
 1 AS `ChallengeID`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `templateversustable`
--

DROP TABLE IF EXISTS `templateversustable`;
/*!50001 DROP VIEW IF EXISTS `templateversustable`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `templateversustable` AS SELECT 
 1 AS `WinningTemplate`,
 1 AS `LosingTemplate`,
 1 AS `count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `winningcount_partial`
--

DROP TABLE IF EXISTS `winningcount_partial`;
/*!50001 DROP VIEW IF EXISTS `winningcount_partial`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `winningcount_partial` AS SELECT 
 1 AS `WinningID`,
 1 AS `count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `winrates`
--

DROP TABLE IF EXISTS `winrates`;
/*!50001 DROP VIEW IF EXISTS `winrates`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `winrates` AS SELECT 
 1 AS `deckID`,
 1 AS `winRate`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `cardmatchcount`
--

/*!50001 DROP VIEW IF EXISTS `cardmatchcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cardmatchcount` AS select `consistof`.`cardID` AS `cardID`,`consistof`.`num` AS `num`,sum(`deckmatchcount`.`WinningCount`) AS `WinningCount`,sum(`deckmatchcount`.`LosingCount`) AS `LosingCount` from (`consistof` join `deckmatchcount` on((`consistof`.`deckID` = `deckmatchcount`.`deckID`))) group by `consistof`.`cardID`,`consistof`.`num` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cardmatchcountgiventemplate`
--

/*!50001 DROP VIEW IF EXISTS `cardmatchcountgiventemplate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cardmatchcountgiventemplate` AS select `consistof`.`cardID` AS `cardID`,`consistof`.`num` AS `num`,`deckderivedfromtemplate`.`templateID` AS `templateID`,sum(`deckmatchcount`.`WinningCount`) AS `WinningCount`,sum(`deckmatchcount`.`LosingCount`) AS `LosingCount` from ((`consistof` join `deckmatchcount` on((`consistof`.`deckID` = `deckmatchcount`.`deckID`))) join `deckderivedfromtemplate` on((`consistof`.`deckID` = `deckderivedfromtemplate`.`deckID`))) group by `consistof`.`cardID`,`consistof`.`num`,`deckderivedfromtemplate`.`templateID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `cardwinrategiventemplate`
--

/*!50001 DROP VIEW IF EXISTS `cardwinrategiventemplate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `cardwinrategiventemplate` AS select `cardmatchcountgiventemplate`.`cardID` AS `cardID`,`cardmatchcountgiventemplate`.`num` AS `num`,`cardmatchcountgiventemplate`.`templateID` AS `templateID`,(`cardmatchcountgiventemplate`.`WinningCount` / (`cardmatchcountgiventemplate`.`WinningCount` + `cardmatchcountgiventemplate`.`LosingCount`)) AS `winrate` from `cardmatchcountgiventemplate` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `corecardcounts`
--

/*!50001 DROP VIEW IF EXISTS `corecardcounts`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `corecardcounts` AS select count(`corecard`.`cardID`) AS `count`,`corecard`.`templateID` AS `templateID` from `corecard` group by `corecard`.`templateID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `deckcontainscorecards`
--

/*!50001 DROP VIEW IF EXISTS `deckcontainscorecards`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `deckcontainscorecards` AS select `consistof`.`deckID` AS `deckID`,`corecard`.`templateID` AS `templateID`,count(0) AS `count` from (`consistof` join `corecard` on((`consistof`.`cardID` = `corecard`.`cardID`))) group by `consistof`.`deckID`,`corecard`.`templateID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `deckderivedfromtemplate`
--

/*!50001 DROP VIEW IF EXISTS `deckderivedfromtemplate`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `deckderivedfromtemplate` AS select `deckcontainscorecards`.`deckID` AS `deckID`,`deckcontainscorecards`.`templateID` AS `templateID` from (`corecardcounts` join `deckcontainscorecards`) where ((`corecardcounts`.`templateID` = `deckcontainscorecards`.`templateID`) and ((`corecardcounts`.`count` * 3) <= (`deckcontainscorecards`.`count` * 4))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `deckmatchcount`
--

/*!50001 DROP VIEW IF EXISTS `deckmatchcount`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `deckmatchcount` AS select `deck`.`deckID` AS `deckID`,coalesce((select `losingcount_partial`.`count` from `losingcount_partial` where (`losingcount_partial`.`LosingID` = `deck`.`deckID`)),0) AS `LosingCount`,coalesce((select `winningcount_partial`.`count` from `winningcount_partial` where (`winningcount_partial`.`WinningID` = `deck`.`deckID`)),0) AS `WinningCount` from `deck` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `deckmetainfo`
--

/*!50001 DROP VIEW IF EXISTS `deckmetainfo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `deckmetainfo` AS select `consistof`.`deckID` AS `deckID`,sum(`consistof`.`num`) AS `numCards`,(sum((`card`.`cost` * `consistof`.`num`)) / sum(`consistof`.`num`)) AS `avgCost`,(sum((case when (`card`.`cardType` = 'Minion') then (`card`.`attack` * `consistof`.`num`) else 0 end)) / sum((case when (`card`.`cardType` = 'Minion') then `consistof`.`num` else 0 end))) AS `avgAttack`,(sum((case when (`card`.`cardType` = 'Minion') then (`card`.`health` * `consistof`.`num`) else 0 end)) / sum((case when (`card`.`cardType` = 'Minion') then `consistof`.`num` else 0 end))) AS `avgHealth`,sum((case when (`card`.`cardType` = 'Weapon') then `consistof`.`num` else 0 end)) AS `numWeapon`,sum((case when (`card`.`cardType` = 'Spell') then `consistof`.`num` else 0 end)) AS `numSpell`,group_concat(distinct `card`.`className` separator ',') AS `className` from (`consistof` join `card` on((`consistof`.`cardID` = `card`.`cardID`))) group by `consistof`.`deckID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `losingcount_partial`
--

/*!50001 DROP VIEW IF EXISTS `losingcount_partial`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `losingcount_partial` AS select `challenge`.`LosingID` AS `LosingID`,count(`challenge`.`ChallengeID`) AS `count` from `challenge` group by `challenge`.`LosingID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `templatechallenge`
--

/*!50001 DROP VIEW IF EXISTS `templatechallenge`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `templatechallenge` AS select `d1`.`templateID` AS `WinningTemplate`,`d2`.`templateID` AS `LosingTemplate`,`challenge`.`ChallengeID` AS `ChallengeID` from ((`deckderivedfromtemplate` `d1` join `deckderivedfromtemplate` `d2`) join `challenge`) where ((`d1`.`deckID` = `challenge`.`WinningID`) and (`d2`.`deckID` = `challenge`.`LosingID`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `templateversustable`
--

/*!50001 DROP VIEW IF EXISTS `templateversustable`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `templateversustable` AS select `templatechallenge`.`WinningTemplate` AS `WinningTemplate`,`templatechallenge`.`LosingTemplate` AS `LosingTemplate`,count(0) AS `count` from `templatechallenge` group by `templatechallenge`.`WinningTemplate`,`templatechallenge`.`LosingTemplate` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `winningcount_partial`
--

/*!50001 DROP VIEW IF EXISTS `winningcount_partial`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `winningcount_partial` AS select `challenge`.`WinningID` AS `WinningID`,count(`challenge`.`ChallengeID`) AS `count` from `challenge` group by `challenge`.`WinningID` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `winrates`
--

/*!50001 DROP VIEW IF EXISTS `winrates`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `winrates` AS select `deckmatchcount`.`deckID` AS `deckID`,(case (`deckmatchcount`.`WinningCount` + `deckmatchcount`.`LosingCount`) when 0 then 0.0000 else (`deckmatchcount`.`WinningCount` / (`deckmatchcount`.`WinningCount` + `deckmatchcount`.`LosingCount`)) end) AS `winRate` from `deckmatchcount` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-21 17:52:05
