import json
import pymysql as ps

try:
	with open('RawData/AllSets.json', 'r') as source:
		data = json.load(source)        # Automatically parse data from file when instantiated
		source.close()
except ValueError:
	data = None

if data is None:
	print('Failed to read card data from existing file.')
else:
	cnx = ps.connect(host='127.0.0.1', port=3306, user='root', passwd='Hehehe0408', db='hearthstone_app', charset='utf8')
	# ###############################################################
	# ##########   Change user and passwd key words!!!!! ############
	# ###############################################################
	cursor = cnx.cursor()

	cursor.execute('DELETE FROM card;')
	cnx.commit()
	cursor.execute('DELETE FROM HeroPower;')
	cnx.commit()

	categories = ['Curse of Naxxramas', 'Basic', 'Promotion', 'Classic', 'Reward', 'Goblins vs Gnomes', 'Blackrock Mountain']

	a_and_h_and_class_r = (	"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, health, cardText, cardType, rarity, className ) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(health)s, %(text)s, %(type)s, %(rarity)s, %(playerClass)s)")
	a_and_h_only_r = (		"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, health, cardText, cardType, rarity) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(health)s, %(text)s, %(type)s, %(rarity)s)")
	class_only_r = (		"	INSERT INTO card" + \
							"( cardID, cardName, cost, cardText, cardType, rarity, className ) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(text)s, %(type)s, %(rarity)s, %(playerClass)s)")
	a_and_h_nor_class_r = (	"INSERT INTO card" + \
							"( cardID, cardName, cost, cardText, cardType, rarity) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(text)s, %(type)s, %(rarity)s)")
	a_and_h_and_class = (	"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, health, cardText, cardType, className ) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(health)s, %(text)s, %(type)s, %(playerClass)s)")
	a_and_h_only = (		"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, health, cardText, cardType) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(health)s, %(text)s, %(type)s)")
	class_only = (			"INSERT INTO card" + \
							"( cardID, cardName, cost, cardText, cardType, className ) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(text)s, %(type)s, %(playerClass)s)")
	a_and_h_nor_class = (	"INSERT INTO card" + \
							"( cardID, cardName, cost, cardText, cardType) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(text)s, %(type)s)")
	weapon = (				"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, cardText, cardType, durability) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(text)s, %(type)s, %(durability)s)")
	weapon_class = (		"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, cardText, cardType, className, durability) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(text)s, %(type)s, %(playerClass)s, %(durability)s)")
	weapon_r = (			"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, cardText, cardType, rarity, durability) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(text)s, %(type)s, %(rarity)s, %(durability)s)")
	weapon_class_r = (		"INSERT INTO card" + \
							"( cardID, cardName, cost, attack, cardText, cardType, rarity, className, durability) " + \
							"VALUES (%(id)s, %(name)s, %(cost)s, %(attack)s, %(text)s, %(type)s, %(rarity)s, %(playerClass)s, %(durability)s)")
	heropower = (			"INSERT IGNORE INTO HeroPower" + \
							"( ClassName, HeroPowerID, HeroPowerName, HeroPowerCost, HeroPowerText) " + \
							"VALUES (%(playerClass)s, %(id)s, %(name)s, %(cost)s, %(text)s)")
	for cat in categories:
		for card in data[cat]:
			if not ('name' in card and 'cost' in card and 'type' in card):
				continue
			if card['type'] == 'Enchantment':
				continue
			if not 'text' in card:
				card['text'] = ''

			has_r = 'rarity' in card
			a_and_h = 'attack' in card and 'health' in card
			class_card = 'playerClass' in card

			if card['type'] == 'Weapon':
				if class_card:
					cursor.execute(
						weapon_class_r if has_r else weapon_class,
						card)
				else:
					cursor.execute(
						weapon_r if has_r else weapon,
						card)

			elif card['type'] == 'Hero Power':
				if class_card:
					cursor.execute(heropower, card)

			elif card['type'] != 'Hero': 
				if (a_and_h and class_card):
					cursor.execute(
						a_and_h_and_class_r if has_r else a_and_h_and_class,
						card)
				elif a_and_h:
					cursor.execute(
						a_and_h_only_r if has_r else a_and_h_only,
						card)
				elif class_card:
					cursor.execute(
						class_only_r if has_r else class_only,
						card)
				else:
					cursor.execute(
						a_and_h_nor_class_r if has_r else a_and_h_nor_class,
						card)
			cnx.commit()
	cursor.close()
	cnx.close()



















