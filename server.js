var express = require('express');
var mysql = require('mysql');
var app = express();
var server = require('http').Server(app);
server.listen(8000);
var polyfill = require('./static/util/polyfills');
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var db_info = {
  host     : 'localhost',
  user     : 'root',
  password : 'Hehehe0408',
  database : 'hearthstone_app'
};

app.use('/', express.static(__dirname + '/static'));

app.get('/userProfile/:username', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var username = connection.escape(req.params.username);
	connection.connect();
	connection.query("SELECT * FROM User WHERE username = " + username, function(err, rows, fields) {
		if (err) throw err;
		res.json(rows[0]);
		connection.end();
	});
});

app.post('/editProfile/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var command =	"UPDATE User SET " +
					"rank = " + req.body.rank + ', ' + 
					"region = " + connection.escape(req.body.region) + ', ' + 
					"spendings = " + req.body.spendings + ', ' + 
					"email = " + connection.escape(req.body.email) + ' ,' + 
					"password = " + connection.escape(req.body.password) + ' ' +
					"WHERE username = " + connection.escape(req.body.username);
	connection.connect();
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		res.json({a:'a'});
		connection.end();
	});
});

app.get('/EmailOf/:username', function(req, res) {
	var connection = mysql.createConnection(db_info);
	connection.connect();
	connection.query('SELECT email FROM User WHERE username = ' + connection.escape(req.params.username), function(err, rows, fields) {
	  if (err) throw err;
	  res.json(rows);
	  connection.end();
	});
});

app.get('/allCards', function(req, res) {
	var connection = mysql.createConnection(db_info);
	connection.connect();
	connection.query('SELECT cardName, cardID FROM Card', function(err, rows, fields) {
	  if (err) throw err;
	  res.json(rows);
	  connection.end();
	});
});

app.post('/allDecks/:username', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var username = connection.escape(req.params.username);
	var password = req.body.password;
	connection.connect();
	connection.query("INSERT IGNORE INTO User (username, rank, region, spendings, password) VALUES ("+username+", 25, \'Americas\', 0, "+connection.escape(password)+")", function(err, rows, fields) {
		if (err) throw err;
		connection.query("SELECT password FROM User WHERE username = " + username, function(err, rows, fields) {
			if (err) throw err;
			if (rows[0].password === password.toLowerCase()) {
				var command = (	'SELECT Deck.deckID, Deck.name, User.password FROM User, UserHasDeck, Deck ' + 
								'WHERE User.username = UserHasDeck.username AND UserHasDeck.deckID = Deck.deckID ' + 
								'AND User.username = ' + username + ' ' +
								'ORDER BY Deck.deckID');
				connection.query( command, function(err, rows, fields) {
				  if (err) throw err;
				  res.json({deck_list: rows, success: true});
				  connection.end();
				});
			}
			else {
				res.json({success: false});
				connection.end();
			}
		});
	})
});

app.get('/receivedInvitations/:username', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var username = connection.escape(req.params.username);
	connection.connect();
	var command = "SELECT * FROM Invitations WHERE receiver = " + username;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		res.json(rows);
		connection.end();
	});
});

app.get('/sentInvitations/:username', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var username = connection.escape(req.params.username);
	connection.connect();
	var command = "SELECT * FROM Invitations WHERE initiator = " + username;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		res.json(rows);
		connection.end();
	});
});

app.post('/declineInvitation/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var username = connection.escape(req.body.username);
	var invitationID = req.body.invitationID;
	connection.connect();
	var command = "DELETE FROM Invitations WHERE receiver = " + username + " AND invitationID = " + invitationID;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		command = "SELECT * FROM Invitations WHERE receiver = " + username ;
		connection.query( command, function(err, rows, fields) {
			if (err) throw err;
			res.json(rows);
			connection.end();
		});
	});
});

app.get('/CardsOfDeck/:deckid', function(req, res) {
	var connection = mysql.createConnection(db_info);
	connection.connect();
	var command = 	"SELECT * FROM Deck NATURAL JOIN ConsistOf NATURAL JOIN Card " + 
					"WHERE deckID = " + req.params.deckid + " " +
					"ORDER BY cost DESC, cardName DESC";
	connection.query( command, function(err, rows, fields) {
	  if (err) throw err;
	  res.json(rows);
	  connection.end();
	});
});

app.post('/searchCards/', function(req, res) {

	var name = req.body.name;
	var attack_comp = req.body.attack_comp;
	var attack = req.body.attack;
	var health_comp = req.body.health_comp;
	var health = req.body.health;
	var cost_comp = req.body.cost_comp;
	var cost = req.body.cost;
	var cls = req.body.cls;

	var command = 'SELECT * FROM Card ';

	var conditions = [];

	var connection = mysql.createConnection(db_info);
	connection.connect();

	if (name !== '') {
		conditions.push('cardName regexp ' + connection.escape(name));
	};
	if (attack !== '') {
		conditions.push('attack ' + attack_comp + ' ' + attack);
	};
	if (health !== '') {
		conditions.push('health ' + health_comp + ' ' + health);
	};
	if (cost !== '') {
		conditions.push('cost ' + cost_comp + ' ' + cost);
	};
	if (cls !== 'All') {
		conditions.push('className = \'' + cls + '\'');
	};

	if (conditions.length > 0) {
		command += 'WHERE ';
		for (var i = conditions.length - 1; i >= 0; i--) {
			command += conditions[i] + ' AND ';
		};
	};

	command += '1 ORDER BY Card.cost;';
	connection.query( command, function(err, rows, fields) {
	  if (err) throw err;
	  res.json(rows);
	  connection.end();
	});
});

app.post('/CreateDeck/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var username = connection.escape(req.body.username);
	var deck_name = connection.escape(req.body.deck_name);

	connection.connect();
	var command = "INSERT INTO Deck (name) VALUES (" + deck_name + ")"
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;

		command = "INSERT INTO UserHasDeck (username, deckID) VALUES ("+username+", "+rows.insertId+")"
		connection.query( command, function(err, rows, fields) {
			if (err) throw err;

			command = (	'SELECT Deck.deckID, Deck.name FROM UserHasDeck, Deck ' + 
						'WHERE UserHasDeck.deckID = Deck.deckID AND UserHasDeck.username = ' + username);
			connection.query( command, function(err, rows, fields) {
			  if (err) throw err;
			  res.json(rows);
			  connection.end();
			});
		})
	})
});

app.post('/SaveDeck/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var cards = req.body.cards;
	var deck_id = req.body.deckId;
	var username = connection.escape(req.body.username);
	connection.connect();
	//	Get deck name
	var command = "SELECT name FROM Deck WHERE deckID = " + deck_id;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		//	Create new deck
		if ( rows.length > 0 ) {
			var deck_name = connection.escape(rows[0].name);
		}
		else {
			return null;
		};
		command = "INSERT INTO Deck (name) VALUES (" + deck_name + ")";
		connection.query( command, function(err, rows, fields) {
			if (err) throw err;
			//	Unrelated old deck with user
			var new_deck_id = rows.insertId;
			command = "DELETE FROM UserHasDeck WHERE deckID = " + deck_id;
			connection.query( command, function(err, rows, fields) {
				if (err) throw err;
				// Relate new deck with user
				command = "INSERT INTO UserHasDeck (username, deckID) VALUES ("+username+", "+new_deck_id+")";
				connection.query( command, function(err, rows, fields) {
					if (err) throw err;
					//	Fill new deck
					command = "INSERT INTO ConsistOf (cardID, deckID, num) VALUES ";
					for (var cardID in cards) {
						command += " (\'"+cardID+"\',"+new_deck_id+","+cards[cardID].num+"), "
					};
					command = command.substring(0, command.lastIndexOf(',') );
					connection.query( command, function(err, rows, fields) {
						if (err) throw err;
						command = (	'SELECT Deck.deckID, Deck.name FROM UserHasDeck, Deck ' + 
									'WHERE UserHasDeck.deckID = Deck.deckID AND UserHasDeck.username = ' + username + ' ' + 
									'ORDER BY Deck.deckID');
						connection.query( command, function(err, rows, fields) {
						  if (err) throw err;
						  res.json(rows);
						  connection.end();
						});
					});
				});
			});
		});
	});
});

app.post('/SearchOpponent/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var avg_cost_compr = req.body.avg_cost_compr;
	var avg_cost_value = req.body.avg_cost_value;
	var avg_attack_compr = req.body.avg_attack_compr;
	var avg_attack_value = req.body.avg_attack_value;
	var avg_health_compr = req.body.avg_health_compr;
	var avg_health_value = req.body.avg_health_value;
	var num_spell_compr = req.body.num_spell_compr;
	var num_spell_value = req.body.num_spell_value;
	var num_weapon_compr = req.body.num_weapon_compr;
	var num_weapon_value = req.body.num_weapon_value;
	var max_number = req.body.max_number;
	var class_name = req.body.class_name;
	var username = connection.escape(req.body.username);
	var cur_deck = req.body.cur_deck;

	connection.connect();
	var command = "SELECT * FROM UserHasDeck NATURAL JOIN DeckMetaInfo NATURAL JOIN Deck WHERE numCards = 30 AND username != " + username + " "; 
	if (avg_cost_value) {
		command += "AND avgCost " + avg_cost_compr + " " + avg_cost_value + " ";
	}
	if (avg_attack_value) {
		command += "AND avgAttack " + avg_attack_compr + " " + avg_attack_value + " ";
	}
	if (avg_health_value) {
		command += "AND avgHealth " + avg_health_compr + " " + avg_health_value + " ";
	}
	if (num_spell_value) {
		command += "AND numSpell " + num_spell_compr + " " + num_spell_value + " ";
	}
	if (num_weapon_value) {
		command += "AND numWeapon " + num_weapon_compr + " " + num_weapon_value + " ";
	}
	if (class_name.toLowerCase() !== 'all') {
		command += "AND className = " + connection.escape(class_name) + " ";
	}
	command += "ORDER BY RAND() ";
	command += "LIMIT " + max_number;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		if (rows.length) {
			//	Send invitations
			command = "INSERT INTO Invitations (sender, receiver, message, initiator) VALUES ";
			for (var i = rows.length - 1; i >= 0; i--) {
				if ( username.toLowerCase() !== rows[i].username.toLowerCase() ) {
					var message = username + ' challenges your '+rows[i].name ;
					if (avg_cost_value) {
						message +=  ' with average cost ' + avg_cost_compr + ' ' + avg_cost_value;
					}
					message += '!';
					command += "(" + cur_deck + ", " + connection.escape(rows[i].username) + ", " + connection.escape(message) + ", " + username + "), ";
				};
			};
			connection.query( command.substring(0, command.lastIndexOf(',') ), function(err, rows, fields) {
				if (err) throw err;
				res.json({a:true});
				connection.end();
			});
		}
		else {
			//	Report not found
			res.json({a:false});
			connection.end();
		}
	});
});

app.post('/ReportResult/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var invitationID = req.body.invitationID;
	var result = req.body.result;
	var used_deck = req.body.used_deck;
	var username = connection.escape(req.body.username);
	connection.connect();
	var command = "SELECT * FROM Invitations WHERE invitationID = " + invitationID;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		if (rows.length) {
			if (result) {
				var loser = rows[0].sender;
				var winner = used_deck;
			}
			else {
				var winner = rows[0].sender;
				var loser = used_deck;
			}
			var waitingfor = connection.escape(rows[0].initiator);
			command = "DELETE FROM Invitations WHERE invitationID = " + invitationID;
			connection.query( command, function(err, rows, fields) {
				if (err) throw err;
				command = "INSERT INTO PendingResult (winner, loser, waitingfor, reporter) VALUES ";
				command += "(" + winner + ", " + loser + ", " + waitingfor + ", " + username + ")";
				connection.query( command, function(err, rows, fields) {
					if (err) throw err;
					command = "SELECT * FROM Invitations WHERE receiver = " + username;
					connection.query( command, function(err, rows, fields) {
						if (err) throw err;
						res.json(rows);
						connection.end();
					});
				});
			});
		};
	});
});

app.get('/AllPendingResults/:username', function(req, res) {
	var connection = mysql.createConnection(db_info);
	connection.connect();
	connection.query(	'SELECT winner, loser, waitingfor, pendingresultid, d1.name as winnerName, d2.name as loserName ' + 
						'FROM PendingResult, Deck d1, Deck d2 WHERE d1.deckID = PendingResult.winner ' + 
						'AND d2.deckID = PendingResult.loser ' + 
						'AND waitingfor = ' + connection.escape(req.params.username), 
						function(err, rows, fields) {
						  if (err) throw err;
						  res.json(rows);
						  connection.end();
						});
});

app.post('/AcceptOrRejectResult/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var accepted = req.body.acceptance;
	var pendingresultid = req.body.pendingresultid;

	connection.connect();
	var command = "SELECT * FROM PendingResult WHERE pendingresultid = " + pendingresultid;
	connection.query( command, function(err, rows, fields) {
		if (err) throw err;
		if (rows.length) {
			var winner = rows[0].winner;
			var loser = rows[0].loser;
			var waitingfor = rows[0].waitingfor;
			var reporter = rows[0].reporter;
			connection.query("DELETE FROM PendingResult WHERE pendingresultid = " + pendingresultid, function(err, rows, fields) {
				if (err) throw err;
				if ( accepted ) {
					connection.query("INSERT INTO Challenge (WinningID, LosingID) VALUES (" + winner + ", " + loser + ")", function(err, rows, fields) {
						if (err) throw err;
						connection.query(	'SELECT winner, loser, waitingfor, pendingresultid, d1.name as winnerName, d2.name as loserName ' + 
											'FROM PendingResult, Deck d1, Deck d2 WHERE d1.deckID = PendingResult.winner ' + 
											'AND d2.deckID = PendingResult.loser ' + 
											'AND waitingfor = ' + connection.escape(req.body.username), 
											function(err, rows, fields) {
											  if (err) throw err;
											  res.json(rows);
											  connection.end();
											});
					});
				}
				else {
					command = "INSERT INTO PendingResult (winner, loser, waitingfor, reporter) VALUES ";
					command += "(" + loser + ", " + winner + ", " + connection.escape(reporter) + ", " + connection.escape(waitingfor) + ")";
					connection.query( command, function (err, rows, fields) {
						if (err) throw err;
						connection.query(	'SELECT winner, loser, waitingfor, pendingresultid, d1.name as winnerName, d2.name as loserName ' + 
											'FROM PendingResult, Deck d1, Deck d2 WHERE d1.deckID = PendingResult.winner ' + 
											'AND d2.deckID = PendingResult.loser ' + 
											'AND waitingfor = ' + connection.escape(req.body.username), 
											function(err, rows, fields) {
											  if (err) throw err;
											  res.json(rows);
											  connection.end();
											});
					});
				};
			});
		};
	});
});

app.get('/allTemplates/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	connection.connect();
	connection.query("SELECT * FROM Template", function(err, rows, fields) {
		if (err) throw err;
		res.json(rows);
		connection.end();
	});
});

app.get('/allTemplateChallenges/', function(req, res) {
	var connection = mysql.createConnection(db_info);
	connection.connect();
	connection.query("SELECT * FROM templateversustable", function(err, rows, fields) {
		if (err) throw err;
		res.json(rows);
		connection.end();
	});
});

app.get('/bestAgainst/:template', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var template = req.params.template;
	connection.connect();
	connection.query("SELECT templateversustable.LosingTemplate as LosingTemplate, templateversustable.count as count, Template.name as name FROM templateversustable, Template WHERE templateversustable.LosingTemplate = Template.templateID AND templateversustable.WinningTemplate = " + template, function(err, rows, fields) {
		if (err) throw err;
		var won_records = rows;
		connection.query("SELECT templateversustable.WinningTemplate as WinningTemplate, templateversustable.count as count, Template.name as name FROM templateversustable, Template WHERE templateversustable.WinningTemplate = Template.templateID AND templateversustable.LosingTemplate = " + template, function(err, rows, fields) {
			if (err) throw err;
			var lost_records = rows;
			if (!(won_records.length && lost_records.length)) {
				res.json( [['None', -1], ['None', -1], ['None', -1]] );
				connection.end();
				return null;
			};
			var winrates = {};
			for (var i = won_records.length - 1; i >= 0; i--) {
				var won_matches = won_records[i].count;
				var lost_matches = 0;
				for (var j = lost_records.length - 1; j >= 0; j--) {
					if ( lost_records[j].WinningTemplate ===  won_records[i].LosingTemplate ) {
						lost_matches = lost_records[j].count;
						break;
					};
				};
				if (lost_matches === 0) {
					winrates[won_records[i].LosingTemplate] = { name: won_records[i].name, rate: 1 };
				}
				else {
					winrates[won_records[i].LosingTemplate] = { name: won_records[i].name, rate: won_matches / (won_matches + lost_matches) };
				};
			};
			var best_3 = [new Array('None', -1), new Array('None', -1), new Array('None', -1)]
			for (var opponent in winrates) {
				var rate = winrates[opponent].rate;
				for (var i = 0; i < best_3.length; i++) {
					if ( rate > best_3[i][1] ) {
						best_3.splice(i, 0, new Array(winrates[opponent].name, rate));
						best_3 = best_3.slice(0,3);
						break;
					}
				};
			};
			res.json(best_3);
			connection.end();
		});
	});
});

app.get('/detailedAnalysis/:template', function(req, res) {
	var connection = mysql.createConnection(db_info);
	var template = req.params.template;
	connection.connect();
	connection.query("SELECT cardID, cardName, num, WinningCount, LosingCount, WinningCount / (WinningCount + LosingCount) as winRate FROM cardmatchcountgiventemplate NATURAL JOIN Card WHERE templateID = " + template + " ORDER BY WinningCount / (WinningCount + LosingCount) DESC", function(err, rows ,fields) {
		if (err) throw err;
		res.json(rows);
		connection.end();
	});
});


































